# TUSI

Universal Streaming Interface(s) is a standard api to access similar peripherals like UART, SPI, I2C, etc.



## Example UART

``` C
/* initialization */
uint8_t myUartBuf[10];
tuart_t myUart = {
	.tusi.ctl.hw = USART0,
	.tusi.ctl.wordType = ttype_U8,
	.tusi.buf = myUartBuf,
	.tusi.bufLen = 10,
};
tusi_init(&myUart);

/* simple tx */
mySrc[4] = {1,2,3,4};
actualTxCnt = myUart.tx(mySrc, 4);		/* This send 1,2,3,4 on the serial bus */

/* simple rx */
myDst[4] = {};
actualRxCnt = myUart.rx(myDst, 4);		/* This will receive up to 4 word into myDst (blocking or non-blocking behavior) */

```


## Example SPI Master

``` C
/* initialization */
uint32_t mySPIMbuf[10];
tspim_t mySPIM = {
	.tusi.ctl.hw = USART0,
	.tusi.ctl.wordType = ttype_U32,
	.tusi.buf = mySPIMbuf,
	.tusi.bufLen = 10,
};
tusi_init(&mySPIM);

/* simple trx */
sending[4] = {0xFFFFFFFF, 0xFFFFFFFE, 0xFFFFFFFD, 0xFFFFFFFC};
receiving[4] = {};

actualCnt = mySPIM.trx(sending, receiving, 4);


```



## Example SPI Slave

``` C
/* initialization */
uint32_t mySPISbuf[10];
tspim_t mySPIS = {
	.tusi.ctl.hw = USART0,
	.tusi.ctl.wordType = ttype_U32,
	.tusi.buf = mySPISbuf,
	.tusi.bufLen = 10,
};
tusi_init(&mySPIS);

/* tx only, only cs */


```



## Example I2C Master

``` C
```



## Example I2C Slave

``` C
```


## Example CDC

``` C
```


