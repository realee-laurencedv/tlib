# tledstr usage guide

Here is a textual representation of the example setup:

* Physical setup:         [   MCU   ]->[0]->[1]->[2]
* pixel bufer:            [G2],[R2],[B2];[G1],[R1],[B1];[G0],[R0],[B0]
* phy buffer:             [GGGG2222],[RRRR2222],[BBBB2222]

Using this setup, the code to use it would be:

``` C
#include <tledstr.h>

uint8_t myLedStrPixBuf[WS2812B_PIXBUF_SIZE_BYTE(3)];
uint8_t myLedStrPhyBuf[WS2812B_PHYBUF_SIZE_BYTE(1)];
tledstr_t myLedStr = {
    .config.pixBuf =    myLedStrPixBuf,
    .config.pixNb =     3,
    .config.phyBuf =    myLedStrPhyBuf,
    .config.phyBufLen = sizeof(myLedStrPhyBuf),
    .config.type =      tledstr_ws2812b,
};

tledstr_grb_t myColor = { .r=0xFF, .g=0xA5, .b=0x00};
tledstr_grb_t inspectColor = {};

tledstr_init(&myLedStr);                                    // init
tledstr_setPixel(&myLedStr, (void*)&myColor, 0, 1);         // set pixel 0 to "myColor"
tledstr_fillPixel(&myLedStr, (void*)&myColor, 0, 2);        // fill pixel 0 to 2 with "myColor"
tledstr_getPixel(&myLedStr, (void*)&inspectColor, 2, 1);    // copy the current pixel 2 color into "inspectColor"

```


