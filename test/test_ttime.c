#include <unity.h>

#include <stdbool.h>
#include <stdint.h>

#include <mock_emutil.h>
#include <mock_em_rtcc.h>
#include <mock_em_cmu.h>
#include <ttime.h>



/* -------------------------------- +
|									|
|	Helper function					|
|									|
+ -------------------------------- */
void setUp(void) {

}
void tearDown(void) {

}

/* -------------------------------- +
|									|
|	Unit test						|
|									|
+ -------------------------------- */
void test_ttime_initReturnCodeWhenGivenInvalidHw(void) {
	// emutil_findCMUClock_ExpectAndReturn(NULL, 0);

	// TEST_ASSERT_EQUAL(tcode_param, ttime_init(NULL));

}

void test_init_initEnableAllClockAndReturnSuccessWhenGivenValidHw(void) {
	// emutil_findCMUClock_ExpectAndReturn(((RTCC_TypeDef *) RTCC_BASE), cmuClock_RTCC);
	// CMU_OscillatorEnable_Expect(cmuOsc_LFRCO, true, true);
	// CMU_ClockSelectSet_Expect(cmuClock_LFE, cmuSelect_LFRCO);
	// CMU_ClockEnable_Expect(cmuClock_HFLE, true);
	// CMU_ClockEnable_Expect(cmuClock_LFE, true);
	// CMU_ClockEnable_Expect(cmuClock_RTCC, true);
	// RTCC_ChannelInit_Expect(0, NULL);
	// RTCC_ChannelInit_IgnoreArg_confPtr();
	// RTCC_Init_ExpectAnyArgs();

	// TEST_ASSERT_EQUAL(tcode_success, ttime_init(((RTCC_TypeDef *) RTCC_BASE)));
}
