#include <unity.h>
#include <testutil.h>

#include <thal_vhw.h>

/* -------------------------------- +
|									|
|	Helper function					|
|									|
+ -------------------------------- */
void setUp(void) {
	/* Load default values to vars */
	_thal.stat.all =		THAL_HW_STAT_DEF;
}

void tearDown(void) {
}

/* -------------------------------- +
|									|
|	VHW - Runtime test				|
|									|
+ -------------------------------- */
void test_thal_vhw_weakDefinitionExist(void) {
	TEST_ASSERT_EQUAL_MESSAGE(thal_hw_virtual, _thal.info.hwPlatform, "hw platform should be virtual");
	TEST_ASSERT_EQUAL_MESSAGE(0x00010000, _thal.info.version, "version should be 1.0.0");
}

/* init */
void test_thal_vhw_initHandleNULLasGlobalInst(void) {
	thal_t * actual = NULL;
	thal_t localThal;

	actual = thal_init(NULL);
	TEST_ASSERT_EQUAL_PTR_MESSAGE(&_thal, actual, "init worked on global instance");
}

void test_thal_vhw_initUseLocalInstWhenGivenOne(void) {
	thal_t * actual = NULL;
	thal_t localThal;

	actual = thal_init(&localThal);
	TEST_ASSERT_EQUAL_PTR_MESSAGE(&localThal, actual, "init worked on local instance");
}

void test_thal_vhw_initSetStatInited(void) {
	thal_init(NULL);
	TEST_ASSERT_EQUAL_MESSAGE(true, _thal.stat.inited, "inited flag should be set after init");
}

void test_thal_vhw_initResetAllVariables(void) {
	_thal.stat.all = ~THAL_HW_STAT_DEF;

	thal_init(NULL);

	TEST_ASSERT_EQUAL_MESSAGE(THAL_VHW_INFO_VERSION_DEF, _thal.info.version, "version should be reseted");
	TEST_ASSERT_EQUAL_MESSAGE(THAL_VHW_INFO_HWPLATFORM_DEF, _thal.info.hwPlatform, "hwplatform should be reseted");
	TEST_ASSERT_NOT_EQUAL_MESSAGE(~THAL_HW_STAT_DEF, _thal.stat.all, "stat should have changed");
}


/* term */
void test_thal_vhw_termHandleNULLasGlobalInst(void) {
	thal_t * actual = NULL;

	actual = thal_term(NULL);
	TEST_ASSERT_EQUAL_PTR_MESSAGE(&_thal, actual, "term worked on global instance");
}

void test_thal_vhw_termUseLocalInstWhenGivenOne(void) {
	thal_t * actual = NULL;
	thal_t localThal;

	actual = thal_term(&localThal);
	TEST_ASSERT_EQUAL_PTR_MESSAGE(&localThal, actual, "term worked on local instance");
}

void test_thal_vhw_termClearAllStat(void) {
	thal_term(NULL);
	TEST_ASSERT_EQUAL_MESSAGE(false, _thal.stat.inited, "inited flag should be cleared after term");
	TEST_ASSERT_EQUAL_MESSAGE(false, _thal.stat.hwCoupled, "hwCoupled flag should be cleared after term");
}


/* -------------------------------- +
|									|
|	VHW - HW mapping test			|
|									|
+ -------------------------------- */
void test_thal_vhw_hwMapShouldBeCorrectlyLinked(void) {
	char msg[32] = {};
	char name[32] = {};
	/* CPU */
	for (int i = 0; i < THAL_HW_CPU_CNT; i++) {
		TEST_FPRINT1(name, "CPU%u", i);
		TEST_FPRINT1(msg, "ptr CPU %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(&__thal_vhw_cpu[i], &_thal.hw.cpu[i], msg);
		TEST_FPRINT1(msg, "name CPU %u", i);
		TEST_ASSERT_EQUAL_STRING_MESSAGE(name, _thal.hw.cpu[i].name, msg);
	}
	/* DMA */
	for (int i = 0; i < THAL_HW_DMA_CNT; i++) {
		TEST_FPRINT1(name, "DMA%u", i);
		TEST_FPRINT1(msg, "ptr DMA %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(&__thal_vhw_dma[i], &_thal.hw.dma[i], msg);
		TEST_FPRINT1(msg, "name DMA %u", i);
		TEST_ASSERT_EQUAL_STRING_MESSAGE(name, _thal.hw.dma[i].name, msg);
		TEST_FPRINT1(msg, "ptr DMACH %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(__thal_vhw_dmach[i], _thal.hw.dma[i].ch, msg);
	}
	/* CLK */
	for (int i = 0; i < THAL_HW_CLK_CNT; i++) {
		TEST_FPRINT1(name, "CLK%u", i);
		TEST_FPRINT1(msg, "ptr CLK %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(&__thal_vhw_clk[i], &_thal.hw.clk[i], msg);
		TEST_FPRINT1(msg, "name CLK %u", i);
		TEST_ASSERT_EQUAL_STRING_MESSAGE(name, _thal.hw.clk[i].name, msg);
	}
	/* GPIO */
	for (int i = 0; i < THAL_HW_GPIO_CNT; i++) {
		TEST_FPRINT1(name, "GPIO%u", i);
		TEST_FPRINT1(msg, "ptr GPIO %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(&__thal_vhw_gpio[i], &_thal.hw.gpio[i], msg);
		TEST_FPRINT1(msg, "name GPIO %u", i);
		TEST_ASSERT_EQUAL_STRING_MESSAGE(name, _thal.hw.gpio[i].name, msg);
	}
	/* TIMER */
	for (int i = 0; i < THAL_HW_TIMER_CNT; i++) {
		TEST_FPRINT1(name, "TIMER%u", i);
		TEST_FPRINT1(msg, "ptr TIMER %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(&__thal_vhw_timer[i], &_thal.hw.timer[i], msg);
		TEST_FPRINT1(msg, "name TIMER %u", i);
		TEST_ASSERT_EQUAL_STRING_MESSAGE(name, _thal.hw.timer[i].name, msg);
	}
	/* UART */
	for (int i = 0; i < THAL_HW_UART_CNT; i++) {
		TEST_FPRINT1(name, "UART%u", i);
		TEST_FPRINT1(msg, "ptr UART %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(&__thal_vhw_uart[i], &_thal.hw.uart[i], msg);
		TEST_FPRINT1(msg, "name UART %u", i);
		TEST_ASSERT_EQUAL_STRING_MESSAGE(name, _thal.hw.uart[i].name, msg);
	}
	/* I2C */
	for (int i = 0; i < THAL_HW_I2C_CNT; i++) {
		TEST_FPRINT1(name, "I2C%u", i);
		TEST_FPRINT1(msg, "ptr I2C %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(&__thal_vhw_i2c[i], &_thal.hw.i2c[i], msg);
		TEST_FPRINT1(msg, "name I2C %u", i);
		TEST_ASSERT_EQUAL_STRING_MESSAGE(name, _thal.hw.i2c[i].name, msg);
	}
	/* I2S */
	for (int i = 0; i < THAL_HW_I2S_CNT; i++) {
		TEST_FPRINT1(name, "I2S%u", i);
		TEST_FPRINT1(msg, "ptr I2S %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(&__thal_vhw_i2s[i], &_thal.hw.i2s[i], msg);
		TEST_FPRINT1(msg, "name I2S %u", i);
		TEST_ASSERT_EQUAL_STRING_MESSAGE(name, _thal.hw.i2s[i].name, msg);
	}
	/* ADC */
	for (int i = 0; i < THAL_HW_ADC_CNT; i++) {
		TEST_FPRINT1(name, "ADC%u", i);
		TEST_FPRINT1(msg, "ptr ADC %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(&__thal_vhw_adc[i], &_thal.hw.adc[i], msg);
		TEST_FPRINT1(msg, "name ADC %u", i);
		TEST_ASSERT_EQUAL_STRING_MESSAGE(name, _thal.hw.adc[i].name, msg);
	}
	/* DAC */
	for (int i = 0; i < THAL_HW_DAC_CNT; i++) {
		TEST_FPRINT1(name, "DAC%u", i);
		TEST_FPRINT1(msg, "ptr DAC %u", i);
		TEST_ASSERT_EQUAL_PTR_MESSAGE(&__thal_vhw_dac[i], &_thal.hw.dac[i], msg);
		TEST_FPRINT1(msg, "name DAC %u", i);
		TEST_ASSERT_EQUAL_STRING_MESSAGE(name, _thal.hw.dac[i].name, msg);
	}
}

void test_thal_vhw_hwTypesAreAsExpected(void) {
	/* This test is mainly useful to ensure test hw is always in known state */

	TEST_ASSERT_EQUAL_MESSAGE(_thal.hw.cpu[0].type, thal_cpu_type_virt32, "CPU0 should be virtual 32bit");
	TEST_ASSERT_EQUAL_MESSAGE(_thal.hw.dma[0].type, thal_dma_type_simple, "DMA0 should be simple");
	TEST_ASSERT_EQUAL_MESSAGE(_thal.hw.dma[1].type, thal_dma_type_linked, "DMA1 should be linked");
}

/* -------------------------------- +
|									|
|	VHW - CPU test					|
|									|
+ -------------------------------- */
/* atomic enter */
void test_thal_vhwCPU_atomicEnterSavePreviousState(void) {
	_thal.hw.cpu[0].stat.atomic = true;							/* force state to true */

	TEST_ASSERT_EQUAL_MESSAGE(true, thal_cpu_atomicEnter(&_thal.hw.cpu[0]), "previous state should be returned");
}

void test_thal_vhwCPU_atomicEnterHandleNull(void) {

	TEST_ASSERT_EQUAL_MESSAGE(false, thal_cpu_atomicEnter(NULL), "Null given, should return false");
}

void test_thal_vhwCPU_atomicEnterSetFlagToTrue(void) {
	_thal.hw.cpu[0].stat.atomic = false;
	TEST_ASSERT_EQUAL_MESSAGE(false, _thal.hw.cpu[0].stat.atomic, "Should be false before entering");
	thal_cpu_atomicEnter(&_thal.hw.cpu[0]);
	TEST_ASSERT_EQUAL_MESSAGE(true, _thal.hw.cpu[0].stat.atomic, "Should be true after entering");
	thal_cpu_atomicEnter(&_thal.hw.cpu[0]);
	TEST_ASSERT_EQUAL_MESSAGE(true, _thal.hw.cpu[0].stat.atomic, "Should still be true");
}

/* atomic exit */
void test_thal_vhwCPU_atomicExitShouldRestoreFlag(void) {
	_thal.hw.cpu[0].stat.atomic = false;

	thal_cpu_atomicExit(&_thal.hw.cpu[0], false);
	TEST_ASSERT_EQUAL_MESSAGE(false, _thal.hw.cpu[0].stat.atomic, "Should stay false");

	thal_cpu_atomicExit(&_thal.hw.cpu[0], true);
	TEST_ASSERT_EQUAL_MESSAGE(true, _thal.hw.cpu[0].stat.atomic, "Should now be true");
}

void test_thal_vhwCPU_atomicExitHandleNull(void) {
	thal_cpu_atomicExit(NULL, false);
	TEST_PASS_MESSAGE( "Null given and execution didn't crash");
}


/* -------------------------------- +
|									|
|	VHW - DMA test					|
|									|
+ -------------------------------- */
/* init */
void test_thal_vhwDMA_init_HandleNull(void) {

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_init(NULL), "NULL given");
}

void test_thal_vhwDMA_init_ResetAllMemory(void) {
	char msg[64] = {};

	/* Setup */
	for (int i = 0; i < THAL_HW_DMA_CNT; i++) {
		_thal.hw.dma[i].ctl.all = 0xFFFFFFFF;
		_thal.hw.dma[i].stat.all = 0xFFFFFFFF;
		TEST_FPRINT1(msg, "DMA%u: CTL should contain all 1s", i);
		TEST_ASSERT_EQUAL_HEX32_MESSAGE(0xFFFFFFFF, _thal.hw.dma[i].ctl.all, msg);
		TEST_FPRINT1(msg, "DMA%u: STAT should contain all 1s", i);
		TEST_ASSERT_EQUAL_HEX32_MESSAGE(0xFFFFFFFF, _thal.hw.dma[i].stat.all, msg);

		for (int j = 0; j < THAL_HW_DMACH_CNT; j++) {
			_thal.hw.dma[i].ch[j].ctl.all = 0xFFFFFFFF;
			_thal.hw.dma[i].ch[j].stat.all = 0xFFFFFFFF;
			_thal.hw.dma[i].ch[j].xfer = (thal_dma_xfer_t *)0xFFFFFFFF;
			TEST_FPRINT2(msg, "DMA%u.CH%u: CTL should contain all 1s", i, j);
			TEST_ASSERT_EQUAL_HEX32_MESSAGE(0xFFFFFFFF, _thal.hw.dma[i].ch[j].ctl.all, msg);
			TEST_FPRINT2(msg, "DMA%u.CH%u: STAT should contain all 1s", i, j);
			TEST_ASSERT_EQUAL_HEX32_MESSAGE(0xFFFFFFFF, _thal.hw.dma[i].ch[j].stat.all, msg);
			TEST_FPRINT2(msg, "DMA%u.CH%u: xfer should contain all 1s", i, j);
			TEST_ASSERT_EQUAL_PTR_MESSAGE(0xFFFFFFFF, _thal.hw.dma[i].ch[j].xfer, msg);
		}
	}

	/* Test */
	for (int i = 0; i < THAL_HW_DMA_CNT; i++) {
		TEST_FPRINT1(msg, "DMA%u: init should return success", i);
		TEST_ASSERT_EQUAL_MESSAGE(tcode_success, thal_dma_init(&_thal.hw.dma[i]), msg);

		TEST_FPRINT1(msg, "DMA%u: CTL should contain default", i);
		TEST_ASSERT_EQUAL_HEX32_MESSAGE(THAL_HW_DMA_CTL_DEF, _thal.hw.dma[i].ctl.all, msg);
		TEST_FPRINT1(msg, "DMA%u: STAT should contain default", i);
		TEST_ASSERT_EQUAL_HEX32_MESSAGE(THAL_HW_DMA_STAT_DEF, _thal.hw.dma[i].stat.all, msg);

		for (int j = 0; j < THAL_HW_DMACH_CNT; j++) {
			TEST_FPRINT2(msg, "DMA%u.CH%u: CTL should contain default", i, j);
			TEST_ASSERT_EQUAL_HEX32_MESSAGE(THAL_HW_DMACH_CTL_DEF, _thal.hw.dma[i].ch[j].ctl.all, msg);
			TEST_FPRINT2(msg, "DMA%u.CH%u: STAT should contain default", i, j);
			TEST_ASSERT_EQUAL_HEX32_MESSAGE(THAL_HW_DMACH_STAT_DEF, _thal.hw.dma[i].ch[j].stat.all, msg);
			TEST_FPRINT2(msg, "DMA%u.CH%u: xfer should point to NULL", i, j);
			TEST_ASSERT_EQUAL_PTR_MESSAGE(NULL, _thal.hw.dma[i].ch[j].xfer, msg);
		}
	}
}

/* term */
void test_thal_vhwDMA_term_HandleNull(void) {

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_term(NULL), "NULL given");
}

/* getCapabilities */
void test_thal_vhwDMA_getCapabilities_handleNULL(void) {

	TEST_ASSERT_EQUAL_MESSAGE(0, thal_dma_getCapabilities(NULL), "NULL given");
}

/* pwr */
void test_thal_vhwDMA_pwr_handleNULL(void) {

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_pwr(NULL, thal_powerLvl_off), "off power");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_pwr(NULL, thal_powerLvl_minimal), "minimal power");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_pwr(NULL, thal_powerLvl_low), "low power");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_pwr(NULL, thal_powerLvl_reduced), "reduced power");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_pwr(NULL, thal_powerLvl_full), "full power");
}

/* cmd */
void test_thal_vhwDMA_cmd_HandleNull(void) {

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_cmd(NULL, thal_dma_cmd_none), "none command");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_cmd(NULL, thal_dma_cmd_enable), "enable command");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_cmd(NULL, thal_dma_cmd_disable), "disable command");
}

/* stat */
void test_thal_vhwDMA_stat_HandleNull(void) {
	thal_dma_stat_t actual = { .all = 0xFFFFFFFF};

	actual = thal_dma_statGet(NULL);

	TEST_ASSERT_EQUAL_MESSAGE(0, actual.all, "NULL given, should return 0");
}

void test_thal_vhwDMA_stat_ReturnStat(void) {
	_thal.hw.dma[0].stat.all = 0x12345678;

	thal_dma_stat_t actual = thal_dma_statGet(&_thal.hw.dma[0]);

	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0x12345678, actual.all, "status should be the same");
}

/* xfer init */
void test_thal_vhwDMA_xferInit_HandleNull(void) {

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_xferInit(NULL), "NULL given");
}

/* xfer term */
void test_thal_vhwDMA_xferTerm_HandleNull(void) {

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_xferTerm(NULL), "NULL given");
}

/* xfer cbAttach */
void test_thal_vhwDMA_xferCbAttach_HandleNull(void) {
	thal_dma_xfer_t testXfer;
	thal_dma_xferCb testCb = (thal_dma_xferCb)0x12345678;

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_xferCbAttach(NULL, NULL), "double NULL given");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_xferCbAttach(NULL, testCb), "xfer NULL given");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_xferCbAttach(&testXfer, NULL), "cb NULL given");
}

/* xfer cbDetach */
void test_thal_vhwDMA_xferCbDetach_HandleNull(void) {
	thal_dma_xfer_t testXfer;
	thal_dma_xferCb testCb = (thal_dma_xferCb)0x12345678;

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_xferCbDetach(NULL, NULL), "double NULL given");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_xferCbDetach(NULL, testCb), "xfer NULL given");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dma_xferCbDetach(&testXfer, NULL), "cb NULL given");
}



/* ch statGet */
void test_thal_vhwDMAch_stat_HandleNull(void) {
	thal_dmach_stat_t actual = { .all = 0xFFFFFFFF};

	actual = thal_dmach_statGet(NULL);

	TEST_ASSERT_EQUAL_MESSAGE(0, actual.all, "NULL given, should return 0");
}

/* ch xferAttach */
void test_thal_vhwDMAch_xferAttach_HandleNull(void) {
	thal_dma_xfer_t testXfer;
	thal_dmach_t testCh;

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dmach_xferAttach(NULL, NULL), "double NULL given");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dmach_xferAttach(NULL, &testXfer), "ch NULL given");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dmach_xferAttach(&testCh, NULL), "xfer NULL given");
}

/* ch xferDetach */
void test_thal_vhwDMAch_xferDetach_HandleNull(void) {
	thal_dma_xfer_t testXfer;
	thal_dmach_t testCh;

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dmach_xferDetach(NULL, NULL), "double NULL given");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dmach_xferDetach(NULL, &testXfer), "ch NULL given");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, thal_dmach_xferDetach(&testCh, NULL), "xfer NULL given");
}
