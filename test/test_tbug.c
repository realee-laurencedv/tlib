#include <unity.h>
#include <stdbool.h>
#include <stdint.h>

#include <tbug.h>



/* -------------------------------- +
|									|
|	Helper function					|
|									|
+ -------------------------------- */
void setUp(void) {

}
void tearDown(void) {

}



/* -------------------------------- +
|									|
|	Unit test						|
|									|
+ -------------------------------- */
void test_tbug_env_u8printsCorrecly(void) {
	tbug_u8(0xFF);
	TEST_IGNORE_MESSAGE("TEST OUTPUT should contain \'FF\'");
}

void test_tbug_env_u16printsCorrecly(void) {
	tbug_u16(0xFEDC);
	TEST_IGNORE_MESSAGE("TEST OUTPUT should contain \'FEDC\'");
}

void test_tbug_env_u32printsCorrecly(void) {
	tbug_u32(0xFEDCBA98);
	TEST_IGNORE_MESSAGE("TEST OUTPUT should contain \'FEDCBA98\'");
}

void test_tbug_env_stringSendCompleteString(void) {
	tbug_string("Allo mon ti co co");
	TEST_IGNORE_MESSAGE("TEST OUTPUT should contain \'Allo mon ti co co\'");
}

void test_tbug_env_arraySendCompleteArray(void) {
	uint8_t myArray[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	tbug_array(myArray, 10);
	TEST_IGNORE_MESSAGE("TEST OUTPUT should contain \'0123456789\'");
}

void test_tbug_env_formatSendFormatedString(void) {
	tbug_format("%u formated string", 1);
	TEST_IGNORE_MESSAGE("TEST OUTPUT should contain \'1 formated string\'");
}

void test_tbug_env_stringShouldHandleNullSilently(void) {
	tbug_string(NULL);
	TEST_PASS();		/* No segfault means success */
}

void test_tbug_env_logShouldHandleNullSilently(void) {
	tbug_log(TBUG_VERBOSITY_CRITICAL, NULL, NULL);
	TEST_PASS();		/* No segfault means success */
}

void test_tbug_env_logShouldPrintOnlyAtCorrectVerbosityLevel(void) {
	tbug_log(TBUG_VERBOSITY_NONE, "this", "msg should not appear in TEST OUTPUT");
	tbug_log(TBUG_VERBOSITY_CRITICAL, "user", "with %u formated message", 1);
}
