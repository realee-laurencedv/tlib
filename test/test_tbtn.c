#include <unity.h>
#include <stdbool.h>
#include <stdint.h>

#include <tbtn.h>



/* -------------------------------- +
|									|
|	Helper function					|
|									|
+ -------------------------------- */
void setUp(void) {

}
void tearDown(void) {

}

void helperPressCb(void) {

}
void helperReleaseCb(void) {

}


/* -------------------------------- +
|									|
|	Unit test						|
|									|
+ -------------------------------- */
void test_tbtn_eventTypeIsUsable(void) {
	tbtn_event_t myEvent = tbtn_event_press;
	TEST_ASSERT_EQUAL(tbtn_event_press, myEvent);
	myEvent = tbtn_event_release;
	TEST_ASSERT_EQUAL(tbtn_event_release, myEvent);
	myEvent = tbtn_event_none;
	TEST_ASSERT_EQUAL(tbtn_event_none, myEvent);
}

void test_tbtn_cbTypeIsUsable(void) {
	tbtn_cb_t myCb = &helperPressCb;
	TEST_ASSERT_EQUAL_PTR(&helperPressCb, myCb);
}

void test_tbtn_ctlStructIsUsable(void) {
	// tbtn_t myBtn = { .pin = 0x12};
	// TEST_ASSERT_EQUAL(0x12, myBtn.pin);
	TEST_IGNORE_MESSAGE("TODO: revise following new tio scheme");
}

