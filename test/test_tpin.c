#include <unity.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <tpin.h>

/* -------------------------------- +
|									|
|	Helper function					|
|									|
+ -------------------------------- */
void setUp(void) {
}

void tearDown(void) {
}


/*--------------------------------- +
|									|
|	Macro test						|
|									|
+ -------------------------------- */
void test_tpin_macroEncodeAsExpected(void) {
	// TEST_ASSERT_EQUAL_MESSAGE((gpioPortA<<8)|0, TIO(gpioPortA, 0),	"PA0");
	// TEST_ASSERT_EQUAL_MESSAGE((gpioPortA<<8)|7, TIO(gpioPortA, 7),	"PA7");
	// TEST_ASSERT_EQUAL_MESSAGE((gpioPortA<<8)|15, TIO(gpioPortA, 15),"PA15");
	// TEST_ASSERT_EQUAL_MESSAGE((gpioPortB<<8)|0, TIO(gpioPortB, 0),	"PB0");
	// TEST_ASSERT_EQUAL_MESSAGE((gpioPortB<<8)|6, TIO(gpioPortB, 6),	"PB6");
	// TEST_ASSERT_EQUAL_MESSAGE((gpioPortB<<8)|15, TIO(gpioPortB, 15),"PB15");
	// TEST_ASSERT_EQUAL_MESSAGE((gpioPortC<<8)|0, TIO(gpioPortC, 0),	"PC0");
	// TEST_ASSERT_EQUAL_MESSAGE((gpioPortC<<8)|9, TIO(gpioPortC, 9),	"PC9");
	// TEST_ASSERT_EQUAL_MESSAGE((gpioPortC<<8)|15, TIO(gpioPortC, 15),"PC15");
	TEST_IGNORE_MESSAGE("TODO: revise following new tio scheme");
}

void test_tpin_macroDecodePinCorrectly(void) {

	// tio_t testedPA0 = TIO(gpioPortA, 0);
	// tio_t testedPB6 = TIO(gpioPortB, 6);
	// tio_t testedPC15 = TIO(gpioPortC, 15);
	// TEST_ASSERT_EQUAL_MESSAGE(0, TIO_PIN(testedPA0),	"PA0");
	// TEST_ASSERT_EQUAL_MESSAGE(6, TIO_PIN(testedPB6),	"PB6");
	// TEST_ASSERT_EQUAL_MESSAGE(15, TIO_PIN(testedPC15),	"PC15");
	TEST_IGNORE_MESSAGE("TODO: revise following new tio scheme");
}

void test_tpin_macroDecodePortCorrectly(void) {
	// tio_t testedPA0 = TIO(gpioPortA, 0);
	// tio_t testedPB6 = TIO(gpioPortB, 6);
	// tio_t testedPC15 = TIO(gpioPortC, 15);
	// TEST_ASSERT_EQUAL_MESSAGE(gpioPortA, TIO_PORT(testedPA0),	"PA0");
	// TEST_ASSERT_EQUAL_MESSAGE(gpioPortB, TIO_PORT(testedPB6),	"PB6");
	// TEST_ASSERT_EQUAL_MESSAGE(gpioPortC, TIO_PORT(testedPC15),	"PC15");
	TEST_IGNORE_MESSAGE("TODO: revise following new tio scheme");
}


/* -------------------------------- +
|									|
|	Unit test						|
|									|
+ -------------------------------- */
void test_tpin_findTIOreturnINVALIDWhenGivenNull(void) {
	// TEST_ASSERT_EQUAL(TPIN_INVALID, tio_findByPeri(NULL, 0, 0));
	TEST_IGNORE_MESSAGE("TODO: revise following new tio scheme");
}

void test_tpin_findTIOhandleCorrectlyImpossibleTimerValue(void) {

	// TEST_ASSERT_EQUAL_HEX16_MESSAGE( TIO_INVALID, tio_findByPeri(TIMER0, tio_pin_tmr_cc0, 15),	"TIMER0 CC0 #15");
	// TEST_ASSERT_EQUAL_HEX16_MESSAGE( TIO_INVALID, tio_findByPeri(TIMER0, tio_pin_tmr_cdti0, 7),	"TIMER0 CDTI0 #7");
	// TEST_ASSERT_EQUAL_HEX16_MESSAGE( TIO_INVALID, tio_findByPeri(TIMER0, tio_pin_tmr_cc3, 0),	"TIMER0 CC3 #0");
	// TEST_ASSERT_EQUAL_HEX16_MESSAGE( TIO_INVALID, tio_findByPeri(TIMER0, tio_pin_letmr_out0, 0),"TIMER0 OUT0 #0");
	TEST_IGNORE_MESSAGE("TODO: revise following new tio scheme");
}
