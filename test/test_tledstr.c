/*
 *	\note	This test is built to be generic, actual pins and location are not specific to any mcu, thus tio_findByPeri returns valid or invalid (depending on requirements) bullshit.
 *
 */

#include <unity.h>

#include <string.h>

#include <thal.h>
#include <thal_vhw.h>
#include <tledstr.h>
#include <dev/ws2812b.h>

/* -------------------------------- +
|									|
|	Test constants					|
|									|
+ -------------------------------- */
#define	TEST_MEMGAP_SIZE		(10)			/* Nb of byte before and after tested memory block (useful for ensuring correctly limited mem access) */
#define	TEST_PIXNB				(10)			/* Nb of pixel in the test tledstr, should remain constant at 10 (some tests are hardcoded) */
#define TEST_PHYBUF_PIXNB		(3)				/* Nb of pixel in phy buffer for tests, should remain constant at 3 (some tests are hardcoded) */

uint8_t testLedStrPixBuf[WS2812B_PIXBUF_SIZE_BYTE(TEST_PIXNB)] = {};
uint8_t testLedStrPhyBuf[WS2812B_PHYBUF_SIZE_BYTE(TEST_PHYBUF_PIXNB)] = {};
thal_spi_t testSpi;
tledstr_t testLedStr = {
	.config.pixBuf =	testLedStrPixBuf,
	.config.pixNb =		TEST_PIXNB,
	.config.type = 		tledstr_ws2812b,
	.config.hw =		&testSpi,
	.config.loc =		0,
	.config.dmaCh =		0,
	.config.activeVal =	false,
	.config.phyBuf =	testLedStrPhyBuf,
	.config.phyBufLen =	sizeof(testLedStrPhyBuf),
};

uint8_t testInput[TEST_PIXNB] = {};


/* -------------------------------- +
|									|
|	Helper function					|
|									|
+ -------------------------------- */
void helper_tledstr_reset(tledstr_t * tledstr) {
	if (tledstr != NULL) {
		tledstr->state = tledstr_state_unknown;
		tledstr->pixSize =	0;
		tledstr->xferSize = 0;
		tledstr->cnt = 0;
		tledstr->encode = NULL;
	}
}

void setUp(void) {
	thal_init(NULL);
	helper_tledstr_reset(&testLedStr);
	memset(testLedStrPixBuf, 0xFF, sizeof(testLedStrPixBuf));
	memset(testLedStrPhyBuf, 0xFF, sizeof(testLedStrPhyBuf));
}

void tearDown(void) {
	thal_term(NULL);
}


/* -------------------------------- +
|									|
|	LEDSTR Unit test				|
|									|
+ -------------------------------- */
void test_tledstr_typeShouldBeUsableAndKnown(void) {
	/* Minimal test, even though, compilation will yell before this */
	TEST_ASSERT_MESSAGE(sizeof(testLedStr), "test object type has a size, thus exist");
}


/* -------------------------------- +
|	init							|
+ -------------------------------- */
void test_tledstr_initShouldHandleNull(void) {
	/* Minimal testing */
	TEST_ASSERT_EQUAL(tcode_null, tledstr_init(NULL));
}

void test_tledstr_initShouldValidateAllConfig(void) {
	uint8_t myLedStrPBuf[WS2812B_PIXBUF_SIZE_BYTE(3)] = {};
	uint32_t myLedStrPhyBuf[WS2812B_COLOR_NB *2] = {};
	uint32_t myLedStrPhyBuf_small[WS2812B_COLOR_NB] = {};

	/* SHOULD PASS SECTION */
	tledstr_t myLedStr = {
		.config.pixBuf = myLedStrPBuf,
		.config.pixNb = 3,
		.config.type = tledstr_ws2812b,
		.config.phyBuf = (void*)myLedStrPhyBuf,
		.config.phyBufLen = sizeof(myLedStrPhyBuf),
		.config.hw = &testSpi,
		.config.dmaCh = 0,
	};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&myLedStr), "All valid config should return success");

	tledstr_t myLedStr9 = {
		.config.pixBuf = myLedStrPBuf,
		.config.pixNb = 1,
		.config.type = tledstr_ws2812b,
		.config.phyBuf = (void*)myLedStrPhyBuf_small,
		.config.phyBufLen = sizeof(myLedStrPhyBuf_small),
		.config.hw = &testSpi,
		.config.dmaCh = 0,
	};
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&myLedStr9), "Only 1 pixel so should return success even if buffer is smaller than 2 pixel");


	/* SHOULD FAIL SECTION */
	/* NULL pix buffer */
	tledstr_t myLedStr2 = {
		.config.pixBuf = NULL,
		.config.pixNb = 3,
		.config.type = tledstr_ws2812b,
		.config.phyBuf = (void*)myLedStrPhyBuf,
		.config.phyBufLen = sizeof(myLedStrPhyBuf),
		.config.hw = &testSpi,
		.config.dmaCh = 0,
	};
	TEST_ASSERT_EQUAL_MESSAGE(tcode_badConfig, tledstr_init(&myLedStr2), "NULL pixBuf should return badConfig");

	/* 0 sized led string */
	tledstr_t myLedStr3 = {
		.config.pixBuf = myLedStrPBuf,
		.config.pixNb = 0,
		.config.type = tledstr_ws2812b,
		.config.phyBuf = (void*)myLedStrPhyBuf,
		.config.phyBufLen = sizeof(myLedStrPhyBuf),
		.config.hw = &testSpi,
		.config.dmaCh = 0,
	};
	TEST_ASSERT_EQUAL_MESSAGE(tcode_badConfig, tledstr_init(&myLedStr3), "0 Pixel should return badConfig");

	/* No led type given */
	tledstr_t myLedStr4 = {
		.config.pixBuf = myLedStrPBuf,
		.config.pixNb = 3,
		.config.type = tledstr_none,
		.config.phyBuf = (void*)myLedStrPhyBuf,
		.config.phyBufLen = sizeof(myLedStrPhyBuf),
		.config.hw = &testSpi,
		.config.dmaCh = 0,
	};
	TEST_ASSERT_EQUAL_MESSAGE(tcode_notSupported, tledstr_init(&myLedStr4), "No LED type should return notSupported");

	/* NULL phy buffer */
	tledstr_t myLedStr5 = {
		.config.pixBuf = myLedStrPBuf,
		.config.pixNb = 3,
		.config.type = tledstr_ws2812b,
		.config.phyBuf = NULL,
		.config.phyBufLen = sizeof(myLedStrPhyBuf),
		.config.hw = &testSpi,
		.config.dmaCh = 0,
	};
	TEST_ASSERT_EQUAL_MESSAGE(tcode_badConfig, tledstr_init(&myLedStr5), "NULL phyBuf should return badConfig");

	/* 0 sized phy buffer */
	tledstr_t myLedStr6 = {
		.config.pixBuf = myLedStrPBuf,
		.config.pixNb = 3,
		.config.type = tledstr_ws2812b,
		.config.phyBuf = (void*)myLedStrPhyBuf,
		.config.phyBufLen = 0,
		.config.hw = &testSpi,
		.config.dmaCh = 0,
	};
	TEST_ASSERT_EQUAL_MESSAGE(tcode_badConfig, tledstr_init(&myLedStr6), "0 sized phyBuf should return badConfig");

	/* hw pointer NULL */
	tledstr_t myLedStr7 = {
		.config.pixBuf = myLedStrPBuf,
		.config.pixNb = 3,
		.config.type = tledstr_ws2812b,
		.config.phyBuf = (void*)myLedStrPhyBuf,
		.config.phyBufLen = sizeof(myLedStrPhyBuf),
		.config.hw = NULL,
		.config.dmaCh = 0,
	};
	TEST_ASSERT_EQUAL_MESSAGE(tcode_badConfig, tledstr_init(&myLedStr7), "NULL hw should return badConfig");

	/* DMA ch 255 */
	tledstr_t myLedStr8 = {
		.config.pixBuf = myLedStrPBuf,
		.config.pixNb = 3,
		.config.type = tledstr_ws2812b,
		.config.phyBuf = (void*)myLedStrPhyBuf,
		.config.phyBufLen = sizeof(myLedStrPhyBuf),
		.config.hw = &testSpi,
		.config.dmaCh = 0xFF,
	};
	TEST_ASSERT_EQUAL_MESSAGE(tcode_badConfig, tledstr_init(&myLedStr8), "255th dma channel should return badConfig");
}

void test_tledstr_initShouldSizeStuffCorrectly(void) {

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_MESSAGE(3, testLedStr.pixSize, "Pixel size should be 3 byte");
	TEST_ASSERT_EQUAL_MESSAGE(3*4, testLedStr.xferSize, "Transfer size should be 4 time longer");
}

void test_tledstr_initShouldCleanAllBuffer(void) {
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, testLedStrPixBuf, sizeof(testLedStrPixBuf), "pixBuf should be full of 1s");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, testLedStrPhyBuf, sizeof(testLedStrPhyBuf), "phyBuf should be full of 1s");

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");

	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0x00, testLedStrPixBuf, sizeof(testLedStrPixBuf), "pixBuf should now be full of 0s");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0x00, testLedStrPhyBuf, sizeof(testLedStrPhyBuf), "phyBuf should now be full of 0s");
}

void test_tledstr_initShouldResetState(void) {
	testLedStr.state = tledstr_state_unknown;
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_MESSAGE(tledstr_state_ready, testLedStr.state, "State should now be resetted");
}

void test_tledstr_initShouldResetCnt(void) {
	testLedStr.cnt = UINT16_MAX;

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_MESSAGE(0, testLedStr.cnt, "cnt should now be 0"); 
}

void test_tledstr_initShouldAssociateCorrectEncodeFunction(void) {
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_PTR_MESSAGE(WS2812B_encode, testLedStr.encode, "Encode function should be ready to use");
}

void test_tledstr_initShouldConfigurePinCorrectly(void) {

	/* ADD expected thal call here */
	TEST_IGNORE_MESSAGE("thal gpio needs to be defined");

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
}

void test_tledstr_initShouldInitSPICorrectly(void) {

	/* ADD expected thal call here */
	TEST_IGNORE_MESSAGE("thal spi needs to be defined");

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
}

void test_tledstr_initShouldInitDMACorrectly(void) {

	/* ADD expected thal call here */
	TEST_IGNORE_MESSAGE("thal dma needs to be defined");

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");


	// 	uint8_t myLedStrPBuf[WS2812B_PIXBUF_SIZE_BYTE(3)] = {};
	// 	uint32_t myLedStrPhyBuf[WS2812B_COLOR_NB *2] = {};
	// 	tledstr_t myLedStr = {
	// 		.config.pixBuf = myLedStrPBuf,
	// 		.config.pixNb = 3,
	// 		.config.type = tledstr_ws2812b,
	// 		.config.phyBuf = (void*)myLedStrPhyBuf,
	// 		.config.phyBufLen = sizeof(myLedStrPhyBuf),
	// 		.config.hw = USART1,
	// 		.config.loc = 2,
	// 		.config.dmaCh = 4,
	// 	};

	// 	emutil_findCMUClock_IgnoreAndReturn(cmuClock_USART1);
	// 	CMU_ClockEnable_Ignore();
	// 	GPIO_PinModeSet_Ignore();
	// 	USART_InitSync_Ignore();
	// 	tio_findByPeri_ExpectAndReturn(USART1, tio_pin_usart_tx, myLedStr.config.loc, TIO(gpioPortB, 0));

	// 	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&myLedStr), "Init should return success");
		
	// 	/* Descriptor 0 should be first pixel */
	// 	TEST_ASSERT_EQUAL_MESSAGE((3 * 4)-1,								myLedStr.ldma.desc[0].xfer.xferCnt,		"Transfer count should be all spi-encoded byte of a pixel (ie 3Col * 4)-1 as dma count start 1");	
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlSrcIncOne ,						myLedStr.ldma.desc[0].xfer.srcInc,		"Src increment should be 1 address");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlSizeByte,							myLedStr.ldma.desc[0].xfer.size,		"Transfer size should be 1 byte");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlBlockSizeUnit1,					myLedStr.ldma.desc[0].xfer.blockSize,	"Block size should be 1 arbitration");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[0].xfer.byteSwap,	"Byte swap is disabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlDstIncNone,						myLedStr.ldma.desc[0].xfer.dstInc,		"Dst increment should be 0");
	// 	TEST_ASSERT_EQUAL_PTR_MESSAGE(&myLedStrPhyBuf[0],					myLedStr.ldma.desc[0].xfer.srcAddr,		"Src should point to phyBuf");
	// 	TEST_ASSERT_EQUAL_PTR_MESSAGE(USART1_BASE +0x34,					myLedStr.ldma.desc[0].xfer.dstAddr,		"Dst should point to TXDATA (@offset 0x34)");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlStructTypeXfer,					myLedStr.ldma.desc[0].xfer.structType,	"Struct should be transfer type");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[0].xfer.structReq,	"Request should be disabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[0].xfer.decLoopCnt,	"Loop counter should not decrement");
	// 	TEST_ASSERT_EQUAL_MESSAGE(1,										myLedStr.ldma.desc[0].xfer.doneIfs,		"When done trigger IRQ enabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaLinkModeRel,							myLedStr.ldma.desc[0].xfer.linkMode,	"Should be in Linked mode");
	// 	TEST_ASSERT_EQUAL_MESSAGE(1,										myLedStr.ldma.desc[0].xfer.link,		"Link should be enabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(4,										myLedStr.ldma.desc[0].xfer.linkAddr,	"Should jump by +4");

	// 	/* Descriptor 1 should be the second pixel */
	// 	TEST_ASSERT_EQUAL_MESSAGE((3 * 4)-1,								myLedStr.ldma.desc[1].xfer.xferCnt,		"Transfer count should be all spi-encoded byte of a pixel (ie 3Col * 4)-1 as dma count start 1");	
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlSrcIncOne ,						myLedStr.ldma.desc[1].xfer.srcInc,		"Src increment should be 1 address");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlSizeByte,							myLedStr.ldma.desc[1].xfer.size,		"Transfer size should be 1 byte");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlBlockSizeUnit1,					myLedStr.ldma.desc[1].xfer.blockSize,	"Block size should be 1 arbitration");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[1].xfer.byteSwap,	"Byte swap is disabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlDstIncNone,						myLedStr.ldma.desc[1].xfer.dstInc,		"Dst increment should be 0");
	// 	TEST_ASSERT_EQUAL_PTR_MESSAGE(&((uint8_t*)myLedStrPhyBuf)[(3*4)],	myLedStr.ldma.desc[1].xfer.srcAddr,		"Src should point to phyBuf + encoded pix size");
	// 	TEST_ASSERT_EQUAL_PTR_MESSAGE(USART1_BASE +0x34,					myLedStr.ldma.desc[1].xfer.dstAddr,		"Dst should point to TXDATA (@offset 0x34)");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlStructTypeXfer,					myLedStr.ldma.desc[1].xfer.structType,	"Struct should be transfer type");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[1].xfer.structReq,	"Request should be disabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[1].xfer.decLoopCnt,	"Loop counter should not decrement");
	// 	TEST_ASSERT_EQUAL_MESSAGE(1,										myLedStr.ldma.desc[1].xfer.doneIfs,		"When done trigger IRQ enabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaLinkModeRel,							myLedStr.ldma.desc[1].xfer.linkMode,	"Should be in Linked mode");
	// 	TEST_ASSERT_EQUAL_MESSAGE(1,										myLedStr.ldma.desc[1].xfer.link,		"Link should be enabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(-4,										myLedStr.ldma.desc[1].xfer.linkAddr,	"Should jump by -4");

	// 	/* Descriptor 2 should be for reset sequence */
	// 	TEST_ASSERT_EQUAL_MESSAGE(40, sizeof(_tledstr_resetSeq), "reset sequence should be 40 byte long");
	// 	TEST_ASSERT_EQUAL_MESSAGE(sizeof(_tledstr_resetSeq)-1,				myLedStr.ldma.desc[2].xfer.xferCnt,		"Transfer count should be sizeof(_tledstr_resetSeq) -1");	
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlSrcIncOne ,						myLedStr.ldma.desc[2].xfer.srcInc,		"Src increment should be 1 address");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlSizeByte,							myLedStr.ldma.desc[2].xfer.size,		"Transfer size should be 1 byte");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlBlockSizeUnit1,					myLedStr.ldma.desc[2].xfer.blockSize,	"Block size should be 1 arbitration");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[2].xfer.byteSwap,	"Byte swap is disabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlDstIncNone,						myLedStr.ldma.desc[2].xfer.dstInc,		"Dst increment should be 0");
	// 	TEST_ASSERT_EQUAL_PTR_MESSAGE(_tledstr_resetSeq,					myLedStr.ldma.desc[2].xfer.srcAddr,		"Src should point to phyBuf + encoded pix size");
	// 	TEST_ASSERT_EQUAL_PTR_MESSAGE(USART1_BASE +0x34,					myLedStr.ldma.desc[2].xfer.dstAddr,		"Dst should point to TXDATA (@offset 0x34)");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCtrlStructTypeXfer,					myLedStr.ldma.desc[2].xfer.structType,	"Struct should be transfer type");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[2].xfer.structReq,	"Request should be disabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[2].xfer.decLoopCnt,	"Loop counter should not decrement");
	// 	TEST_ASSERT_EQUAL_MESSAGE(1,										myLedStr.ldma.desc[2].xfer.doneIfs,		"When done trigger IRQ enabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[2].xfer.linkMode,	"Should be in single mode");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[2].xfer.link,		"Link should be disabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.desc[2].xfer.linkAddr,	"Should jump by 0");

	// 	/* All descriptor share the same trigger configuration */
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaPeripheralSignal_USART1_TXBL,			myLedStr.ldma.conf.ldmaReqSel,			"Trigger should be USART1 TXBL");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0, 										myLedStr.ldma.conf.ldmaCtrlSyncPrsClrOff,"We dont use PRS");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0, 										myLedStr.ldma.conf.ldmaCtrlSyncPrsClrOn,"We dont use PRS");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0, 										myLedStr.ldma.conf.ldmaCtrlSyncPrsSetOff,"We dont use PRS");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0, 										myLedStr.ldma.conf.ldmaCtrlSyncPrsSetOn,"We dont use PRS");
	// 	TEST_ASSERT_EQUAL_MESSAGE(false, 									myLedStr.ldma.conf.ldmaReqDis,			"Auto Request is disabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(false, 									myLedStr.ldma.conf.ldmaDbgHalt,			"Debug halt is disabled");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCfgArbSlotsAs1,						myLedStr.ldma.conf.ldmaCfgArbSlots,		"Arbiration slot 1 should be used");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCfgSrcIncSignPos,						myLedStr.ldma.conf.ldmaCfgSrcIncSign,	"Src increment should be positive");
	// 	TEST_ASSERT_EQUAL_MESSAGE(ldmaCfgDstIncSignPos,						myLedStr.ldma.conf.ldmaCfgDstIncSign,	"Dst increment should be positive (actually we dont care)");
	// 	TEST_ASSERT_EQUAL_MESSAGE(0,										myLedStr.ldma.conf.ldmaLoopCnt,			"Dma should not loop");
}


/* -------------------------------- +
|	pixel set						|
+ -------------------------------- */
void test_tledstr_pixelSetShouldHandleNull(void) {
	uint8_t bidonDessence;

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, tledstr_pixelSet(NULL, NULL, 1, 1), "pixelSet: all null");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, tledstr_pixelSet(NULL, (void*)&bidonDessence, 1, 1), "pixelSet: tledstr null");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, tledstr_pixelSet((tledstr_t*)&bidonDessence, NULL, 1, 1), "pixelSet: src null");
}

void test_tledstr_pixelSetShouldVerifyReadyness(void) {
	/* Purposefully not inited */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_notReady, tledstr_pixelSet(&testLedStr, (void*)&testInput, 0, 3), "SetPixel should return notReady");
}

void test_tledstr_pixelSetShouldGracefullyHandle0len(void) {
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_pixelSet(&testLedStr, (void*)&testInput, 0, 0), "setPixel should silently return");
}

void test_tledstr_pixelSetShouldCopyFromSrcAccordingToType(void) {

	/* WS2812B: GRB */
	tcol_rgb_t input[3] = {	[0].r=0xFF,	[0].g=0xEE,	[0].b=0xDD,
							[1].r=0xAA,	[1].g=0x55,	[1].b=0x00,
							[2].r=0x01,	[2].g=0x23,	[2].b=0x45};

	uint8_t expected[WS2812B_PIXBUF_SIZE_BYTE(3)] = {	0xEE,	0xFF,	0xDD,
														0x55,	0xAA,	0x00,
														0x23,	0x01,	0x45};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_pixelSet(&testLedStr, (void*)input, 0, 3), "SetPixel should return success");
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(expected, testLedStr.config.pixBuf, WS2812B_PIXBUF_SIZE_BYTE(3), "Data should be copied in correct order");

	/* SK6812: GRBW */
}

void test_tledstr_pixelSetShouldRespectAbsoluteLenOfString(void) {

	uint8_t expected[WS2812B_PIXBUF_SIZE_BYTE(11)] = {};
	uint8_t actual[WS2812B_PIXBUF_SIZE_BYTE(11)] = {};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_pixelSet(&testLedStr, (void*)&expected, 0, 10),		"SetPixel should return success when asked for correct nb of pixel");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_outOfRange, tledstr_pixelSet(&testLedStr, (void*)&expected, 0, 11),		"SetPixel should return outOfRange when asked for more than configured pixNb");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_outOfRange, tledstr_pixelSet(&testLedStr, (void*)&expected, 5, 6),		"SetPixel should return outOfRange when request goes beyond pixBuf");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_outOfRange, tledstr_pixelSet(&testLedStr, (void*)&expected, 11, 1),		"SetPixel should return outOfRange when request starts beyond pixBuf");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_outOfRange, tledstr_pixelSet(&testLedStr, (void*)&expected, 10, 0),		"SetPixel should return outOfRange when request starts beyond pixBuf but size is sneaky");
}

void test_tledstr_pixelSetShouldBeAbleToCopySubSetOfPixels(void) {
	tcol_rgb_t input[2] = {	[0].r=0xFF,	[0].g=0xFF,	[0].b=0xFF,
							[1].r=0xAA,	[1].g=0xAA,	[1].b=0xAA	};
	uint8_t expected[WS2812B_PIXBUF_SIZE_BYTE(2)] = { 	0xFF, 0xFF, 0xFF,
														0xAA, 0xAA, 0xAA};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	/* Wipe pix buffer */
	memset(testLedStrPixBuf, 0, sizeof(testLedStrPixBuf));

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_pixelSet(&testLedStr, (void*)expected, 1, 2), "SetPixel should return success");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0, &testLedStrPixBuf[0], WS2812B_PIXBUF_SIZE_BYTE(1), "pixel 0 should be unchanged");
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(expected, &testLedStrPixBuf[WS2812B_PIXBUF_SIZE_BYTE(1)], WS2812B_PIXBUF_SIZE_BYTE(2), "pixel 1 & 2 should be changed");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0, &testLedStrPixBuf[WS2812B_PIXBUF_SIZE_BYTE(3)], WS2812B_PIXBUF_SIZE_BYTE(9-3), "pixel 3 to 9 should be unchanged");
}


/* -------------------------------- +
|	pixel get						|
+ -------------------------------- */
void test_tledstr_pixelGetShouldHandleNull(void) {
	uint8_t bidonDessence;

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, tledstr_pixelGet(NULL, NULL, 1, 1), "pixelGet: all null");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, tledstr_pixelGet(NULL, (void*)&bidonDessence, 1, 1), "pixelGet: tledstr null");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, tledstr_pixelGet((tledstr_t*)&bidonDessence, NULL, 1, 1), "pixelGet: dst null");
}

void test_tledstr_pixelGetShouldVerifyReadyness(void) {
	/* Purposefully not inited */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_notReady, tledstr_pixelGet(&testLedStr, (void*)&testInput, 0, 3), "pixelGet should return notReady");
}

void test_tledstr_pixelGetShouldGracefullyHandle0len(void) {
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_pixelGet(&testLedStr, (void*)&testInput, 0, 0), "getPixel should silently return");
}

void test_tledstr_pixelGetShouldCopyToDstAccordingToType(void) {

	/* WS2812B: GRB */
	uint8_t input[WS2812B_PIXBUF_SIZE_BYTE(3)] =	{	0x67,	0xFF,	0xEF,
														0x12,	0x34,	0x56,
														0x78,	0x9A,	0xBC};
	uint8_t expected[WS2812B_PIXBUF_SIZE_BYTE(3)] = {	0xFF,	0x67,	0xEF,
														0x34,	0x12,	0x56,
														0x9A,	0x78,	0xBC};
	uint8_t actual[WS2812B_PIXBUF_SIZE_BYTE(5)] = {};					/* Larger so we can inspect surrounding mem */

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	/* After init, put data in internal buffer */
	memcpy(testLedStrPixBuf, input, sizeof(input));
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(input, testLedStrPixBuf, WS2812B_PIXBUF_SIZE_BYTE(3), "data should be ready in pixbuf");

	/* Asserts */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_pixelGet(&testLedStr, (void*)&actual, 0, 3), "pixelGet should return success");
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(expected, actual, WS2812B_PIXBUF_SIZE_BYTE(3), "data should be in user's mem in correct order");

	/* SK6812: GRBW */
}

void test_tledstr_getPixelShouldRespectAbsoluteLenOfString(void) {

	uint8_t expected[WS2812B_PIXBUF_SIZE_BYTE(11)] = {};
	uint8_t actual[WS2812B_PIXBUF_SIZE_BYTE(11)] = {};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_pixelGet(&testLedStr, (void*)&actual, 0, 10),			"GetPixel should return success when asked for correct nb of pixel");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_outOfRange, tledstr_pixelGet(&testLedStr, (void*)&actual, 0, 11),		"GetPixel should return outOfRange when asked for more than configured pixNb");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_outOfRange, tledstr_pixelGet(&testLedStr, (void*)&actual, 5, 6),		"GetPixel should return outOfRange when request goes beyond pixBuf");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_outOfRange, tledstr_pixelGet(&testLedStr, (void*)&actual, 11, 1),		"GetPixel should return outOfRange when request starts beyond pixBuf");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_outOfRange, tledstr_pixelGet(&testLedStr, (void*)&actual, 10, 0),		"GetPixel should return outOfRange when request starts beyond pixBuf but size is sneaky");
}

void test_tledstr_pixelGetShouldBeAbleToCopySubSetOfPixels(void) {
	uint8_t expected[WS2812B_PIXBUF_SIZE_BYTE(2)] = { 	0xFF, 0xFF, 0xFF,
														0xBB, 0xBB, 0xBB};
	uint8_t actual[WS2812B_PIXBUF_SIZE_BYTE(5)] = {};						/* Larger so we can inspect surrounding mem */

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	/* Wipe pix buffer */
	memset(testLedStrPixBuf, 0, sizeof(testLedStrPixBuf));
	/* Insert data to get */
	testLedStrPixBuf[WS2812B_PIXBUF_SIZE_BYTE(1)+0]=0xFF;	testLedStrPixBuf[WS2812B_PIXBUF_SIZE_BYTE(1)+1]=0xFF;	testLedStrPixBuf[WS2812B_PIXBUF_SIZE_BYTE(1)+2]=0xFF;
	testLedStrPixBuf[WS2812B_PIXBUF_SIZE_BYTE(2)+0]=0xBB;	testLedStrPixBuf[WS2812B_PIXBUF_SIZE_BYTE(2)+1]=0xBB;	testLedStrPixBuf[WS2812B_PIXBUF_SIZE_BYTE(2)+2]=0xBB;

	/* Asserts */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_pixelGet(&testLedStr, (void*)actual, 1, 2), "GetPixel should return success");
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(expected, actual, WS2812B_PIXBUF_SIZE_BYTE(2), "pixel data should be in user's mem");
}


/* -------------------------------- +
|	frame send						|
+ -------------------------------- */
void test_tledstr_frameSendShouldHandleNull(void) {
	/* Minimal testing */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, tledstr_frameSend(NULL), "sendFrame");
}

void test_tledstr_frameSendShouldVerifyReadyness(void) {
	/* Purposefully not inited */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_notReady, tledstr_frameSend(&testLedStr), "frameSend should return notReady");
}

void test_tledstr_frameSendEncodeOnly1PixelWhenOnly1Present(void) {
	uint8_t myLedStrPBuf[WS2812B_PIXBUF_SIZE_BYTE(1)] = {};
	uint32_t myLedStrPhyBuf[6] = { };
	tledstr_t myLedStr = {
		.config.pixBuf = myLedStrPBuf,
		.config.pixNb = 1,
		.config.type = tledstr_ws2812b,
		.config.phyBuf = (void*)myLedStrPhyBuf,
		.config.phyBufLen = sizeof(myLedStrPhyBuf)/2,		/* ONly give half for the tledstr */
		.config.hw = &testSpi,
		.config.dmaCh = 5,
	};
	tcol_rgb_t input[1] = { [0].r=0xCB, [0].g=0xBC, [0].b=0xFF};
	
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&myLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_frameSend(&myLedStr), "sendFrame should return success");
	TEST_ASSERT_NOT_EQUAL_HEX32_MESSAGE(0 , myLedStrPhyBuf[0], "only 1 pixel wide should be changed");
	TEST_ASSERT_NOT_EQUAL_HEX32_MESSAGE(0 , myLedStrPhyBuf[1], "only 1 pixel wide should be changed");
	TEST_ASSERT_NOT_EQUAL_HEX32_MESSAGE(0 , myLedStrPhyBuf[2], "only 1 pixel wide should be changed");
	TEST_ASSERT_EACH_EQUAL_HEX32_MESSAGE(0, &myLedStrPhyBuf[3], 3, "and remaining mem should be unchanged");
}

void test_tledstr_frameSendShouldSetState(void) {
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_frameSend(&testLedStr), "frameSend should return success");
	TEST_ASSERT_EQUAL_MESSAGE(tledstr_state_txing, testLedStr.state, "State should now be transmiting");
}

void test_tledstr_frameSendShouldEncodeInTheEntirePhyBufAndInReverseOrder(void) {

	/* We expected pixel in reverse order, in grb color encoded */
	const uint16_t expected[WS2812B_PHYBUF_SIZE_BYTE(TEST_PIXNB)] = {	0x88EE, 0x88EE,	0x88EE, 0x88EE, 0x88EE, 0x88EE,
																		0x88E8, 0x88E8,	0x88E8, 0x88E8,	0x88E8, 0x88E8,
																		0x888E, 0x888E, 0x888E, 0x888E, 0x888E, 0x888E};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");

	/* Make pixel data ready, use last 3 pixel so they are first in phy buffer */
	testLedStrPixBuf[(TEST_PIXNB*3)-9] = 0x11;	testLedStrPixBuf[(TEST_PIXNB*3)-8] = 0x11;	testLedStrPixBuf[(TEST_PIXNB*3)-7] = 0x11;
	testLedStrPixBuf[(TEST_PIXNB*3)-6] = 0x22;	testLedStrPixBuf[(TEST_PIXNB*3)-5] = 0x22;	testLedStrPixBuf[(TEST_PIXNB*3)-4] = 0x22;
	testLedStrPixBuf[(TEST_PIXNB*3)-3] = 0x33;	testLedStrPixBuf[(TEST_PIXNB*3)-2] = 0x33;	testLedStrPixBuf[(TEST_PIXNB*3)-1] = 0x33;

	
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_frameSend(&testLedStr), "frameSend should return success");
	TEST_ASSERT_EQUAL_HEX16_ARRAY_MESSAGE(expected, testLedStrPhyBuf, (sizeof(expected)/2), "Update should have encoded in reverse order");
}

void test_tledstr_frameSendShouldResetCnt(void) {
	/* Setup */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	testLedStr.cnt = 22;
	TEST_ASSERT_EQUAL_MESSAGE(22, testLedStr.cnt, "cnt should be 22 now");

	/* Test */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_frameSend(&testLedStr), "frameSend should return success");
	TEST_ASSERT_EQUAL_MESSAGE(0, testLedStr.cnt, "cnt should be 0 now");
}


/* -------------------------------- +
|	dma cb							|
+ -------------------------------- */
void test_tledstr_dmacbShouldHandleNull(void) {
	tledstr_dma_cb(NULL);
	TEST_PASS();		/* execution gracefully handled a null, thus pass */
}

void test_tledstr_dmacbShouldVerifyReadyness(void) {
	/* Purposefully not inited */
	tledstr_dma_cb(&testLedStr);
	TEST_PASS();		/* execution gracefully handled it, thus pass */
}

void test_tledstr_dmacbShouldEncodeUntilAllPixelAreSent(void) {
	/* Setup */
	TEST_ASSERT_EQUAL_MESSAGE(0, testLedStr.cnt, "cnt should be at zero first");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");

	/* Test */
	TEST_ASSERT_EQUAL_MESSAGE(TEST_PIXNB, testLedStr.cnt, "cnt should be at TEST_PIXNB now");
}

void test_tledstr_dmacbShouldSetState(void) {
	/* Setup */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, tledstr_init(&testLedStr), "Init should return success");
	testLedStr.cnt = TEST_PIXNB;

	/* Test */
	TEST_ASSERT_EQUAL_MESSAGE(tledstr_state_ready, testLedStr.state, "state should be ready after reset seq was sent");
}

