#include <unity.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <mock_tbug.h>
#include <mock_ttime.h>
#include <mock_tatomic.h>
#define TREC_AUTO_EN		1
#include <trec.h>

/* allocate control mem here instead of inside source file, for inspection */
trec_t __trec;

#define	MEM_WATERMARK 			(0x52)
#define MY_CUSTOM_EVENT_TYPE	(0x22)

/* -------------------------------- +
|									|
|	Helper function					|
|									|
+ -------------------------------- */
void setUp(void) {
	__trec.in = UINT32_MAX;
	__trec.out = UINT32_MAX;
	__trec.cnt = UINT32_MAX;
	__trec.bufOVF = false;
	memset(__trec.buf, MEM_WATERMARK, TREC_BUF_SIZE_BYTE);
}

void tearDown(void) {

}

/* -------------------------------- +
|									|
|	trec Unit test					|
|									|
+ -------------------------------- */
void test_trec_initShouldInitRTCCwhenAsked(void) {
	ttime_init_ExpectAndReturn( tcode_success);
	tbug_init_Expect();

	trec_init();
}

void test_trec_initShouldFlushBuffer(void) {
	ttime_init_IgnoreAndReturn(tcode_success);
	tbug_init_Ignore();

	trec_init();

	TEST_ASSERT_EQUAL_MESSAGE(0, __trec.in, "writing index should be reseted");
	TEST_ASSERT_EQUAL_MESSAGE(0, __trec.out, "reading index should be reseted");
	TEST_ASSERT_EQUAL_MESSAGE(0, __trec.cnt, "count should be reseted");
	TEST_ASSERT_EACH_EQUAL_UINT8_MESSAGE(0, __trec.buf, TREC_BUF_SIZE_BYTE, "buf should be empty");
}

void test_trec_rec0ShouldWriteToBuffer(void) {
	ttime_init_IgnoreAndReturn(tcode_success);
	tbug_init_Ignore();
	tbug_format_Ignore();
	trec_init();

	trec_rec0(TREC_USER_TEST, TREC_EVT_INIT);

	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0, (uint32_t)__trec.buf[0], "timestamp should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_USER_TEST, __trec.buf[4], "user should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_EVT_INIT, __trec.buf[5], "event should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(trec_recType_empty, (uint16_t)__trec.buf[6], "event ctl should be written");
	TEST_ASSERT_EQUAL_MESSAGE(0, __trec.out, "reading index should not have changed");
	TEST_ASSERT_EQUAL_MESSAGE(8, __trec.cnt, "count should hold some data");
	TEST_ASSERT_EQUAL_MESSAGE(8, __trec.in, "writing index should have advanced");
}

void test_trec_rec1ShouldWriteToBuffer(void) {
	/* Setup */
	ttime_init_IgnoreAndReturn(tcode_success);
	tbug_init_Ignore();
	trec_init();
	
	/* Action */
	trec_rec1(TREC_USER_TEST, TREC_EVT_MSG, ('A'<<24)|('l'<<16)|('l'<<8)|'o');

	/* Measure */
	trec_rec_t actual = {};
	uint32_t * actualPtrU32 = (uint32_t*)&__trec.buf[sizeof(trec_rec_t)];
	memcpy(&actual, __trec.buf, sizeof(trec_rec_t));

	/* Test */
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0, actual.time, "timestamp should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_USER_TEST, actual.user, "user should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_EVT_MSG, actual.event, "event should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(trec_recType_mem, actual.type, "event ctl should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(sizeof(uint32_t), actual.len, "data len should be written");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(('A'<<24)|('l'<<16)|('l'<<8)|'o', *actualPtrU32, "param 0 should be written");
}

void test_trec_rec2ShouldWriteToBuffer(void) {
	/* Setup */
	ttime_init_IgnoreAndReturn(tcode_success);
	tbug_init_Ignore();
	trec_init();

	/* Action */
	trec_rec2(TREC_USER_TEST, TREC_EVT_MSG, ('A'<<24)|('l'<<16)|('l'<<8)|'o', 0xFEDCBA98);

	/* Measure */
	trec_rec_t actual = {};
	uint32_t * actualPtrU32 = (uint32_t*)&__trec.buf[sizeof(trec_rec_t)];
	memcpy(&actual, __trec.buf, sizeof(trec_rec_t));

	/* Test */
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0, actual.time, "timestamp should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_USER_TEST, actual.user, "user should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_EVT_MSG, actual.event, "event should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(trec_recType_mem, actual.type, "event ctl should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(2*sizeof(uint32_t), actual.len, "data len should be written");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(('A'<<24)|('l'<<16)|('l'<<8)|'o', actualPtrU32[0], "param 0 should be written");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0xFEDCBA98, actualPtrU32[1], "param 1 should be written");
}

void test_trec_rec3ShouldWriteToBuffer(void) {
	/* Setup */
	ttime_init_IgnoreAndReturn(tcode_success);
	tbug_init_Ignore();
	trec_init();

	/* Action */
	trec_rec3(TREC_USER_TEST, TREC_EVT_MSG, ('A'<<24)|('l'<<16)|('l'<<8)|'o', 0xFEDCBA98, 0x55004412);

	/* Measure */
	trec_rec_t actual = {};
	uint32_t * actualPtrU32 = (uint32_t*)&__trec.buf[sizeof(trec_rec_t)];
	memcpy(&actual, __trec.buf, sizeof(trec_rec_t));

	/* Test */
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0, actual.time, "timestamp should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_USER_TEST, actual.user, "user should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_EVT_MSG, actual.event, "event should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(trec_recType_mem, actual.type, "event ctl should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(3*sizeof(uint32_t), actual.len, "data len should be written");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(('A'<<24)|('l'<<16)|('l'<<8)|'o', actualPtrU32[0], "param 0 should be written");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0xFEDCBA98, actualPtrU32[1], "param 1 should be written");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0x55004412, actualPtrU32[2], "param 2 should be written");
}

void test_trec_rec4ShouldWriteToBuffer(void) {
	/* Setup */
	ttime_init_IgnoreAndReturn(tcode_success);
	tbug_init_Ignore();
	trec_init();

	/* Action */
	trec_rec4(TREC_USER_TEST, TREC_EVT_MSG, ('A'<<24)|('l'<<16)|('l'<<8)|'o', 0xFEDCBA98, 0x55004412, 0xFFFE7FFF);

	/* Measure */
	trec_rec_t actual = {};
	uint32_t * actualPtrU32 = (uint32_t*)&__trec.buf[sizeof(trec_rec_t)];
	memcpy(&actual, __trec.buf, sizeof(trec_rec_t));

	/* Test */
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0, actual.time, "timestamp should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_USER_TEST, actual.user, "user should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_EVT_MSG, actual.event, "event should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(trec_recType_mem, actual.type, "event ctl should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(4*sizeof(uint32_t), actual.len, "data len should be written");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(('A'<<24)|('l'<<16)|('l'<<8)|'o', actualPtrU32[0], "param 0 should be written");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0xFEDCBA98, actualPtrU32[1], "param 1 should be written");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0x55004412, actualPtrU32[2], "param 2 should be written");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0xFFFE7FFF, actualPtrU32[3], "param 3 should be written");
}

void test_trec_recMemShouldWriteToBuffer(void) {
	/* Setup */
	ttime_init_IgnoreAndReturn(tcode_success);
	tbug_init_Ignore();
	trec_init();
	char expected[] = "This is a string of char to test the trec memory record type, I dont really know what to say more... haha";

	/* Action */
	trec_recMem(TREC_USER_TEST, MY_CUSTOM_EVENT_TYPE, expected, sizeof(expected));

	/* Measure */
	trec_rec_t actual = {};
	memcpy(&actual, __trec.buf, sizeof(trec_rec_t));
	
	/* Test */
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0, actual.time, "timestamp should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_USER_TEST, actual.user, "user should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(MY_CUSTOM_EVENT_TYPE, actual.event, "event should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(trec_recType_mem, actual.type, "event ctl should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(sizeof(expected), actual.len, "data len should be written");
	TEST_ASSERT_EQUAL_CHAR_ARRAY_MESSAGE(expected, (char*)&__trec.buf[sizeof(trec_rec_t)], sizeof(expected), "mem content should be copied");
}

void test_trec_recMsgShouldWriteToBuffer(void) {
	/* Setup */
	ttime_init_IgnoreAndReturn(tcode_success);
	tbug_init_Ignore();
	trec_init();
	const char expected[] = "shorter test string :P";


	/* Action */
	trec_recMsg(TREC_USER_TEST, TREC_EVT_MSG, expected);

	/* Measure */
	trec_rec_t actual = {};
	memcpy(&actual, __trec.buf, sizeof(trec_rec_t));

	/* Test */
	TEST_ASSERT_EQUAL_HEX32_MESSAGE(0, actual.time, "timestamp should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_USER_TEST, actual.user, "user should be written");
	TEST_ASSERT_EQUAL_HEX8_MESSAGE(TREC_EVT_MSG, actual.event, "event should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(trec_recType_msg, actual.type, "event ctl should be written");
	TEST_ASSERT_EQUAL_HEX16_MESSAGE(sizeof(expected), actual.len, "data len should be written");
	TEST_ASSERT_EQUAL_CHAR_ARRAY_MESSAGE(expected, (char*)&__trec.buf[sizeof(trec_rec_t)], sizeof(expected), "msg content should be copied");
}

void test_trec_allRecShouldDoNothingWhenMemoryFull(void) {
	/* Setup */
	ttime_init_IgnoreAndReturn(tcode_success);
	tbug_init_Ignore();
	trec_init();
	__trec.cnt = sizeof(__trec.buf) -1;

	/* Action */
	trec_rec0(TREC_USER_TEST, TREC_EVT_INIT);
	trec_rec1(TREC_USER_TEST, TREC_EVT_INIT, 0);
	trec_rec2(TREC_USER_TEST, TREC_EVT_INIT, 0, 0);
	trec_rec3(TREC_USER_TEST, TREC_EVT_INIT, 0, 0, 0);
	trec_rec4(TREC_USER_TEST, TREC_EVT_INIT, 0, 0, 0, 0);
	trec_recMem(TREC_USER_TEST, TREC_EVT_INIT, NULL, 0);
	trec_recMsg(TREC_USER_TEST, TREC_EVT_INIT, "pwet");

	/* Measure */

	/* Test */
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0, __trec.buf, sizeof(__trec.buf), "buffer should be empty");
}

void test_trec_bufferOVFshouldBeHandledGraciously(void) {
	/* Setup */
	ttime_init_IgnoreAndReturn(tcode_success);
	tbug_init_Ignore();
	trec_init();
	uint8_t expected[4095-sizeof(trec_rec_t)] = { [0 ... 4095-sizeof(trec_rec_t)-1] = 0xFF };
	uint8_t extra[3] = { 0x44, 0x33, 0x22};

	/* Action */
	trec_recMem(TREC_USER_TEST, TREC_EVT_MEM, expected, sizeof(expected));
	TEST_ASSERT_EQUAL_MESSAGE(false, __trec.bufOVF, "overflow flag should be off");
	
	/* Test */
	trec_recMem(TREC_USER_TEST, TREC_EVT_MEM, extra, sizeof(extra));		/* This should overflow but do nothing */
	TEST_ASSERT_EQUAL_MESSAGE(true, __trec.bufOVF, "overflow flag should now be on");
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(expected, &__trec.buf[sizeof(trec_rec_t)], sizeof(expected), "entire buffer should be still full by expected");

	/* empty buffer, then check if ovf is still on (should be latching) */
}

