#include <unity.h>
#include <stdbool.h>
#include <stdint.h>


void setUp(void) {

}
void tearDown(void) {

}

void test_env_VarsAreSizedCorrectly(void) {

	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(int), "int");
	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(void *), "pointer");
	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(long), "long");
	TEST_ASSERT_EQUAL_MESSAGE(8, sizeof(long long), "long long");

	TEST_ASSERT_EQUAL_MESSAGE(1, sizeof(uint8_t), "uint8_t");
	TEST_ASSERT_EQUAL_MESSAGE(2, sizeof(uint16_t), "uint16_t");
	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(uint32_t), "uint32_t");
	TEST_ASSERT_EQUAL_MESSAGE(8, sizeof(uint64_t), "uint64_t");

	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(float), "float");
	TEST_ASSERT_EQUAL_MESSAGE(8, sizeof(double), "double");

	TEST_ASSERT_EQUAL_MESSAGE(1, sizeof(bool), "bool");
}

