#include <unity.h>

#include <string.h>
#include <dev/ws2812b.h>

/* -------------------------------- +
|									|
|	Helper function					|
|									|
+ -------------------------------- */
#define	TEST_MEMGAP_SIZE		(10)

void setUp(void) {

}

void tearDown(void) {
	
}


/* -------------------------------- +
|									|
|	WS2812B Unit test				|
|									|
+ -------------------------------- */
void test_WS2812B_constantDataShouldBeAsExpected(void) {
	TEST_ASSERT_EQUAL_MESSAGE(0x8888, WS2812B_NIB_0000, "0000");
	TEST_ASSERT_EQUAL_MESSAGE(0x888E, WS2812B_NIB_0001, "0001");
	TEST_ASSERT_EQUAL_MESSAGE(0x88E8, WS2812B_NIB_0010, "0010");
	TEST_ASSERT_EQUAL_MESSAGE(0x88EE, WS2812B_NIB_0011, "0011");
	TEST_ASSERT_EQUAL_MESSAGE(0x8E88, WS2812B_NIB_0100, "0100");
	TEST_ASSERT_EQUAL_MESSAGE(0x8E8E, WS2812B_NIB_0101, "0101");
	TEST_ASSERT_EQUAL_MESSAGE(0x8EE8, WS2812B_NIB_0110, "0110");
	TEST_ASSERT_EQUAL_MESSAGE(0x8EEE, WS2812B_NIB_0111, "0111");
	TEST_ASSERT_EQUAL_MESSAGE(0xE888, WS2812B_NIB_1000, "1000");
	TEST_ASSERT_EQUAL_MESSAGE(0xE88E, WS2812B_NIB_1001, "1001");
	TEST_ASSERT_EQUAL_MESSAGE(0xE8E8, WS2812B_NIB_1010, "1010");
	TEST_ASSERT_EQUAL_MESSAGE(0xE8EE, WS2812B_NIB_1011, "1011");
	TEST_ASSERT_EQUAL_MESSAGE(0xEE88, WS2812B_NIB_1100, "1100");
	TEST_ASSERT_EQUAL_MESSAGE(0xEE8E, WS2812B_NIB_1101, "1101");
	TEST_ASSERT_EQUAL_MESSAGE(0xEEE8, WS2812B_NIB_1110, "1110");
	TEST_ASSERT_EQUAL_MESSAGE(0xEEEE, WS2812B_NIB_1111, "1111");

	TEST_ASSERT_EQUAL_MESSAGE(3, WS2812B_PIXSIZE_BYTE, "WS2812B is 3x8bit color");
	TEST_ASSERT_EQUAL_MESSAGE(3*4, WS2812B_ENCODEDSIZE_BYTE, "WS2812B ratio should be 4");
	TEST_ASSERT_EQUAL_MESSAGE(8333, WS2812B_MAX_DEV_IN_STRING_FPS(1), "Maximum number of WS2812B at 1fps");
	TEST_ASSERT_EQUAL_MESSAGE(277, WS2812B_MAX_DEV_IN_STRING_FPS(30), "Maximum number of WS2812B at 30fps");
	TEST_ASSERT_EQUAL_MESSAGE(10*3, WS2812B_PIXBUF_SIZE_BYTE(10), "WS2812B 10pixel should take 30byte");
	TEST_ASSERT_EQUAL_MESSAGE(10*3*4, WS2812B_PHYBUF_SIZE_BYTE(10), "WS2812B encoded pixel should take 4x pixSize");
}

void test_WS2812B_allFctShouldHandleNull(void) {
	uint8_t bidonDhuile;

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, WS2812B_encode(NULL, NULL, 0), "encode: all null");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, WS2812B_encode(NULL, &bidonDhuile, 0), "encode: dest null");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, WS2812B_encode(&bidonDhuile, NULL, 0), "encode: src null");
}

void test_WS2812B_encodeShouldHandle0len(void) {
	uint8_t bidonDlaveGlace = 0xFF;

	TEST_ASSERT_EQUAL(tcode_success, WS2812B_encode(&bidonDlaveGlace, &bidonDlaveGlace, 0));
	TEST_ASSERT_EQUAL_MESSAGE(0xFF, bidonDlaveGlace, "Should not have touched memory");
}

void test_WS2812B_encodeShouldCorrectlyEncodeData(void) {
	/* Current bit encoding ratio is 4:1 , unchangeable */
	uint8_t input[8] =		{	0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF};
	uint32_t expected[8] =	{	0x8888888E,
								0x88E888EE,
								0x8E888E8E,
								0x8EE88EEE,
								0xE888E88E,
								0xE8E8E8EE,
								0xEE88EE8E,
								0xEEE8EEEE};
	uint32_t actual[8] =	{	[0 ... 7] = 0x00000000};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, WS2812B_encode((void *)actual, input, 8), "Encode should return success");
	TEST_ASSERT_EQUAL_HEX32_ARRAY_MESSAGE(expected, actual, 8, "Should have correctly encoded data");
}

void test_WS2812B_shouldNotWriteInAdjacentMemory(void) {
	uint8_t input[2] = {		0xFE, 0xDC};
	uint32_t expected[2] = {	0xEEEEEEE8, 0xEE8EEE88};
	struct {
		uint8_t pre[TEST_MEMGAP_SIZE];
		uint32_t actual[2];
		uint8_t post[TEST_MEMGAP_SIZE];
	}testMem = { .pre = {}, .actual = {}, .post = {}};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, WS2812B_encode((void*)testMem.actual, input, 2), "Encode should return success");
	TEST_ASSERT_EQUAL_HEX32_ARRAY_MESSAGE(expected, testMem.actual, 2, "Encode should have correctly encoded");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0, testMem.pre, TEST_MEMGAP_SIZE, "Should not have touch pre memory");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0, testMem.post, TEST_MEMGAP_SIZE, "Should not have touch post memory");
}

