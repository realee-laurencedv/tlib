#include <unity.h>

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <trbuf.h>

/* test buffer const parameters */
#define	TEST_TRBUF_ELECNT		10		/* Must be multiple of 2 */
#define	TEST_TRBUF2_ELECNT		10
#define	TEST_TRBUF3_ELECNT		4096
#define	TEST_TRBUF4_ELECNT		128
#define	TEST_PREPOSTBUF_SIZE	256
#define TEST_TRBUF_MEMDEFVAL_	(0)		/* This is private, so i manually copy it here */

/* Test memory array */
static struct __attribute__((packed)) {
	const uint8_t test_src[10];

	uint8_t test_prebuf[TEST_PREPOSTBUF_SIZE];
	uint8_t test_buf[TEST_TRBUF_ELECNT];
	uint8_t test_postbuf[TEST_PREPOSTBUF_SIZE];
	trbuf_t test_trbuf;

	uint8_t test_prebuf2[TEST_PREPOSTBUF_SIZE];
	uint32_t test_buf2[TEST_TRBUF2_ELECNT];
	uint8_t test_postbuf2[TEST_PREPOSTBUF_SIZE];
	trbuf_t test_trbuf2;

	uint8_t test_prebuf3[TEST_PREPOSTBUF_SIZE];
	uint8_t test_buf3[TEST_TRBUF3_ELECNT];
	uint8_t test_postbuf3[TEST_PREPOSTBUF_SIZE];
	trbuf_t test_trbuf3;

	uint8_t test_prebuf4[TEST_PREPOSTBUF_SIZE];
	uint32_t test_buf4[TEST_TRBUF4_ELECNT];
	uint8_t test_postbuf4[TEST_PREPOSTBUF_SIZE];
	trbuf_t test_trbuf4;
}fakeMem = {
	.test_src = {0xFF, 1, 2, 3, 4, 5, 6, 7, 8, 9},
	.test_trbuf.eleType =	ttype_u8,
	.test_trbuf.len =		TEST_TRBUF_ELECNT,
	.test_trbuf.buf =		fakeMem.test_buf,

	.test_trbuf2.eleType =	ttype_u32,
	.test_trbuf2.len =		TEST_TRBUF2_ELECNT,
	.test_trbuf2.buf =		fakeMem.test_buf2,

	.test_trbuf3.eleType =	ttype_u8,
	.test_trbuf3.len =		TEST_TRBUF3_ELECNT,
	.test_trbuf3.buf =		fakeMem.test_buf3,

	.test_trbuf4.ovfSel =	TRBUF_OVFSEL_DISCARD,
	.test_trbuf4.udfSel =	TRBUF_UDFSEL_REPEAT,
	.test_trbuf4.flushSel =	TRBUF_FLUSHSEL_COMPLETE,
	.test_trbuf4.eleType =	ttype_u32,
	.test_trbuf4.len =		TEST_TRBUF4_ELECNT,
	.test_trbuf4.buf =		fakeMem.test_buf4,
};


/*--------------------------------- +
|									|
|	Test Helper						|
|									|
+ -------------------------------- */
void setUp(void) {
	/* Cleanup test buffer before testing! */
	memset(fakeMem.test_prebuf,		0x55, sizeof(fakeMem.test_prebuf));
	memset(fakeMem.test_buf,		0, sizeof(fakeMem.test_buf));
	memset(fakeMem.test_postbuf,	0x55, sizeof(fakeMem.test_postbuf));

	memset(fakeMem.test_prebuf2,	0x55, sizeof(fakeMem.test_prebuf2));
	memset(fakeMem.test_buf2,		0, sizeof(fakeMem.test_buf2));
	memset(fakeMem.test_postbuf2,	0x55, sizeof(fakeMem.test_postbuf2));

	memset(fakeMem.test_prebuf3,	0x55, sizeof(fakeMem.test_prebuf3));
	memset(fakeMem.test_buf3,		0, sizeof(fakeMem.test_buf3));
	memset(fakeMem.test_postbuf3,	0x55, sizeof(fakeMem.test_postbuf3));

	memset(fakeMem.test_prebuf4,	0x55, sizeof(fakeMem.test_prebuf4));
	memset(fakeMem.test_buf4,		0, sizeof(fakeMem.test_buf4));
	memset(fakeMem.test_postbuf4,	0x55, sizeof(fakeMem.test_postbuf4));

	fakeMem.test_trbuf.eleCnt = 0;
	fakeMem.test_trbuf2.eleCnt = 0;
	fakeMem.test_trbuf3.eleCnt = 0;
	fakeMem.test_trbuf4.eleCnt = 0;

	fakeMem.test_trbuf.in = NULL;
	fakeMem.test_trbuf2.in = NULL;
	fakeMem.test_trbuf3.in = NULL;
	fakeMem.test_trbuf4.in = NULL;

	fakeMem.test_trbuf.out = NULL;
	fakeMem.test_trbuf2.out = NULL;
	fakeMem.test_trbuf3.out = NULL;
	fakeMem.test_trbuf4.out = NULL;

	fakeMem.test_trbuf.end = NULL;
	fakeMem.test_trbuf2.end = NULL;
	fakeMem.test_trbuf3.end = NULL;
	fakeMem.test_trbuf4.end = NULL;
}
void tearDown(void) {

}

bool helper_trbuf_areEqual(trbuf_t * expected, trbuf_t * actual) {
	if ((actual->ctl != expected->ctl)||
		(actual->eleType != expected->eleType)||
		(actual->eleCnt != expected->eleCnt)||
		(actual->len != expected->len)||
		(actual->buf != expected->buf)||
		(actual->in != expected->in)||
		(actual->out != expected->out)||
		(actual->end != expected->end)) {
		return false;
	}
	return true;
}


/*--------------------------------- +
|									|
|	Generic							|
|									|
+ -------------------------------- */
void test_trbuf_allFunctionShouldHandleNull(void) {
	uint8_t fakeBuf[4];
	trbuf_t invalid_trbuf = { .flushSel = TRBUF_FLUSHSEL_COMPLETE, .buf=NULL};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, trbuf_init(NULL), "init");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, trbuf_init(&invalid_trbuf), "init valid & FLUSH=COMPLETE but buf NULL");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, trbuf_flush(NULL), "flush");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_null, trbuf_flush(&invalid_trbuf), "flush valid & FLUSH=COMPLETE but buf NULL");
	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_getFreeSpace(NULL), "getFreeSpace");
	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_getUsedSpace(NULL), "getUsedSpace");
	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_getTotalSpace(NULL), "getTotalSpace");
	TEST_ASSERT_EQUAL_MESSAGE(false, trbuf_isEmpty(NULL), "isEmpty");
	TEST_ASSERT_EQUAL_MESSAGE(false, trbuf_isFilled(NULL), "isFilled");
	TEST_ASSERT_EQUAL_MESSAGE(false, trbuf_isFull(NULL), "isFull");

	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pushElements(NULL, NULL, 0), "pushElements all NULL");
	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pushElements(NULL, fakeBuf, 0), "pushElements trbuf NULL");
	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pushElements(&invalid_trbuf, NULL, 0), "pushElements src NULL");
	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pushElements(&invalid_trbuf, fakeBuf, 0), "pushElements buf NULL");
	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pullElements(NULL, NULL, 0), "pullElements all NULL");
	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pullElements(NULL, fakeBuf, 0), "pullElements trbuf NULL");
	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pullElements(&invalid_trbuf, NULL, 0), "pullElements src NULL");
	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pullElements(&invalid_trbuf, fakeBuf, 0), "pullElements buf NULL");
}

void test_trbuf_helperIsWorkingCorrectly(void) {
	TEST_ASSERT_MESSAGE(helper_trbuf_areEqual(&fakeMem.test_trbuf, &fakeMem.test_trbuf), "Should be equal");
	TEST_ASSERT_MESSAGE(!helper_trbuf_areEqual(&fakeMem.test_trbuf, &fakeMem.test_trbuf2), "Should be different");
}

/*--------------------------------- +
|									|
|	Public Macro					|
|									|
+ -------------------------------- */
void test_trbuf_VarsAreSizedCorrectly(void) {
	TEST_ASSERT_EQUAL_MESSAGE(1, sizeof(fakeMem.test_trbuf.ctl), "ctl");
	TEST_ASSERT_EQUAL_MESSAGE(1, sizeof(fakeMem.test_trbuf.eleType), "eleType");
	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(fakeMem.test_trbuf.eleCnt), "eleCnt");
	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(fakeMem.test_trbuf.len), "len");
	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(fakeMem.test_trbuf.buf), "buf");
	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(fakeMem.test_trbuf.in), "in");
	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(fakeMem.test_trbuf.out), "out");
	TEST_ASSERT_EQUAL_MESSAGE(4, sizeof(fakeMem.test_trbuf.end), "end");
	TEST_ASSERT_EQUAL_MESSAGE(26, sizeof(fakeMem.test_trbuf), "eleType");
}

void test_trbuf_ARRAYSIZEreturnEmptySize(void) {
	TEST_ASSERT_EQUAL_MESSAGE(0, TRBUF_ARRAYSIZE(ttype_none, 0), "Void");
	TEST_ASSERT_EQUAL_MESSAGE(0, TRBUF_ARRAYSIZE(ttype_u8, 0), "Unsigned 8bit");
	TEST_ASSERT_EQUAL_MESSAGE(0, TRBUF_ARRAYSIZE(ttype_u16, 0), "Unsigned 16bit");
	TEST_ASSERT_EQUAL_MESSAGE(0, TRBUF_ARRAYSIZE(ttype_u24, 0), "Unsigned 24bit");
	TEST_ASSERT_EQUAL_MESSAGE(0, TRBUF_ARRAYSIZE(ttype_u32, 0), "Unsigned 32bit");
	TEST_ASSERT_EQUAL_MESSAGE(0, TRBUF_ARRAYSIZE(ttype_u64, 0), "Unsigned 64bit");
	TEST_ASSERT_EQUAL_MESSAGE(0, TRBUF_ARRAYSIZE(ttype_s8, 0), "Signed 8bit");
	TEST_ASSERT_EQUAL_MESSAGE(0, TRBUF_ARRAYSIZE(ttype_f32, 0), "Float 32bit");
	TEST_ASSERT_EQUAL_MESSAGE(0, TRBUF_ARRAYSIZE(ttype_f64, 0), "Float 64bit");
	TEST_ASSERT_EQUAL_MESSAGE(0, TRBUF_ARRAYSIZE(ttype_bool, 0), "Boolean");
}

void test_trbuf_ARRAYSIZEreturnCorrectSize(void) {
	TEST_ASSERT_EQUAL_MESSAGE(32, TRBUF_ARRAYSIZE(ttype_u8, 32), "32 x 8bit");
	TEST_ASSERT_EQUAL_MESSAGE(256, TRBUF_ARRAYSIZE(ttype_u8, 256), "256 x 8bit");
	TEST_ASSERT_EQUAL_MESSAGE(UINT32_MAX, TRBUF_ARRAYSIZE(ttype_u8, UINT32_MAX), "UINT32_MAX x 8bit");
	TEST_ASSERT_EQUAL_MESSAGE(32*4, TRBUF_ARRAYSIZE(ttype_u32, 32), "32 x 32bit");
}

void test_trbuf_CTLSIZEreturnCorrectSize(void) {

	TEST_ASSERT_EQUAL(26, TRBUF_CTLSIZE());
}

void test_trbuf_TOTALSIZEreturnsCorrectSize(void) {
	TEST_ASSERT_EQUAL_MESSAGE(32+26, TRBUF_TOTALSIZE(ttype_u8, 32), "32x 8bit");
	TEST_ASSERT_EQUAL_MESSAGE(256+26, TRBUF_TOTALSIZE(ttype_u8, 256), "256x 8bit");
	TEST_ASSERT_EQUAL_MESSAGE((2048*4)+26, TRBUF_TOTALSIZE(ttype_u32, 2048), "2048x 32bit");
}


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
void test_trbuf_initShouldCallFlush(void) {
	TEST_IGNORE_MESSAGE("Mocking the same file you are testing is not supported by unity/cmock at this time...");
}

void test_trbuf_initShouldComputeEnd(void) {

	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)tcode_success, (uint32_t)trbuf_init(&fakeMem.test_trbuf), "init 10x u8");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)&fakeMem.test_buf[TEST_TRBUF_ELECNT-1], (uint32_t)fakeMem.test_trbuf.end, "end 10x u8");

	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)tcode_success, (uint32_t)trbuf_init(&fakeMem.test_trbuf2), "init 10x u32");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)&fakeMem.test_buf2[TEST_TRBUF2_ELECNT-1], (uint32_t)fakeMem.test_trbuf2.end, "end 10x u32");
	
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)tcode_success, (uint32_t)trbuf_init(&fakeMem.test_trbuf3), "init 4096x u8");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)&fakeMem.test_buf3[TEST_TRBUF3_ELECNT-1], (uint32_t)fakeMem.test_trbuf3.end, "end 4096x u8");

	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)tcode_success, (uint32_t)trbuf_init(&fakeMem.test_trbuf4), "init 128x u32 with options");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)&fakeMem.test_buf4[TEST_TRBUF4_ELECNT-1], (uint32_t)fakeMem.test_trbuf4.end, "end 128x u32 with options");
}

void test_trbuf_initShouldIgnoreInvalid(void) {
	trbuf_t invalid_trbuf = {
		.eleType = ttype_none,
		.len = 10,
		.buf = fakeMem.test_buf,
	};
	trbuf_t invalid_trbuf2 = {
		.eleType = ttype_u8,
		.len = 0,
		.buf = fakeMem.test_buf,
	};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_param, trbuf_init(&invalid_trbuf), "none type");
	TEST_ASSERT_EQUAL_MESSAGE(tcode_param, trbuf_init(&invalid_trbuf2), "0 len");
}


void test_trbuf_flushShouldClearCnt(void) {
	/* fake fill the buffer */
	fakeMem.test_trbuf.eleCnt = TEST_TRBUF_ELECNT;
	fakeMem.test_trbuf2.eleCnt = TEST_TRBUF2_ELECNT;
	fakeMem.test_trbuf3.eleCnt = TEST_TRBUF3_ELECNT;
	fakeMem.test_trbuf4.eleCnt = TEST_TRBUF4_ELECNT;

	TEST_ASSERT_EQUAL(tcode_success, trbuf_flush(&fakeMem.test_trbuf));
	TEST_ASSERT_EQUAL(0, fakeMem.test_trbuf.eleCnt);
	TEST_ASSERT_EQUAL(tcode_success, trbuf_flush(&fakeMem.test_trbuf2));
	TEST_ASSERT_EQUAL(0, fakeMem.test_trbuf2.eleCnt);
	TEST_ASSERT_EQUAL(tcode_success, trbuf_flush(&fakeMem.test_trbuf3));
	TEST_ASSERT_EQUAL(0, fakeMem.test_trbuf3.eleCnt);
	TEST_ASSERT_EQUAL(tcode_success, trbuf_flush(&fakeMem.test_trbuf4));
	TEST_ASSERT_EQUAL(0, fakeMem.test_trbuf4.eleCnt);
}

void test_trbuf_flushShouldClearMemoryWhenAsked(void) {
	/* fill all mem with data */
	memset(fakeMem.test_prebuf,		0xFF, sizeof(fakeMem.test_prebuf));
	memset(fakeMem.test_buf,		0xFF, sizeof(fakeMem.test_buf));
	memset(fakeMem.test_postbuf,	0xFF, sizeof(fakeMem.test_postbuf));
	/* Ensure it is actually filled */
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_prebuf, sizeof(fakeMem.test_prebuf));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_buf, sizeof(fakeMem.test_buf));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_postbuf, sizeof(fakeMem.test_postbuf));
	/* Call flush */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_flush(&fakeMem.test_trbuf), "Should return tcode_success");
	/* Ensure the flush was as requested (complete or just control) */
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_prebuf, sizeof(fakeMem.test_prebuf), "preMem should not be touched");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_buf, sizeof(fakeMem.test_buf), "buffer mem should not be touched");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_postbuf, sizeof(fakeMem.test_postbuf), "postMem should not be touched");

	/* Do the same for test_trbuf2 */
	memset(fakeMem.test_prebuf2,	0xFF, sizeof(fakeMem.test_prebuf2));
	memset(fakeMem.test_buf2,		0xFF, sizeof(fakeMem.test_buf2));
	memset(fakeMem.test_postbuf2,	0xFF, sizeof(fakeMem.test_postbuf2));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_prebuf2, sizeof(fakeMem.test_prebuf2));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_buf2, sizeof(fakeMem.test_buf2));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_postbuf2, sizeof(fakeMem.test_postbuf2));
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_flush(&fakeMem.test_trbuf2), "Should return tcode_success");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_prebuf2, sizeof(fakeMem.test_prebuf2), "preMem should not be touched");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_buf2, sizeof(fakeMem.test_buf2), "buffer mem should not be touched");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_postbuf2, sizeof(fakeMem.test_postbuf2), "postMem should not be touched");

	/* Do the same for test_trbuf3 */
	memset(fakeMem.test_prebuf3,	0xFF, sizeof(fakeMem.test_prebuf3));
	memset(fakeMem.test_buf3,		0xFF, sizeof(fakeMem.test_buf3));
	memset(fakeMem.test_postbuf3,	0xFF, sizeof(fakeMem.test_postbuf3));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_prebuf3, sizeof(fakeMem.test_prebuf3));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_buf3, sizeof(fakeMem.test_buf3));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_postbuf3, sizeof(fakeMem.test_postbuf3));
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_flush(&fakeMem.test_trbuf3), "Should return tcode_success");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_prebuf3, sizeof(fakeMem.test_prebuf3), "preMem should not be touched");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_buf3, sizeof(fakeMem.test_buf3), "buffer mem should not be touched");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_postbuf3, sizeof(fakeMem.test_postbuf3), "postMem should not be touched");

	/* Do the same for test_trbuf4 */
	memset(fakeMem.test_prebuf4,	0xFF, sizeof(fakeMem.test_prebuf4));
	memset(fakeMem.test_buf4,		0xFF, sizeof(fakeMem.test_buf4));
	memset(fakeMem.test_postbuf4,	0xFF, sizeof(fakeMem.test_postbuf4));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_prebuf4, sizeof(fakeMem.test_prebuf4));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_buf4, sizeof(fakeMem.test_buf4));
	TEST_ASSERT_EACH_EQUAL_HEX8(0xFF, fakeMem.test_postbuf4, sizeof(fakeMem.test_postbuf4));
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_flush(&fakeMem.test_trbuf4), "Should return tcode_success");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_prebuf4, sizeof(fakeMem.test_prebuf4), "preMem should not be touched");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(TEST_TRBUF_MEMDEFVAL_, fakeMem.test_buf4, sizeof(fakeMem.test_buf4), "buffer mem should be flushed");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fakeMem.test_postbuf4, sizeof(fakeMem.test_postbuf4), "postMem should not be touched");
}

void test_trbuf_flushShouldInitInAndOut(void) {
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_flush(&fakeMem.test_trbuf), "Should return tcode_success");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)fakeMem.test_trbuf.buf, (uint32_t)fakeMem.test_trbuf.in, "IN should be at start");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)fakeMem.test_trbuf.buf, (uint32_t)fakeMem.test_trbuf.out, "OUT should be at start");

	/* Do the same for test_trbuf2 */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_flush(&fakeMem.test_trbuf2), "Should return tcode_success");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)fakeMem.test_trbuf2.buf, (uint32_t)fakeMem.test_trbuf2.in, "IN should be at start");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)fakeMem.test_trbuf2.buf, (uint32_t)fakeMem.test_trbuf2.out, "OUT should be at start");

	/* Do the same for test_trbuf3 */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_flush(&fakeMem.test_trbuf3), "Should return tcode_success");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)fakeMem.test_trbuf3.buf, (uint32_t)fakeMem.test_trbuf3.in, "IN should be at start");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)fakeMem.test_trbuf3.buf, (uint32_t)fakeMem.test_trbuf3.out, "OUT should be at start");

	/* Do the same for test_trbuf4 */
	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_flush(&fakeMem.test_trbuf4), "Should return tcode_success");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)fakeMem.test_trbuf4.buf, (uint32_t)fakeMem.test_trbuf4.in, "IN should be at start");
	TEST_ASSERT_EQUAL_HEX32_MESSAGE((uint32_t)fakeMem.test_trbuf4.buf, (uint32_t)fakeMem.test_trbuf4.out, "OUT should be at start");
}


void test_trbuf_getFreeSpaceShouldReturnFreeSpace(void) {
	/* Fake fill buffers */
	fakeMem.test_trbuf.eleCnt = TEST_TRBUF_ELECNT/2;
	fakeMem.test_trbuf2.eleCnt = TEST_TRBUF2_ELECNT;
	fakeMem.test_trbuf3.eleCnt = 0;
	fakeMem.test_trbuf4.eleCnt = TEST_TRBUF4_ELECNT-1;

	TEST_ASSERT_EQUAL_MESSAGE(TEST_TRBUF_ELECNT/2,	trbuf_getFreeSpace(&fakeMem.test_trbuf),	"Should be half empty");
	TEST_ASSERT_EQUAL_MESSAGE(0,					trbuf_getFreeSpace(&fakeMem.test_trbuf2),	"Should be full");
	TEST_ASSERT_EQUAL_MESSAGE(TEST_TRBUF3_ELECNT,	trbuf_getFreeSpace(&fakeMem.test_trbuf3),	"Should be empty");
	TEST_ASSERT_EQUAL_MESSAGE(1,					trbuf_getFreeSpace(&fakeMem.test_trbuf4),	"Should be almost full");
}

void test_trbuf_getUsedSpaceShouldReturnUsedSpace(void) {
	/* Fake fill buffers */
	fakeMem.test_trbuf.eleCnt = TEST_TRBUF_ELECNT/2;
	fakeMem.test_trbuf2.eleCnt = TEST_TRBUF2_ELECNT;
	fakeMem.test_trbuf3.eleCnt = 0;
	fakeMem.test_trbuf4.eleCnt = TEST_TRBUF4_ELECNT-1;

	TEST_ASSERT_EQUAL_MESSAGE(TEST_TRBUF_ELECNT/2,	trbuf_getUsedSpace(&fakeMem.test_trbuf),	"Should be half empty");
	TEST_ASSERT_EQUAL_MESSAGE(TEST_TRBUF2_ELECNT,	trbuf_getUsedSpace(&fakeMem.test_trbuf2),	"Should be full");
	TEST_ASSERT_EQUAL_MESSAGE(0,					trbuf_getUsedSpace(&fakeMem.test_trbuf3),	"Should be empty");
	TEST_ASSERT_EQUAL_MESSAGE(TEST_TRBUF4_ELECNT-1,	trbuf_getUsedSpace(&fakeMem.test_trbuf4),	"Should be almost full");
}

void test_trbuf_getTotalSpaceShouldReturnTotalSpace(void) {
	TEST_ASSERT_EQUAL_MESSAGE(TEST_TRBUF_ELECNT, trbuf_getTotalSpace(&fakeMem.test_trbuf), "test trbuf");
	TEST_ASSERT_EQUAL_MESSAGE(TEST_TRBUF2_ELECNT, trbuf_getTotalSpace(&fakeMem.test_trbuf2), "test trbuf2");
	TEST_ASSERT_EQUAL_MESSAGE(TEST_TRBUF3_ELECNT, trbuf_getTotalSpace(&fakeMem.test_trbuf3), "test trbuf3");
	TEST_ASSERT_EQUAL_MESSAGE(TEST_TRBUF4_ELECNT, trbuf_getTotalSpace(&fakeMem.test_trbuf4), "test trbuf4");
}

void test_trbuf_isEmptyReturnsCorrectly(void) {
	TEST_ASSERT_EQUAL_MESSAGE(true, trbuf_isEmpty(&fakeMem.test_trbuf), "should be empty");
	fakeMem.test_trbuf.eleCnt = 1;
	TEST_ASSERT_EQUAL_MESSAGE(false, trbuf_isEmpty(&fakeMem.test_trbuf), "should not be empty");
	fakeMem.test_trbuf.eleCnt = TEST_TRBUF_ELECNT;
	TEST_ASSERT_EQUAL_MESSAGE(false, trbuf_isEmpty(&fakeMem.test_trbuf), "should be full");
}

void test_trbuf_isFilledReturnsCorrectly(void) {
	TEST_ASSERT_EQUAL_MESSAGE(false, trbuf_isFilled(&fakeMem.test_trbuf2), "should be empty");
	fakeMem.test_trbuf2.eleCnt = 1;
	TEST_ASSERT_EQUAL_MESSAGE(true, trbuf_isFilled(&fakeMem.test_trbuf2), "should be filled");
	fakeMem.test_trbuf2.eleCnt = TEST_TRBUF2_ELECNT;
	TEST_ASSERT_EQUAL_MESSAGE(true, trbuf_isFilled(&fakeMem.test_trbuf2), "should be full");
}

void test_trbuf_isFullReturnsCorrectly(void) {
	TEST_ASSERT_EQUAL_MESSAGE(false, trbuf_isFull(&fakeMem.test_trbuf3), "should be empty");
	fakeMem.test_trbuf3.eleCnt = 1;
	TEST_ASSERT_EQUAL_MESSAGE(false, trbuf_isFull(&fakeMem.test_trbuf3), "should not be full");
	fakeMem.test_trbuf3.eleCnt = TEST_TRBUF3_ELECNT;
	TEST_ASSERT_EQUAL_MESSAGE(true, trbuf_isFull(&fakeMem.test_trbuf3), "should be full");
}


void test_trbuf_pushElementsShouldHandle0cnt(void) {
	uint8_t fake_buf[10] = {};
	trbuf_t fake_trbuf = { .eleType=ttype_none, .buf=fake_buf, .eleCnt=0};
	trbuf_t copy_trbuf = { .eleType=ttype_none, .buf=fake_buf, .eleCnt=0};

	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pushElements(&fake_trbuf, (void*)fakeMem.test_src, 0), "should do nothing");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0, fake_buf, TEST_TRBUF_ELECNT, "memory should be unchanged");
	TEST_ASSERT_MESSAGE(helper_trbuf_areEqual(&copy_trbuf, &fake_trbuf), "control should not have changed");
}

void test_trbuf_pushElementsShouldHandleNoneType(void) {
	uint8_t fake_buf[10] = {};
	trbuf_t fake_trbuf = { .eleType=ttype_none, .buf=fake_buf, .eleCnt=0};
	trbuf_t copy_trbuf = { .eleType=ttype_none, .buf=fake_buf, .eleCnt=0};

	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pushElements(&fake_trbuf, (void*)fakeMem.test_src, 10), "should do nothing");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0, fake_buf, 10, "memory should be unchanged");
	TEST_ASSERT_MESSAGE(helper_trbuf_areEqual(&copy_trbuf, &fake_trbuf), "control should not have changed");
}

void test_trbuf_pullElementsShouldHandle0cnt(void) {
	uint8_t fake_buf[10] = {[0 ... 9] = 0xFF};
	uint8_t fake_dst[10] = {};
	trbuf_t fake_trbuf = { .eleType=ttype_none, .buf=fake_buf, .eleCnt=10, .out=&fake_buf[0]};
	trbuf_t copy_trbuf = { .eleType=ttype_none, .buf=fake_buf, .eleCnt=10, .out=&fake_buf[0]};

	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pullElements(&fake_trbuf, fake_dst, 0), "should do nothing");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0, fake_dst, 10, "dst should not have been written");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fake_buf, 10, "buf should not have been changed");
	TEST_ASSERT_MESSAGE(helper_trbuf_areEqual(&copy_trbuf, &fake_trbuf), "control should not have changed");
}

void test_trbuf_pullElementsShouldHandleNoneType(void) {
	uint8_t fake_buf[10] = {[0 ... 9] = 0xFF};
	uint8_t fake_dst[10] = {};
	trbuf_t fake_trbuf = { .eleType=ttype_none, .buf=fake_buf, .eleCnt=10, .out=&fake_buf[0]};
	trbuf_t copy_trbuf = { .eleType=ttype_none, .buf=fake_buf, .eleCnt=10, .out=&fake_buf[0]};

	TEST_ASSERT_EQUAL_MESSAGE(0, trbuf_pullElements(&fake_trbuf, fake_dst, 10), "should do nothing");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0, fake_dst, 10, "dst should not have been written");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, fake_buf, 10, "buf should not have been changed");
	TEST_ASSERT_MESSAGE(helper_trbuf_areEqual(&copy_trbuf, &fake_trbuf), "control should not have changed");
}



void test_trbuf_useCase8bitAndSilentDrop(void) {
	struct __attribute__((aligned(1), packed)) {
		uint8_t prebuf[10];
		uint8_t buf[4];
		uint8_t postbuf[10];
		trbuf_t trbuf;
	}testMem = {
		.prebuf = {[0 ... 9]=0xFF},
		.buf = {},
		.postbuf = {[0 ... 9]=0xFF},
		.trbuf.eleType = ttype_u8,
		.trbuf.ctl = 0,
		.trbuf.len = 4,
		.trbuf.buf = testMem.buf,
	};
	uint8_t testSrc[8] = {1, 2, 3, 4, 5, 6, 7, 8};
	uint8_t testDst[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_init(&testMem.trbuf),				"Init should succeed");
	TEST_ASSERT_EQUAL_MESSAGE(4 ,trbuf_pushElements(&testMem.trbuf, testSrc, 8),		"push should only load 4 elements ");
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(testSrc, testMem.buf, 4, 						"elements should be in buffer");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, testMem.prebuf, 10,						"prebuffer should be unchanged");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, testMem.postbuf, 10,						"postbuffer should be unchanged");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_getUsedSpace(&testMem.trbuf),					"used space should be 4");
	TEST_ASSERT_EQUAL_MESSAGE(3, trbuf_pullElements(&testMem.trbuf, testDst, 3),		"pull should be able to pull 3 elements");
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(testSrc, testDst, 3,							"dst should contain 3 ele from src");
	TEST_ASSERT_EQUAL_MESSAGE(3, trbuf_getFreeSpace(&testMem.trbuf),					"free space should be 3");
	TEST_ASSERT_EQUAL_MESSAGE(1, trbuf_pullElements(&testMem.trbuf, &testDst[3], 10),	"pull should only pull 1 elements");
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(testSrc, testDst, 4,							"dst should contain 4 ele from src");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_pushElements(&testMem.trbuf, &testSrc[4], 10),	"push should only load 4 new elements");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, testMem.prebuf, 10,						"prebuffer should be unchanged");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, testMem.postbuf, 10,						"postbuffer should be unchanged");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_pullElements(&testMem.trbuf, &testDst[4], 4),	"pull should be able to pull 4 elements");
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(testSrc, testDst, 8,							"dst should contain all ele from src");
}

void test_trbuf_useCase16bitAndSilentDrop(void) {
	struct __attribute__((aligned(1), packed)) {
		uint16_t prebuf[10];
		uint16_t buf[4];
		uint16_t postbuf[10];
		trbuf_t trbuf;
	}testMem = {
		.prebuf = {[0 ... 9]=0xFFFF},
		.buf = {},
		.postbuf = {[0 ... 9]=0xFFFF},
		.trbuf.eleType = ttype_u16,
		.trbuf.ctl = 0,
		.trbuf.len = 4,
		.trbuf.buf = testMem.buf,
	};
	uint16_t testSrc[8] = {0x0901, 0x0A02, 0x0B03, 0x0C04, 0x0D05, 0x0E06, 0x0F07, 0x1008};
	uint16_t testDst[8] = {0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_init(&testMem.trbuf),					"Init should succeed");
	TEST_ASSERT_EQUAL_MESSAGE(4 ,trbuf_pushElements(&testMem.trbuf, testSrc, 8),			"push should only load 4 elements ");
	TEST_ASSERT_EQUAL_HEX16_ARRAY_MESSAGE(testSrc, testMem.buf, 4, 							"elements should be in buffer");
	TEST_ASSERT_EACH_EQUAL_HEX16_MESSAGE(0xFFFF, testMem.prebuf, 10,						"prebuffer should be unchanged");
	TEST_ASSERT_EACH_EQUAL_HEX16_MESSAGE(0xFFFF, testMem.postbuf, 10,						"postbuffer should be unchanged");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_getUsedSpace(&testMem.trbuf),						"used space should be 4");
	TEST_ASSERT_EQUAL_MESSAGE(3, trbuf_pullElements(&testMem.trbuf, testDst, 3),			"pull should be able to pull 3 elements");
	TEST_ASSERT_EQUAL_HEX16_ARRAY_MESSAGE(testSrc, testDst, 3,								"dst should contain 3 ele from src");
	TEST_ASSERT_EQUAL_MESSAGE(3, trbuf_getFreeSpace(&testMem.trbuf),						"free space should be 3");
	TEST_ASSERT_EQUAL_MESSAGE(1, trbuf_pullElements(&testMem.trbuf, &testDst[3], 10),		"pull should only pull 1 elements");
	TEST_ASSERT_EQUAL_HEX16_ARRAY_MESSAGE(testSrc, testDst, 4,								"dst should contain 4 ele from src");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_pushElements(&testMem.trbuf, &testSrc[4], 10),		"push should only load 4 new elements");
	TEST_ASSERT_EACH_EQUAL_HEX16_MESSAGE(0xFFFF, testMem.prebuf, 10,						"prebuffer should be unchanged");
	TEST_ASSERT_EACH_EQUAL_HEX16_MESSAGE(0xFFFF, testMem.postbuf, 10,						"postbuffer should be unchanged");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_pullElements(&testMem.trbuf, &testDst[4], 4),		"pull should be able to pull 4 elements");
	TEST_ASSERT_EQUAL_HEX16_ARRAY_MESSAGE(testSrc, testDst, 8,								"dst should contain all ele from src");
}

void test_trbuf_useCase32bitAndSilentDrop(void) {
	struct __attribute__((aligned(1), packed)) {
		uint32_t prebuf[10];
		uint32_t buf[4];
		uint32_t postbuf[10];
		trbuf_t trbuf;
	}testMem = {
		.prebuf = {[0 ... 9]=0xFFFFFFFF},
		.buf = {},
		.postbuf = {[0 ... 9]=0xFFFFFFFF},
		.trbuf.eleType = ttype_u32,
		.trbuf.ctl = 0,
		.trbuf.len = 4,
		.trbuf.buf = testMem.buf,
	};
	uint32_t testSrc[8] = {0x01020304, 0x05060708, 0x090A0B0C, 0x0D0E0F10, 0x11121314, 0x15161718, 0x191A1B1C, 0x1D1E1F20};
	uint32_t testDst[8] = {0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_init(&testMem.trbuf),						"Init should succeed");
	TEST_ASSERT_EQUAL_MESSAGE(4 ,trbuf_pushElements(&testMem.trbuf, testSrc, 8),				"push should only load 4 elements ");
	TEST_ASSERT_EQUAL_HEX32_ARRAY_MESSAGE(testSrc, testMem.buf, 4, 								"elements should be in buffer");
	TEST_ASSERT_EACH_EQUAL_HEX32_MESSAGE(0xFFFFFFFF, testMem.prebuf, 10,						"prebuffer should be unchanged");
	TEST_ASSERT_EACH_EQUAL_HEX32_MESSAGE(0xFFFFFFFF, testMem.postbuf, 10,						"postbuffer should be unchanged");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_getUsedSpace(&testMem.trbuf),							"used space should be 4");
	TEST_ASSERT_EQUAL_MESSAGE(3, trbuf_pullElements(&testMem.trbuf, testDst, 3),				"pull should be able to pull 3 elements");
	TEST_ASSERT_EQUAL_HEX32_ARRAY_MESSAGE(testSrc, testDst, 3,									"dst should contain 3 ele from src");
	TEST_ASSERT_EQUAL_MESSAGE(3, trbuf_getFreeSpace(&testMem.trbuf),							"free space should be 3");
	TEST_ASSERT_EQUAL_MESSAGE(1, trbuf_pullElements(&testMem.trbuf, &testDst[3], 10),			"pull should only pull 1 elements");
	TEST_ASSERT_EQUAL_HEX32_ARRAY_MESSAGE(testSrc, testDst, 4,									"dst should contain 4 ele from src");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_pushElements(&testMem.trbuf, &testSrc[4], 10),			"push should only load 4 new elements");
	TEST_ASSERT_EACH_EQUAL_HEX32_MESSAGE(0xFFFFFFFF, testMem.prebuf, 10,						"prebuffer should be unchanged");
	TEST_ASSERT_EACH_EQUAL_HEX32_MESSAGE(0xFFFFFFFF, testMem.postbuf, 10,						"postbuffer should be unchanged");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_pullElements(&testMem.trbuf, &testDst[4], 4),			"pull should be able to pull 4 elements");
	TEST_ASSERT_EQUAL_HEX32_ARRAY_MESSAGE(testSrc, testDst, 8,									"dst should contain all ele from src");
}

void test_trbuf_useCase64bitAndSilentDrop(void) {
	struct __attribute__((aligned(1), packed)) {
		uint64_t prebuf[10];
		uint64_t buf[4];
		uint64_t postbuf[10];
		trbuf_t trbuf;
	}testMem = {
		.prebuf = {[0 ... 9]=0xFFFFFFFFFFFFFFFF},
		.buf = {},
		.postbuf = {[0 ... 9]=0xFFFFFFFFFFFFFFFF},
		.trbuf.eleType = ttype_u64,
		.trbuf.ctl = 0,
		.trbuf.len = 4,
		.trbuf.buf = testMem.buf,
	};
	uint64_t testSrc[8] = {	0x0102030405060708, 0x090A0B0C0D0E0F10, 0x1112131415161718, 0x191A1B1C1D1E1F20,
							0x2122232425262728, 0x292A2B2C2D2E2F30, 0x3132333435363738, 0x393A3B3C3D3E3F40};
	uint64_t testDst[8] = {	0x0000000000000000, 0x0000000000000000, 0x0000000000000000, 0x0000000000000000,
							0x0000000000000000, 0x0000000000000000, 0x0000000000000000, 0x0000000000000000};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_init(&testMem.trbuf),						"Init should succeed");
	TEST_ASSERT_EQUAL_MESSAGE(4 ,trbuf_pushElements(&testMem.trbuf, testSrc, 8),				"push should only load 4 elements ");
	TEST_ASSERT_EQUAL_HEX64_ARRAY_MESSAGE(testSrc, testMem.buf, 4, 								"elements should be in buffer");
	TEST_ASSERT_EACH_EQUAL_HEX64_MESSAGE(0xFFFFFFFFFFFFFFFF, testMem.prebuf, 10,				"prebuffer should be unchanged");
	TEST_ASSERT_EACH_EQUAL_HEX64_MESSAGE(0xFFFFFFFFFFFFFFFF, testMem.postbuf, 10,				"postbuffer should be unchanged");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_getUsedSpace(&testMem.trbuf),							"used space should be 4");
	TEST_ASSERT_EQUAL_MESSAGE(3, trbuf_pullElements(&testMem.trbuf, testDst, 3),				"pull should be able to pull 3 elements");
	TEST_ASSERT_EQUAL_HEX64_ARRAY_MESSAGE(testSrc, testDst, 3,									"dst should contain 3 ele from src");
	TEST_ASSERT_EQUAL_MESSAGE(3, trbuf_getFreeSpace(&testMem.trbuf),							"free space should be 3");
	TEST_ASSERT_EQUAL_MESSAGE(1, trbuf_pullElements(&testMem.trbuf, &testDst[3], 10),			"pull should only pull 1 elements");
	TEST_ASSERT_EQUAL_HEX64_ARRAY_MESSAGE(testSrc, testDst, 4,									"dst should contain 4 ele from src");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_pushElements(&testMem.trbuf, &testSrc[4], 10),			"push should only load 4 new elements");
	TEST_ASSERT_EACH_EQUAL_HEX64_MESSAGE(0xFFFFFFFFFFFFFFFF, testMem.prebuf, 10,				"prebuffer should be unchanged");
	TEST_ASSERT_EACH_EQUAL_HEX64_MESSAGE(0xFFFFFFFFFFFFFFFF, testMem.postbuf, 10,				"postbuffer should be unchanged");
	TEST_ASSERT_EQUAL_MESSAGE(4, trbuf_pullElements(&testMem.trbuf, &testDst[4], 4),			"pull should be able to pull 4 elements");
	TEST_ASSERT_EQUAL_HEX64_ARRAY_MESSAGE(testSrc, testDst, 8,									"dst should contain all ele from src");
}

void test_trbuf_useCase8bitAndDiscardLast(void) {
	TEST_IGNORE_MESSAGE("trbuf test will be reworked into unit test instead of usecase");

	struct __attribute__((aligned(1), packed)) {
		uint8_t prebuf[10];
		uint8_t buf[4];
		uint8_t postbuf[10];
		trbuf_t trbuf;
	}testMem = {
		.prebuf = {[0 ... 9]=0xFF},
		.buf = {},
		.postbuf = {[0 ... 9]=0xFF},
		.trbuf.eleType = ttype_u8,
		.trbuf.ctl = 0,
		.trbuf.len = 4,
		.trbuf.buf = testMem.buf,
	};
	uint8_t testSrc[8] = {1, 2, 3, 4, 5, 6, 7, 8};
	uint8_t testDst[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	TEST_ASSERT_EQUAL_MESSAGE(tcode_success, trbuf_init(&testMem.trbuf),						"Init should succeed");
	TEST_ASSERT_EQUAL_MESSAGE(1 ,trbuf_pushElements(&testMem.trbuf, testSrc, 1),				"push should take one element");
	TEST_ASSERT_EQUAL_MESSAGE(testSrc[0], testMem.buf[0],										"first element is in buffer");
	TEST_ASSERT_EQUAL_MESSAGE(4 ,trbuf_pushElements(&testMem.trbuf, &testSrc[1], 4),			"push should load 4 elements and overwrite");
	TEST_ASSERT_EQUAL_MESSAGE(4 ,trbuf_pullElements(&testMem.trbuf, &testDst[0], 8),			"pull should give only 4 elements");
	TEST_ASSERT_EQUAL_HEX8_ARRAY_MESSAGE(&testSrc[1], &testDst[0], 4,							"dst hold 4 values skipping the first");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0, &testDst[4], 4,										"dst was not overflowed");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, testMem.prebuf, 10,								"prebuffer should be unchanged");
	TEST_ASSERT_EACH_EQUAL_HEX8_MESSAGE(0xFF, testMem.postbuf, 10,								"postbuffer should be unchanged");
}

void test_trbuf_useCase8bitAndRepeatLast(void) {
	TEST_IGNORE();
}

