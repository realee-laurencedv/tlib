/* This file enables test to run without a complete project with custom configuration */

#ifndef __CONF_H__
#define __CONF_H__	1

#ifdef __cplusplus
	extern "C" {
#endif

/* Test environement needs vhw implementation */
#define THAL_USE_VHW	1

#ifdef __cplusplus
}
#endif
#endif
