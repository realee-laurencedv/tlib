#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>


/* -------------------------------- +
|									|
|	Print function					|
|									|
+ -------------------------------- */
#define TEST_FPRINT1(str, msg, var1)						{memset(str, 0, sizeof(str));	sprintf(str, msg, var1);}
#define TEST_FPRINT2(str, msg, var1, var2)					{memset(str, 0, sizeof(str));	sprintf(str, msg, var1, var2);}
#define TEST_FPRINT3(str, msg, var1, var2, var3)			{memset(str, 0, sizeof(str));	sprintf(str, msg, var1, var2, var3);}
#define TEST_FPRINT4(str, msg, var1, var2, var3, var4)		{memset(str, 0, sizeof(str));	sprintf(str, msg, var1, var2, var3, var4);}
#define TEST_FPRINT5(str, msg, var1, var2, var3, var4, var5){memset(str, 0, sizeof(str));	sprintf(str, msg, var1, var2, var3, var4, var5);}
