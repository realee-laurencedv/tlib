# Thor's Lib

Agnostic Low-level C lib for MCU  
Contact [maintainer][maintainer_email] for information and/or questions


## USAGE

1. Include the `inc` folder into the compiler include search path
2. Ensure the `src` folder is the compiler's compile list
3. Exclude `test` and `doc` from any compiler interaction (mainly compile list)
4. Some file automatically include a `conf.h` file to enable user to overload some static configuration
    - you can define the symbol, `TLIB_DISABLE_AUTOINC_CONF` to disable this feature


## TEST

1. Install [SublimeText][st_link]
2. Install the following plugin for ST:
    - [terminus][terminus_link]
3. Install [ceedling][ceedling_link]
    1. sudo apt install ruby
    2. sudo gem install ceedling
    3. pip install gcovr
4. Open the sublime [project][sublproj_file] file (menu __Project__ / __Open Project...__)
5. Select from the menu __Tools__ / __Build System__ / `Tlib Testing`
6. `CTRL+B` to invoke the tool, `CTRL+SHIFT+B` to select options

## CONTRIB

Current steps for contributing are:

1. Dont
2. Count those instead:
![image title][patate_img]
3. If you really need something not present or a change contact [maintainer][maintainer_email]


[sublproj_file]:        tlib.sublime-project

[maintainer_email]:     <MAILTO:laurencedv@realee.tech>

[patate_img]:           https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Kartoffeln_Markt.jpg/1200px-Kartoffeln_Markt.jpg

[staticmockprob_link]:  https://github.com/ThrowTheSwitch/CMock/pull/261
[st_link]:              https://www.sublimetext.com/
[terminus_link]:        https://packagecontrol.io/packages/Terminus
[ceedling_link]:        https://github.com/ThrowTheSwitch/Ceedling