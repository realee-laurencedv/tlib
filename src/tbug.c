/*
	Check header for details
*/
#include <tbug.h>

#pragma GCC diagnostic ignored "-Wunused-function"		/* Disable the warning from unused private function in your lib */
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"

#if defined(GPIO_ROUTE_SWOPEN)     \
  || defined(GPIO_ROUTEPEN_SWVPEN) \
  || defined(GPIO_TRACEROUTEPEN_SWVPEN)
#define	__TBUG_SWO_PRESENT	1
#endif

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
#define	__TBUG_MAXSTRINGCHAR		(0xFF)
#define	__TBUG_MAXARRAYBYTE			(0xFFFFFFFF)
#define	__TBUG_LOCALBUFSIZE			(0xFF)


/* -------------------------------- +
|									|
|	Global Variable					|
|									|
+ -------------------------------- */
static char __tbug_strbuf[__TBUG_MAXSTRINGCHAR+1];


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
void tbug_init(void) {
	#if (TBUG_STREAM == TBUG_STREAM_ENV)
	/* This is not the function you are looking for, move along */
	#elif (TBUG_STREAM == TBUG_STREAM_SWO)
	tbug_swo_init();
	#elif (TBUG_STREAM == TBUG_STREAM_GPIO)
	#elif (TBUG_STREAM == TBUG_STREAM_UART)
	tbug_uart_init();
	#elif (TBUG_STREAM == TBUG_STREAM_USBCDC)
	#elif (TBUG_STREAM == TBUG_STREAM_LOGMEM)
	#endif
}

void tbug_u8(uint8_t data) {
	#if (TBUG_STREAM == TBUG_STREAM_ENV)
	printf("%X", data);
	#elif (TBUG_STREAM == TBUG_STREAM_SWO)
	tbug_swo_u8(data);
	#elif (TBUG_STREAM == TBUG_STREAM_GPIO)
	#elif (TBUG_STREAM == TBUG_STREAM_UART)
	tbug_uart_u8(data);
	#elif (TBUG_STREAM == TBUG_STREAM_USBCDC)
	#elif (TBUG_STREAM == TBUG_STREAM_LOGMEM)
	#endif
}

void tbug_u16(uint16_t data) {
	#if (TBUG_STREAM == TBUG_STREAM_ENV)
	printf("%X", data);
	#elif (TBUG_STREAM == TBUG_STREAM_SWO)
	tbug_swo_u16(data);
	#elif (TBUG_STREAM == TBUG_STREAM_GPIO)
	#elif (TBUG_STREAM == TBUG_STREAM_UART)
	tbug_uart_u16(data);
	#elif (TBUG_STREAM == TBUG_STREAM_USBCDC)
	#elif (TBUG_STREAM == TBUG_STREAM_LOGMEM)
	#endif
}

void tbug_u32(uint32_t data) {
	#if (TBUG_STREAM == TBUG_STREAM_ENV)
	printf("%X", data);
	#elif (TBUG_STREAM == TBUG_STREAM_SWO)
	tbug_swo_u32(data);
	#elif (TBUG_STREAM == TBUG_STREAM_GPIO)
	#elif (TBUG_STREAM == TBUG_STREAM_UART)
	tbug_uart_u32(data);
	#elif (TBUG_STREAM == TBUG_STREAM_USBCDC)
	#elif (TBUG_STREAM == TBUG_STREAM_LOGMEM)
	#endif
}

void tbug_string(const char * string) {
	#if (TBUG_STREAM == TBUG_STREAM_ENV)
		printf("%s", string);
	#elif (TBUG_STREAM == TBUG_STREAM_SWO)
		tbug_swo_string(string);
	#elif (TBUG_STREAM == TBUG_STREAM_GPIO)
	#elif (TBUG_STREAM == TBUG_STREAM_UART)
		tbug_uart_string(string);
	#elif (TBUG_STREAM == TBUG_STREAM_USBCDC)
	#elif (TBUG_STREAM == TBUG_STREAM_LOGMEM)
	#endif
}

void tbug_array(uint8_t * array, uint32_t len) {
	if ((array!=NULL)&&(len>0)) {
		uint32_t cnt = 0;
		for (; cnt < MIN(len, __TBUG_MAXARRAYBYTE); cnt++) {
			tbug_u8(array[cnt]);
		}
	}
}

void tbug_format(const char * format, ...) {
	#if (TBUG_STREAM == TBUG_STREAM_ENV)
	if (format != NULL) {
		va_list args;
		va_start(args, format);
		vsnprintf(__tbug_strbuf, __TBUG_MAXSTRINGCHAR, format, args);
		printf("%s", __tbug_strbuf);
		va_end(args);
	}
	#else
	#warning "formated string output not implemented yet, ensure you dont depend on this function for your current purpose"
	#endif
}

void tbug_log(uint8_t verbosity, const char * user, const char * msg, ...) {
	#if (TBUG_STREAM == TBUG_STREAM_ENV)
	if (verbosity >= TBUG_VERBOSITY) {
		tbug_string("[");
		tbug_string("00000000");
		tbug_string(".");
		tbug_string("00000000");
		tbug_string("|");
		tbug_u8(verbosity);
		tbug_string("|");
		tbug_string(user);
		tbug_string("]");
		va_list args;
		va_start(args, msg);
		vsnprintf(__tbug_strbuf, __TBUG_MAXSTRINGCHAR, msg, args);
		tbug_string(__tbug_strbuf);
		va_end(args);
	}
	#else
	#warning "formated log not implemented yet, ensure you dont depend on this function for your current purpose"
	#endif
}


/* -------------------------------- +
|									|
|	SWO								|
|									|
+ -------------------------------- */
void tbug_swo_init(void) {
	#if defined(__TBUG_SWO_PRESENT) && defined(TBUG_USE_SWO)
	if (DBG_Connected()) {
		uint32_t freq;
		uint32_t div;

		DBG_SWOEnable(TBUG_SWO_LOC);
		
		/* Enable trace in core debug */
		CoreDebug->DHCSR |= CoreDebug_DHCSR_C_DEBUGEN_Msk;
		CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

		/* Enable PC and IRQ sampling output */
		DWT->CTRL = 0x400113FF;

		/* Set TPIU prescaler for the current debug clock frequency. Target frequency
		is 875 kHz so we choose a divider that gives us the closest match.
		Actual divider is TPI->ACPR + 1. */
		#if (_SILICON_LABS_32B_SERIES < 2)
			freq = CMU_ClockFreqGet(cmuClock_DBG) + (875000 / 2);
		#else //_SILICON_LABS_32B_SERIES
			freq = CMU_ClockFreqGet(cmuClock_TRACECLK) + (875000 / 2);
		#endif //_SILICON_LABS_32B_SERIES
		div  = freq / 875000;
		TPI->ACPR = div - 1;

		/* Set protocol to NRZ */
		TPI->SPPR = 2;

		/* Disable continuous formatting */
		TPI->FFCR = 0x100;

		/* Unlock ITM and output data */
		ITM->LAR = 0xC5ACCE55;
		ITM->TCR = 0x10009;

		/* ITM Channel 0 is used for UART output */
		ITM->TER |= (1UL << 0);
	}

	/* Test the output */
	tbug_swo_u8('!');
	tbug_swo_u8('\n');
	/* --------------- */
	#endif
}

void tbug_swo_u8(uint8_t data) {
	#if defined(__TBUG_SWO_PRESENT) && defined(TBUG_USE_SWO)
	ITM_SendChar(data);
	#endif
}

void tbug_swo_u16(uint16_t data) {
	#if defined(__TBUG_SWO_PRESENT) && defined(TBUG_USE_SWO)
	ITM_SendChar(data);
	#endif
}

void tbug_swo_u32(uint32_t data) {
	#if defined(__TBUG_SWO_PRESENT) && defined(TBUG_USE_SWO)
	ITM_SendChar(data);
	#endif
}

void tbug_swo_string(const char * string) {
	#if defined(__TBUG_SWO_PRESENT) && defined(TBUG_USE_SWO)
	if (string != NULL) {
		uint32_t cnt = 0;
		for (; cnt < __TBUG_MAXSTRINGCHAR; cnt++) {
			if (string[cnt] == '\0') {
				break;
			} else {
				tbug_u8(string[cnt]);
			}
		}
	}
	#endif
}

void tbug_swo_array(uint8_t * array, uint32_t len) {
	#if defined(__TBUG_SWO_PRESENT) && defined(TBUG_USE_SWO)
	if ((array!=NULL)&&(len>0)) {
		uint32_t cnt = 0;
		for (; cnt < MIN(len, __TBUG_MAXARRAYBYTE); cnt++) {
			ITM_SendChar(array[cnt]);
		}
	}
	#endif
}

void tbug_swo_format(const char * format, ...) {
	#if defined(__TBUG_SWO_PRESENT) && defined(TBUG_USE_SWO)
	#endif
}


/* -------------------------------- +
|									|
|	UART							|
|									|
+ -------------------------------- */
void tbug_uart_init(void) {
	#if defined(TBUG_USE_UART)
	/* Enable Pin */
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(cmuClock_HFPER, true);
	CMU_ClockEnable(emutil_findCMUClock(TBUG_UART_DEV), true);
	//GPIO_PinModeSet(gpioPortG, 6, gpioModePushPull, 1);
#warning "add pin finding and init the correct pin"

	/* Enable peripheral */
	const USART_InitAsync_TypeDef usartConf = {
		.baudrate =	TBUG_UART_BAUD,
		.databits = usartDatabits8,
		.parity = usartNoParity,
		.stopbits = usartStopbits1,
		.enable = usartDisable,
	};
	USART_InitAsync(TBUG_UART_DEV, &usartConf);
	#if defined(USART_ROUTEPEN_TXPEN)
		(TBUG_UART_DEV)->ROUTEPEN = USART_ROUTEPEN_TXPEN;
		(TBUG_UART_DEV)->ROUTELOC0 = TBUG_UART_TXLOC<<_USART_ROUTELOC0_TXLOC_SHIFT;
	#else
		#warning "Probably needs a TX ENABLE here"
		(TBUG_UART_DEV)->ROUTE = TBUG_UART_TXLOC<<_USART_ROUTELOC0_TXLOC_SHIFT;
	#endif
	USART_Enable(TBUG_UART_DEV, usartEnableTx);

	/* Test output */
	tbug_uart_u8('!');
	tbug_uart_u16(0xFF22);
	tbug_uart_u32(0x76543210);
	#endif
}

void tbug_uart_u8(uint8_t data) {
	#if defined(TBUG_USE_UART)
	USART_Tx(TBUG_UART_DEV, data);
	#endif
}

void tbug_uart_u16(uint16_t data) {
	#if defined(TBUG_USE_UART)
	USART_TxDouble(TBUG_UART_DEV, data);
	#endif
}

void tbug_uart_u32(uint32_t data) {
	#if defined(TBUG_USE_UART)
	USART_TxDouble(TBUG_UART_DEV, (uint16_t)data);
	USART_TxDouble(TBUG_UART_DEV, (uint16_t)(data>>16));
	#endif
}

void tbug_uart_string(const char * string) {
	#if defined(TBUG_USE_UART)
	if (string != NULL) {
		uint32_t cnt = 0;
		for (; cnt < __TBUG_MAXSTRINGCHAR; cnt++) {
			if (string[cnt] == '\0') {
				break;
			} else {
				tbug_uart_u8(string[cnt]);
			}
		}
	}
	#endif
}

void tbug_uart_array(uint8_t * array, uint32_t len) {
	#if defined(TBUG_USE_UART)
	#endif
}

void tbug_uart_format(const char * format, ...) {
	#if defined(TBUG_USE_UART)
	#endif
}

#pragma GCC diagnostic warning "-Wunused-function"		/* Restore the warnings from unused private function in your lib */
#pragma GCC diagnostic warning "-Wunused-parameter"
#pragma GCC diagnostic warning "-Wunused-variable"
