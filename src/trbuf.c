/*
	Check header for details
*/
#include <trbuf.h>

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
#define	__TRBUF_MEMDEFVAL__		(0)


/* -------------------------------- +
|									|
|	Global Variable					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
tcode_t trbuf_init(trbuf_t * buf) {
	if ((buf != NULL)&&(buf->buf != NULL)) {
		if ((buf->len == 0)||(buf->eleType == ttype_none)) {
			return tcode_param;
		}

		buf->end = buf->buf+((buf->len-1) * TTYPE_SIZEOF(buf->eleType));

		trbuf_flush(buf);

		return tcode_success;
	}
	return tcode_null;
}

tcode_t trbuf_flush(trbuf_t * buf) {
	if (buf != NULL) {
		/* Memory flush required */
		if (buf->flushSel == TRBUF_FLUSHSEL_COMPLETE) {
			if (buf->buf != NULL) {
				memset(	buf->buf,
						__TRBUF_MEMDEFVAL__,
						(buf->len)*TTYPE_SIZEOF(buf->eleType));
			} else {
				return tcode_null;
			}
		}
		/* --------------------- */

		/* Control flush */
		buf->eleCnt = 0;
		buf->in = buf->buf;
		buf->out = buf->buf;

		return tcode_success;
	}
	return tcode_null;
}

uint32_t trbuf_getFreeSpace(trbuf_t * buf) {
	if (buf != NULL) {
		return (buf->len - buf->eleCnt);
	}
	return 0;
}
uint32_t trbuf_getUsedSpace(trbuf_t * buf) {
	if (buf != NULL) {
		return (buf->eleCnt);
	}
	return 0;
}
uint32_t trbuf_getTotalSpace(trbuf_t * buf) {
	if (buf != NULL) {
		return (buf->len);
	}
	return 0;
}

bool trbuf_isEmpty(trbuf_t * buf) {
	if ((buf != NULL)&&(buf->eleCnt == 0)) {
		return true;
	}
	return false;
}
bool trbuf_isFilled(trbuf_t * buf) {
	if ((buf != NULL)&&(buf->eleCnt > 0)) {
		return true;
	}
	return false;
}
bool trbuf_isFull(trbuf_t * buf) {
	if ((buf != NULL)&&(buf->eleCnt == buf->len)) {
		return true;
	}
	return false;
}

uint32_t trbuf_pushElements(trbuf_t * buf, void * src, uint32_t eleCnt) {
	uint32_t pushedCnt = 0;
	if ((buf != NULL)&&(src != NULL)&&(buf->buf != NULL)&&(buf->eleCnt < buf->len)) {
		switch(buf->eleType) {
			case ttype_f64:
			case ttype_s64:
			case ttype_u64: {
				/* Push upto eleCnt element into buffer */
				for (; pushedCnt<eleCnt; ) {

					*(uint64_t*)(buf->in) = ((uint64_t*)src)[pushedCnt];
					
					/* Warp to start on end */
					if (buf->in >= buf->end) {
						buf->in = buf->buf;
					} else {
						buf->in += sizeof(uint64_t);
					}

					pushedCnt++;
					buf->eleCnt++;
					/* Stop when full */
					if (buf->eleCnt >= buf->len) {
						break;
					}
				}
				break;
			}
			case ttype_s24:
			case ttype_u24:
			case ttype_f32:
			case ttype_s32:
			case ttype_u32: {
				/* Push upto eleCnt element into buffer */
				for (; pushedCnt<eleCnt; ) {

					*(uint32_t*)(buf->in) = ((uint32_t*)src)[pushedCnt];
					
					/* Warp to start on end */
					if (buf->in >= buf->end) {
						buf->in = buf->buf;
					} else {
						buf->in += sizeof(uint32_t);
					}

					pushedCnt++;
					buf->eleCnt++;
					/* Stop when full */
					if (buf->eleCnt >= buf->len) {
						break;
					}
				}
				break;
			}
			case ttype_s16:
			case ttype_u16: {
				/* Push upto eleCnt element into buffer */
				for (; pushedCnt<eleCnt; ) {
					*(uint16_t*)(buf->in) = ((uint16_t*)src)[pushedCnt];
					
					/* Warp to start on end */
					if (buf->in >= buf->end) {
						buf->in = buf->buf;
					} else {
						buf->in += sizeof(uint16_t);
					}

					pushedCnt++;
					buf->eleCnt++;
					/* Stop when full */
					if (buf->eleCnt >= buf->len) {
						break;
					}
				}
				break;
			}
			case ttype_bool:
			case ttype_s8:
			case ttype_u8: {
				/* Push upto eleCnt element into buffer */
				for (; pushedCnt<eleCnt; ) {
					/* continue until full, or no more src */
					if ((buf->eleCnt < buf->len)||(buf->ovfSel == TRBUF_OVFSEL_DISCARD)) {
						
						*(uint8_t*)(buf->in) = ((uint8_t*)src)[pushedCnt];
						pushedCnt++;

						/* Warp to start on end */
						if (buf->in >= buf->end) {
							buf->in = buf->buf;
						} else {
							buf->in += sizeof(uint8_t);
						}


						if (buf->ovfSel != TRBUF_OVFSEL_DISCARD) {
							buf->eleCnt++;
						}
					} else {
						break;
					}					
				};
				break;
			}
			default: return 0;	/* unsupported type */
		}
	}
	return pushedCnt;
}
uint32_t trbuf_pullElements(trbuf_t * buf, void * dst, uint32_t eleCnt) {
	uint32_t pulledCnt = 0;
	if ((buf != NULL)&&(dst != NULL)&&(buf->buf != NULL)&&(buf->eleType != ttype_none)) {
		switch(buf->eleType) {
			case ttype_f64:
			case ttype_s64:
			case ttype_u64: {
				/* Push upto eleCnt element into buffer */
				for (; pulledCnt<eleCnt; ) {
					((uint64_t*)dst)[pulledCnt] = *(uint64_t*)(buf->out);
					
					/* Warp to start on end */
					if (buf->out >= buf->end) {
						buf->out = buf->buf;
					} else {
						buf->out += sizeof(uint64_t);
					}

					pulledCnt++;
					buf->eleCnt--;
					/* Stop when empty */
					if (buf->eleCnt <= 0) {
						break;
					}
				}
				break;
			}
			case ttype_s24:
			case ttype_u24:
			case ttype_f32:
			case ttype_s32:
			case ttype_u32: {
				/* Push upto eleCnt element into buffer */
				for (; pulledCnt<eleCnt; ) {
					((uint32_t*)dst)[pulledCnt] = *(uint32_t*)(buf->out);
					
					/* Warp to start on end */
					if (buf->out >= buf->end) {
						buf->out = buf->buf;
					} else {
						buf->out += sizeof(uint32_t);
					}

					pulledCnt++;
					buf->eleCnt--;
					/* Stop when empty */
					if (buf->eleCnt <= 0) {
						break;
					}
				}
				break;
			}
			case ttype_s16:
			case ttype_u16: {
				/* Push upto eleCnt element into buffer */
				for (; pulledCnt<eleCnt; ) {

					((uint16_t*)dst)[pulledCnt] = *(uint16_t*)(buf->out);
					
					/* Warp to start on end */
					if (buf->out >= buf->end) {
						buf->out = buf->buf;
					} else {
						buf->out += sizeof(uint16_t);
					}

					pulledCnt++;
					buf->eleCnt--;
					/* Stop when empty */
					if (buf->eleCnt <= 0) {
						break;
					}
				}
				break;
			}
			case ttype_bool:
			case ttype_s8:
			case ttype_u8: {
				/* Push upto eleCnt element into buffer */
				for (; pulledCnt<eleCnt; ) {

					((uint8_t*)dst)[pulledCnt] = *(uint8_t*)(buf->out);
					
					/* Warp to start on end */
					if (buf->out >= buf->end) {
						buf->out = buf->buf;
					} else {
						buf->out += sizeof(uint8_t);
					}

					pulledCnt++;
					buf->eleCnt--;
					/* Stop when empty */
					if (buf->eleCnt <= 0) {
						break;
					}
				};
				break;
			}
			default: return 0;	/* unsupported type */
		}
	}
	return pulledCnt;
}



