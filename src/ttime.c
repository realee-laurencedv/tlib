/*
	Check header for details
*/
#include <ttime.h>
#include <emutil.h>

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Global Variable					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
tcode_t ttime_init(void) {

	// const RTCC_Init_TypeDef rtccConf = {
	// 	.enable = true,
	// 	.debugRun = true,
	// 	.precntWrapOnCCV0 = true,
	// 	.cntWrapOnCCV1 = false,
	// 	.presc = rtccCntPresc_1,
	// 	.prescMode = rtccCntTickCCV0Match,
	// 	.enaOSCFailDetect = true,
	// 	.cntMode = rtccCntModeNormal,
	// };
	// const RTCC_CCChConf_TypeDef rtccCh0Conf = {
	// 	.chMode = rtccCapComChModeCompare,
	// 	.prsSel = rtccPRSCh0,
	// 	.compBase = rtccCompBasePreCnt,
	// };

	// /* Find the associated clock and enables it */
	// CMU_Clock_TypeDef clock = emutil_findCMUClock(rtcc);
	// if (clock == 0) {
	// 	return tcode_param;
	// }
	// CMU_OscillatorEnable(cmuOsc_LFRCO, true, true);		/* LFRCO is always present and reliable enough */
	// /* Warning, there is no timeout on LFRCO to be valid */
	// CMU_ClockSelectSet(cmuClock_LFE, cmuSelect_LFRCO);	/* Source LFE from LFRCO */
	// CMU_ClockEnable(cmuClock_HFLE, true);				/* All LF peripheral needs this bridge active */
	// CMU_ClockEnable(cmuClock_LFE, true);				/* RTCC is supplied by LFE clock */
	// CMU_ClockEnable(clock, true);

	// /* Start RTCC with empty value (uptime) */
	// RTCC_ChannelInit(0, &rtccCh0Conf);
	// RTCC_ChannelCCVSet(0, 0);
	// RTCC_Init(&rtccConf);

	return tcode_success;
}

