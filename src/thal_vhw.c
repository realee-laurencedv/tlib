/*
	Check header for details
*/
#include <thal_vhw.h>



/* -------------------------------- +
|									|
|	Private Macro	 				|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Thal definition 				|
|									|
+ -------------------------------- */
/* Protection against unintended inclusion */
#ifdef THAL_USE_VHW
/* CPU */
thal_cpu_t			__thal_vhw_cpu[THAL_HW_CPU_CNT] = {
	{	.hw =			NULL,
		.name =			"CPU0",
		.type =			thal_cpu_type_virt32},
};

/* DMA */
thal_dma_t			__thal_vhw_dma[THAL_HW_DMA_CNT] = {
	{	.hw =			NULL,
		.name =			"DMA0",
		.type =			thal_type_dma,
		.ch =			__thal_vhw_dmach[0],
		.chNb =			THAL_HW_DMACH_CNT},
	{	.hw =			NULL,
		.name =			"DMA1",
		.type =			thal_type_dma,
		.ch =			__thal_vhw_dmach[1],
		.chNb =			THAL_HW_DMACH_CNT},
};
thal_dmach_t		__thal_vhw_dmach[THAL_HW_DMA_CNT][THAL_HW_DMACH_CNT] = {
	{[0 ... THAL_HW_DMACH_CNT-1] = {.hw =			NULL,
									.xfer =			NULL}},
	{[0 ... THAL_HW_DMACH_CNT-1] = {.hw =			NULL,
									.xfer =			NULL}},
};

/* CLK */
thal_clk_t			__thal_vhw_clk[THAL_HW_CLK_CNT] = {
	{	.hw =			NULL,
		.name =			"CLK0"},
};

/* GPIO */
thal_gpio_t			__thal_vhw_gpio[THAL_HW_GPIO_CNT] = {
	{	.hw =			NULL,
		.name =			"GPIO0"},
	{	.hw =			NULL,
		.name =			"GPIO1"},
	{	.hw =			NULL,
		.name =			"GPIO2"},
	{	.hw =			NULL,
		.name =			"GPIO3"},
};
thal_gpiopin_t		__thal_vhw_gpiopin[THAL_HW_GPIO_CNT][THAL_HW_GPIOPIN_CNT] = {
	{},
	{},
	{},
	{},
};

/* TIMER */
thal_timer_t		__thal_vhw_timer[THAL_HW_TIMER_CNT] = {
	{	.hw =			NULL,
		.name =			"TIMER0"},
	{	.hw =			NULL,
		.name =			"TIMER1"},
	{	.hw =			NULL,
		.name =			"TIMER2"},
	{	.hw =			NULL,
		.name =			"TIMER3"},
	{	.hw =			NULL,
		.name =			"TIMER4"},
	{	.hw =			NULL,
		.name =			"TIMER5"},
	{	.hw =			NULL,
		.name =			"TIMER6"},
	{	.hw =			NULL,
		.name =			"TIMER7"},
};
thal_timercc_t		__thal_vhw_timercc[THAL_HW_TIMER_CNT][THAL_HW_TIMERCC_CNT] = {
	{},
	{},
	{},
	{},
	{},
	{},
	{},
	{},
};

/* UART */
thal_uart_t			__thal_vhw_uart[THAL_HW_UART_CNT] = {
	{	.hw =			NULL,
		.name =			"UART0"},
	{	.hw =			NULL,
		.name =			"UART1"},
	{	.hw =			NULL,
		.name =			"UART2"},
	{	.hw =			NULL,
		.name =			"UART3"},
};

/* SPI */
thal_spi_t			__thal_vhw_spi[THAL_HW_SPI_CNT] = {
	{	.hw =			NULL,
		.name =			"SPI0"},
	{	.hw =			NULL,
		.name =			"SPI1"},
	{	.hw =			NULL,
		.name =			"SPI2"},
	{	.hw =			NULL,
		.name =			"SPI3"},
};

/* I2C */
thal_i2c_t			__thal_vhw_i2c[THAL_HW_I2C_CNT] = {
	{	.hw =			NULL,
		.name =			"I2C0"},
	{	.hw =			NULL,
		.name =			"I2C1"},
	{	.hw =			NULL,
		.name =			"I2C2"},
	{	.hw =			NULL,
		.name =			"I2C3"},
};

/* I2S */
thal_i2s_t			__thal_vhw_i2s[THAL_HW_I2S_CNT] = {
	{	.hw =			NULL,
		.name =			"I2S0"},
	{	.hw =			NULL,
		.name =			"I2S1"},
	{	.hw =			NULL,
		.name =			"I2S2"},
	{	.hw =			NULL,
		.name =			"I2S3"},
};

/* ADC */
thal_adc_t			__thal_vhw_adc[THAL_HW_ADC_CNT] = {
	{	.hw =			NULL,
		.name =			"ADC0"},
	{	.hw =			NULL,
		.name =			"ADC1"},
};

/* DAC */
thal_dac_t			__thal_vhw_dac[THAL_HW_DAC_CNT] = {
	{	.hw =			NULL,
		.name =			"DAC0"},
	{	.hw =			NULL,
		.name =			"DAC1"},
};


thal_t _thal = {
	.info.version =			THAL_VHW_INFO_VERSION_DEF,
	.info.hwPlatform =		THAL_VHW_INFO_HWPLATFORM_DEF,
	.stat.all =				THAL_HW_STAT_DEF,
	.hw.cpu =				__thal_vhw_cpu,
	.hw.dma =				__thal_vhw_dma,
	.hw.clk =				__thal_vhw_clk,
	.hw.timer =				__thal_vhw_timer,
	.hw.gpio =				__thal_vhw_gpio,
	.hw.uart =				__thal_vhw_uart,
	.hw.spi =				__thal_vhw_spi,
	.hw.i2c =				__thal_vhw_i2c,
	.hw.i2s =				__thal_vhw_i2s,
	.hw.adc =				__thal_vhw_adc,
	.hw.dac =				__thal_vhw_dac,
};
#endif /* THAL_USE_VHW */


/* -------------------------------- +
|									|
|	Private helper	 				|
|									|
+ -------------------------------- */

TLIB_PRIVATE void __thal_vhw_resetObj(thal_t * thal) {
	/* Specific instance */
	thal_t * obj = thal;

	/* Global instance */
	if (obj == NULL) {
		obj = &_thal;
	}

	/* Load default values to vars */
	obj->stat.all =			THAL_HW_STAT_DEF;
}


/* -------------------------------- +
|									|
|	VHW - Runtime			 		|
|									|
+ -------------------------------- */
TLIB_WEAK thal_t * thal_init(thal_t * thal) {
	/* Specific instance */
	thal_t * obj = thal;

	/* Global instance */
	if (obj == NULL) {
		obj = &_thal;
	}

	/* -- Reset values -- */
	__thal_vhw_resetObj(obj);

	/* -- Update state -- */
	obj->stat.inited = true;

	return obj;
}

TLIB_WEAK thal_t * thal_term(thal_t * thal) {
	/* Specific instance */
	thal_t * obj = thal;

	/* Global instance */
	if (obj == NULL) {
		obj = &_thal;
	}

	/* Terminate something */

	/* update state */
	obj->stat.inited = false;
	obj->stat.hwCoupled = false;

	return obj;
}


/* -------------------------------- +
|									|
|	VHW - CPU						|
|									|
+ -------------------------------- */
TLIB_WEAK bool thal_cpu_atomicEnter(thal_cpu_t * cpu) {
	bool previousState = false;

	if (cpu != NULL) {
		previousState = (bool)cpu->stat.atomic;
		cpu->stat.atomic = true;
	}

	return previousState;
}

TLIB_WEAK void thal_cpu_atomicExit(thal_cpu_t * cpu, bool state){
	if (cpu != NULL) {
		cpu->stat.atomic = (bool)state;
	}
}


/* -------------------------------- +
|									|
|	VHW - DMA						|
|									|
+ -------------------------------- */
/* dma */
TLIB_WEAK tcode_t thal_dma_init(thal_dma_t * dma) {
	if (dma == NULL) {
		return tcode_null;
	}

	/* Load defaults */
	dma->ctl.all = THAL_HW_DMA_CTL_DEF;
	dma->stat.all = THAL_HW_DMA_STAT_DEF;
	for (uint8_t i = 0; i < dma->chNb; ++i)	{
		dma->ch[i].ctl.all = THAL_HW_DMACH_CTL_DEF;
		dma->ch[i].stat.all = THAL_HW_DMACH_STAT_DEF;
		dma->ch[i].xfer = NULL;
	}

	/* Would start the hw here */

	return tcode_success;
}

TLIB_WEAK tcode_t thal_dma_term(thal_dma_t * dma) {
	if (dma == NULL) {
		return tcode_null;
	}

	/* Would stop the hw here */

	return tcode_success;
}

thal_dma_capabilities_t thal_dma_getCapabilities(thal_dma_t * dma) {
	thal_dma_capabilities_t retVal = 0;

	return retVal;
}

tcode_t thal_dma_pwr(thal_dma_t * dma, thal_powerLvl_t newPowerLvl) {
	if (dma == NULL) {
		return tcode_null;
	}

	/* Configure new powerLevel */

	return tcode_success;
}

TLIB_WEAK tcode_t thal_dma_cmd(thal_dma_t * dma, thal_dma_ctl_t setup) {
	if (dma == NULL) {
		return tcode_null;
	}

	/* Parse and act on cmd */

	return tcode_success;
}

TLIB_WEAK thal_dma_stat_t thal_dma_stat(thal_dma_t * dma) {
	thal_dma_stat_t retVal = {.all = 0};

	if (dma != NULL) {
		retVal.all = 0xFF;	/* TODO: return actual status */
	}

	return retVal;
}

/* xfer */
TLIB_WEAK tcode_t thal_dma_xferInit(thal_dma_xfer_t * xfer) {
	if (xfer == NULL) {
		return tcode_null;
	}



	return tcode_success;
}

TLIB_WEAK tcode_t thal_dma_xferTerm(thal_dma_xfer_t * xfer) {
	if (xfer == NULL) {
		return tcode_null;
	}



	return tcode_success;
}

TLIB_WEAK tcode_t thal_dma_xferCbAttach(thal_dma_xfer_t * xfer, thal_dma_xferCb cb) {
	if ((xfer == NULL)||(cb == NULL)) {
		return tcode_null;
	}



	return tcode_success;
}

TLIB_WEAK tcode_t thal_dma_xferCbDetach(thal_dma_xfer_t * xfer, thal_dma_xferCb cb) {
	if ((xfer == NULL)||(cb == NULL)) {
		return tcode_null;
	}



	return tcode_success;
}

/* dmach */
TLIB_WEAK tcode_t thal_dmach_setup(thal_dmach_t * ch, thal_dmach_ctl_t setup) {
	if (ch == NULL) {
		return tcode_null;
	}



	return tcode_success;
}

TLIB_WEAK thal_dmach_stat_t thal_dmach_statGet(thal_dmach_t * ch) {
	thal_dmach_stat_t retVal = { .all = 0};

	if (ch != NULL) {
		
	}

	return retVal;
}

TLIB_WEAK tcode_t thal_dmach_xferAttach(thal_dmach_t * ch, thal_dma_xfer_t * xfer) {
	if ((ch == NULL)||(xfer == NULL)) {
		return tcode_null;
	}



	return tcode_success;
}

TLIB_WEAK tcode_t thal_dmach_xferDetach(thal_dmach_t * ch, thal_dma_xfer_t * xfer) {
	if ((ch == NULL)||(xfer == NULL)) {
		return tcode_null;
	}



	return tcode_success;
}





