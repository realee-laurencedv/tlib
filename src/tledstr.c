/*
	Check header for details
*/
#include <tledstr.h>

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
// const USART_InitSync_TypeDef _tledstr_usartConfig = {
// 	.enable = usartEnableTx,
// 	.refFreq = 0,
// 	.baudrate = WS2812B_SPI_BITRATE_BPS,
// 	.databits = usartDatabits8,
// 	.master = true,
// 	.msbf = true,
// 	.clockMode = usartClockMode2,
// };

/* Reset sequence must be: */
/* for LTST-E263 >50us */
/* for SK6812RGBW >80us */
/* for WS2812B >50us */
/* at 2.4Mbps, 1 SPIbyte is 2.5us, sending 40 sequential 0 should do it in all cases */
#ifdef TLIBTEST
const uint8_t _tledstr_resetSeq[40] =		{ [0] = 0x80, [1 ... 38] = 0, [39] = 0x01};
#else
TLIB_PRIVATE const uint8_t _tledstr_resetSeq[40] ={ [0] = 0x80, [1 ... 38] = 0, [39] = 0x01};
#endif


/* -------------------------------- +
|									|
|	Global Variable					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private API						|
|									|
+ -------------------------------- */
TLIB_PRIVATE void __tledstr_reset(tledstr_t * tledstr) {
	if (tledstr != NULL) {
		tledstr->state = tledstr_state_unknown;
		tledstr->pixSize =	0;
		tledstr->xferSize = 0;
		tledstr->cnt = 0;
		tledstr->encode = NULL;
	}
}

TLIB_PRIVATE void __tledstr_encodeContext(tledstr_t * tledstr) {

}

TLIB_PRIVATE uint32_t __tledstr_copy3ColSwap2(void * src, void * dst, uint32_t len) {
	uint32_t cnt = 0;
	uint8_t * wPtr = (uint8_t*)dst;
	uint8_t * rPtr = (uint8_t*)src;

	/* 3Color in src, round down to 3s */
	len -= len % 3;

	for (; cnt < len; cnt+=3) {
		wPtr[cnt] = rPtr[cnt+1];		/* G */
		wPtr[cnt+1] = rPtr[cnt];		/* R */
		wPtr[cnt+2] = rPtr[cnt+2];		/* B */
	}

	return cnt;
}

TLIB_PRIVATE uint32_t __tledstr_copy3ColStraight(void * src, void * dst, uint32_t len) {
	uint32_t cnt = 0;
	uint8_t * wPtr = (uint8_t*)dst;
	uint8_t * rPtr = (uint8_t*)src;

	/* 3Color in src, round down to 3s */
	len -= len % 3;

	for (; cnt < len; cnt+=3) {
		wPtr[cnt] = rPtr[cnt];			/* R */
		wPtr[cnt+1] = rPtr[cnt+1];		/* G */
		wPtr[cnt+2] = rPtr[cnt+2];		/* B */
	}

	return cnt;
}

TLIB_PRIVATE uint32_t __tledstr_copy4ColStraight(void * src, void * dst, uint32_t len) {
	uint32_t cnt = 0;
	uint32_t * wPtr = (uint32_t*)dst;
	uint32_t * rPtr = (uint32_t*)src;

	/* 4Color in src, round down to 4s */
	len -= len % 4;

	for (; cnt < len; cnt+=4) {
		wPtr[cnt] = rPtr[cnt];			/* RGBW */
	}

	return cnt;
}

TLIB_PRIVATE uint32_t __tledstr_copy3ColTo4Col(void * src, void * dst, uint32_t len) {
	uint32_t cnt = 0;
	uint32_t dstCnt = 0;
	uint8_t * wPtr = (uint8_t*)dst;
	uint8_t * rPtr = (uint8_t*)src;

	/* 3Color in src, round down to 3s */
	len -= len % 3;

	for (; cnt < len; ) {
		wPtr[dstCnt] = rPtr[cnt];			/* R */
		wPtr[dstCnt+1] = rPtr[cnt+1];		/* G */
		wPtr[dstCnt+2] = rPtr[cnt+2];		/* B */
		wPtr[dstCnt+3] = 0;					/* W */

		cnt+=3;
		dstCnt+=4;
	}

	return cnt;
}

TLIB_PRIVATE uint32_t __tledstr_copy4ColTo3Col(void * src, void * dst, uint32_t len) {
	uint32_t cnt = 0;
	uint32_t srcCnt = 0;
	uint8_t * wPtr = (uint8_t*)dst;
	uint8_t * rPtr = (uint8_t*)src;

	/* 4Color in src, round down to 4s */
	len -= len % 4;

	for (; srcCnt < len; ) {
		wPtr[cnt] = rPtr[srcCnt];			/* R */
		wPtr[cnt+1] = rPtr[srcCnt+1];		/* G */
		wPtr[cnt+2] = rPtr[srcCnt+2];		/* B */

		cnt+=3;
		srcCnt+=4;
	}

	return cnt;
}


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
tcode_t tledstr_init(tledstr_t * tledstr) {

	/* ---- Validation ---- */
	if (tledstr == NULL) {
		return tcode_null;
	}

	/* Clean from previous time */
	__tledstr_reset(tledstr);

	/* Verify type is supported */
	switch (tledstr->config.type) {
		default:				return tcode_notSupported;
		case tledstr_ws2812b:	tledstr->pixSize = WS2812B_PIXSIZE_BYTE;	tledstr->xferSize = WS2812B_ENCODEDSIZE_BYTE;	tledstr->encode = WS2812B_encode; break;
	}

	/* Validate configs */
	if (	(tledstr->config.pixBuf == NULL)||
			(tledstr->config.phyBuf == NULL)||
			(tledstr->config.pixNb == 0)||
			(tledstr->config.phyBufLen == 0)||
			(tledstr->config.hw == NULL)) {
		return tcode_badConfig;
	}
	/* HW related config validation */
	// volatile tio_t pin = tio_findByPeri(tledstr->config.hw, tio_pin_usart_tx, tledstr->config.loc);
	// #ifdef TLEDSTR_CLKEN
	// volatile tio_t clkPin = tio_findByPeri(tledstr->config.hw, tio_pin_usart_clk, tledstr->config.loc);
	// #endif

	/* Check if dmaCh is sane */
	/* TODO: dma ch api changed, adapt */
	// if ( tledstr->config.dmaCh > thal_dma_getMaxChannel()) {
	// 	return tcode_badConfig;
	// }

	// #ifdef TLEDSTR_CLKEN
	// if ((pin == TIO_INVALID) || (clkPin == TIO_INVALID)) {
	// #else
	// if (pin == TIO_INVALID) {
	// #endif
	// 	return tcode_badConfig;
	// }
	/* -------------------- */

	/* Ensure phyBuf is large enough to hold 2 pixel if str is longer than 1 */
	if (tledstr->config.pixNb > 1) {
		if (tledstr->config.phyBufLen < (tledstr->xferSize*2)) {
			return tcode_tooSmall;
		}
	} else {
		if (tledstr->config.phyBufLen < (tledstr->xferSize)) {
			return tcode_tooSmall;
		}
	}
	


	/* ---- Init ---- */
	/* Clean buffers */
	memset(tledstr->config.pixBuf, 0, (tledstr->pixSize * tledstr->config.pixNb));
	memset(tledstr->config.phyBuf, 0, tledstr->config.phyBufLen);

	/* Init clocks */
	// CMU_ClockEnable(cmuClock_GPIO, true);
	// CMU_ClockEnable(cmuClock_HFPER, true);
	// CMU_ClockEnable(emutil_findCMUClock(tledstr->config.hw), true);
	/* TODO: those are side-effects, they should save and restore clock state */

	/* LDMA transfert setup */
	/* Total size is according to phyBuf, single byte transfered when hw fifo is available */
	// tledstr->ldma.desc[0] = (LDMA_Descriptor_t)LDMA_DESCRIPTOR_LINKREL_M2P_BYTE(&((uint8_t*)tledstr->config.phyBuf)[0],
	// 																			&(tledstr->config.hw->TXDATA),
	// 																			tledstr->xferSize,
	// 																			1);
	// tledstr->ldma.desc[1] = (LDMA_Descriptor_t)LDMA_DESCRIPTOR_LINKREL_M2P_BYTE(&((uint8_t*)tledstr->config.phyBuf)[tledstr->xferSize],
	// 																			&(tledstr->config.hw->TXDATA),
	// 																			tledstr->xferSize,
	// 																			-1);
	// tledstr->ldma.desc[2] = (LDMA_Descriptor_t)LDMA_DESCRIPTOR_SINGLE_M2P_BYTE(	&_tledstr_resetSeq[0],
	// 																			&(tledstr->config.hw->TXDATA),
	// 																			sizeof(_tledstr_resetSeq));

	/* One byte will transfer everytime the USARTn TXBL flag is high */
	// LDMA_PeripheralSignal_t trigger = ldmaPeripheralSignal_NONE;
	// switch((uint32_t)(tledstr->config.hw)) {
	// 	default:					return tcode_badConfig;
	// 	#ifdef USART0_BASE
	// 	case (USART0_BASE):			trigger = ldmaPeripheralSignal_USART0_TXBL;		break;
	// 	#endif
	// 	#ifdef USART1_BASE
	// 	case (USART1_BASE):			trigger = ldmaPeripheralSignal_USART1_TXBL;		break;
	// 	#endif
	// 	#ifdef USART2_BASE
	// 	case (USART2_BASE):			trigger = ldmaPeripheralSignal_USART2_TXBL;		break;
	// 	#endif
	// 	#ifdef USART3_BASE
	// 	case (USART3_BASE):			trigger = ldmaPeripheralSignal_USART3_TXBL;		break;
	// 	#endif
	// 	#ifdef USART4_BASE
	// 	case (USART4_BASE):			trigger = ldmaPeripheralSignal_USART4_TXBL;		break;
	// 	#endif
	// 	#ifdef USART5_BASE
	// 	case (USART5_BASE):			trigger = ldmaPeripheralSignal_USART5_TXBL;		break;
	// 	#endif
	// 	#ifdef UART0_BASE
	// 	case (UART0_BASE):			trigger = ldmaPeripheralSignal_UART0_TXBL;		break;
	// 	#endif
	// 	#ifdef UART1_BASE
	// 	case (UART1_BASE):			trigger = ldmaPeripheralSignal_UART1_TXBL;		break;
	// 	#endif
	// }

	// tledstr->ldma.conf = (LDMA_TransferCfg_t)LDMA_TRANSFER_CFG_PERIPHERAL(trigger);

	/* IO */
	// GPIO_PinModeSet(TIO_PORT(pin), TIO_PIN(pin), gpioModePushPull, tledstr->config.activeVal);
	// #ifdef TLEDSTR_CLKEN
	// GPIO_PinModeSet(TIO_PORT(clkPin), TIO_PIN(clkPin), gpioModePushPull, 1);
	// #endif

	/* USART */
	// USART_InitSync(tledstr->config.hw, &_tledstr_usartConfig);

	/* Enable peripheral pins */
	// #ifndef TLIBTEST
	// #ifdef TLEDSTR_CLKEN
	// tledstr->config.hw->ROUTEPEN = USART_ROUTEPEN_TXPEN|USART_ROUTEPEN_CLKPEN;
	// tledstr->config.hw->ROUTELOC0 = ((tledstr->config.loc) << _USART_ROUTELOC0_TXLOC_SHIFT) |
	// 								((tledstr->config.loc) << _USART_ROUTELOC0_CLKLOC_SHIFT);
	// #else
	// tledstr->config.hw->ROUTEPEN = USART_ROUTEPEN_TXPEN;
	// tledstr->config.hw->ROUTELOC0 = (tledstr->config.loc) << _USART_ROUTELOC0_TXLOC_SHIFT;
	// #endif

	/* Polarity config */
	// if (tledstr->config.activeVal == false) {
	// 	tledstr->config.hw->CTRL |= USART_CTRL_TXINV;
	// }
	// #endif

	tledstr->state = tledstr_state_ready;
	/* -------------- */

	return tcode_success;
}

tcode_t tledstr_pixelSet(tledstr_t * tledstr, void * src, uint16_t pixID, uint16_t pixNb) {
	if ((tledstr == NULL)||(src == NULL)) {
		return tcode_null;
	}

	/* Validate tledstr is ready */
	if (tledstr->pixSize == 0) {
		return tcode_notReady;
	}

	/* Validate resquest is in bound */
	if ((pixID >= tledstr->config.pixNb) || ((pixID+pixNb) > tledstr->config.pixNb)) {
		return tcode_outOfRange;
	}

	/* Copy the pixel content from source to internal buffer */
	uint32_t cnt = 0;
	switch (tledstr->config.type) {
		case tledstr_ws2812b:	cnt = __tledstr_copy3ColSwap2(src, (void*)&((uint8_t*)tledstr->config.pixBuf)[pixID*tledstr->pixSize], (pixNb * (tledstr->pixSize)));	break;
		default:	break;
	}
	
	/* Return actual result */
	if (cnt == (pixNb * (tledstr->pixSize))) {
		return tcode_success;
	} else {
		return tcode_notEnought;
	}
}

tcode_t tledstr_pixelGet(tledstr_t * tledstr, void * dst, uint16_t pixID, uint16_t pixNb) {
	if ((tledstr == NULL)||(dst == NULL)) {
		return tcode_null;
	}

	/* Validate tledstr is ready */
	if (tledstr->pixSize == 0) {
		return tcode_notReady;
	}

	/* Validate resquest is in bound */
	if ((pixID >= tledstr->config.pixNb) || ((pixID+pixNb) > tledstr->config.pixNb)) {
		return tcode_outOfRange;
	}

	/* Copy the pixel content from internal buffer to dst, reverting order */
	uint32_t cnt = 0;
	switch (tledstr->config.type) {
		case tledstr_ws2812b:	cnt = __tledstr_copy3ColSwap2((void*)&((uint8_t*)tledstr->config.pixBuf)[pixID*tledstr->pixSize], dst, (pixNb * (tledstr->pixSize)));	break;
		default:	break;
	}

	/* Return actual result */
	if (cnt == (pixNb * (tledstr->pixSize))) {
		return tcode_success;
	} else {
		return tcode_notEnought;
	}
}

tcode_t tledstr_frameSend(tledstr_t * tledstr) {
	if (tledstr == NULL) {
		return tcode_null;
	}

	/* Validate tledstr is ready */
	if (tledstr->pixSize == 0) {
		return tcode_notReady;
	}

	thal_atomic_t atomic;
	atomic = thal_atomic_enter();
	if (tledstr->state == tledstr_state_ready) {
		tledstr->state = tledstr_state_txing;
		thal_atomic_exit(atomic);

		/* Find which is limiting, phybuf size or pixNb */
		uint16_t pixToEncode = MIN((tledstr->config.phyBufLen / tledstr->xferSize), tledstr->config.pixNb);
		
		/* Fill phybuf as much as possible */
		for (uint16_t pixEncoded = 0; pixEncoded < pixToEncode; pixEncoded++) {
			/* Encode a single pixel in phybuf */
			tcode_t encodeCode = tledstr->encode(	&((uint8_t*)tledstr->config.phyBuf)[tledstr->xferSize * pixEncoded],
													&((uint8_t*)tledstr->config.pixBuf)[((tledstr->config.pixNb) -1 -pixEncoded) * tledstr->pixSize],
													tledstr->pixSize);
			/* Last encode failed, exit */
			if (encodeCode != tcode_success) {
				return encodeCode;
			}
		}

		/* Start phy transfer */
		tledstr->cnt = 0;						/* Start counting the sent pixel */
		// LDMA_StartTransfer(tledstr->config.dmaCh, &tledstr->ldma.conf, &tledstr->ldma.desc[0]);
		// USART_Enable(tledstr->config.hw, usartEnableTx);

		return tcode_success;
	}
	
	thal_atomic_exit(atomic);
	return tcode_busy;
}

void tledstr_dma_cb(tledstr_t * tledstr) {

	if (tledstr != NULL) {

		tledstr->cnt++;										/* We have just finished sending a pixel */

		/* All pixel sent, start the reset sequence */
		if (tledstr->cnt == tledstr->config.pixNb) {
			// LDMA_StopTransfer(tledstr->config.dmaCh);
			// LDMA_StartTransfer(tledstr->config.dmaCh, &tledstr->ldma.conf, &tledstr->ldma.desc[2]);
		/* Reset sequence done, stop everything */
		} else if (tledstr->cnt > tledstr->config.pixNb) {
			tledstr->state = tledstr_state_ready;			/* String is not active anymore */
			// USART_Enable(tledstr->config.hw, usartDisable);
		/* Still pixel remaining to send */
		} else {
			// uint8_t bufId = (tledstr->cnt) & 0b1;
			// tcode_t encodeCode = tledstr->encode(	&((uint8_t*)tledstr->config.phyBuf)[ bufId * (tledstr->config.phyBufLen/2)],
			// 										&((uint8_t*)tledstr->config.pixBuf)[ (((tledstr->config.pixNb)-1) - tledstr->cnt) * tledstr->pixSize ],
			// 										tledstr->pixSize);
			// if (encodeCode == tcode_success) {
			// 	LDMA_StartTransfer(tledstr->config.dmaCh, &tledstr->ldma.conf, &tledstr->ldma.desc[bufId]);
			// } else {
			// 	/* TODO: Report an error for this string */
			// }
		}
	}
}
