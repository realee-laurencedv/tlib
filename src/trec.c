
/*
	Check header for details
*/
#include <trec.h>

#pragma GCC diagnostic ignored "-Wunused-function"		/* Disable the warning from unused private function in your lib */
#pragma GCC diagnostic ignored "-Wunused-parameter"

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */
#define	__TREC_MAX_DATASIZE			(4095)


/* -------------------------------- +
|									|
|	Private Type					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Global Variable					|
|									|
+ -------------------------------- */
#ifdef TLIBTEST
	extern trec_t __trec;				/* test runner hold the actual var */
#else
/* when in target memory, put in separate section for easier management */
// trec_t __trec __attribute__ ((section (".trec")));
#endif


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */
#define __TREC_REC0_SIZE_BYTE		(sizeof(trec_rec_t))
#define __TREC_REC1_SIZE_BYTE		(__TREC_REC0_SIZE_BYTE+sizeof(uint32_t))
#define __TREC_REC2_SIZE_BYTE		(__TREC_REC1_SIZE_BYTE+sizeof(uint32_t))
#define __TREC_REC3_SIZE_BYTE		(__TREC_REC2_SIZE_BYTE+sizeof(uint32_t))
#define __TREC_REC4_SIZE_BYTE		(__TREC_REC3_SIZE_BYTE+sizeof(uint32_t))



/* -------------------------------- +
|									|
|	Private Helpers					|
|									|
+ -------------------------------- */
static uint32_t _trec_getTS(void);
static uint32_t _trec_availableBufSpace(void);
static uint32_t _trec_cpy2buf(void * src, uint32_t len);
static uint32_t _trec_pullFromBuf(void * dst, uint32_t len);
static void _trec_flushBuf(void);


static uint32_t _trec_getTS(void) {
	uint32_t ts = 0;
	#if (TREC_TIMESRC == TREC_TIMESRC_RTCC)
	ts = 1;
	#endif
	return ts;
}

static uint32_t _trec_cpy2buf(void * src, uint32_t len) {
	uint32_t cnt = 0;

	/* Validate param */
	if (src != NULL) {
		TATOMIC_STATE;
		TATOMIC_START();
		/* Push as much data as possible or requested */
		uint32_t target =  (sizeof(__trec.buf) - __trec.cnt);	/* max possible target */
		if (len < target) {										/* reduced if less is requested */
			target = len;
		}

		for (; cnt < target; cnt++) {
			__trec.buf[__trec.in] = ((uint8_t*)src)[cnt];
			__trec.in++;
			/* Wrap around when at end */
			if (__trec.in >= sizeof(__trec.buf)) {
				__trec.in = 0;
			}
		}
		__trec.cnt += cnt;
		TATOMIC_STOP();
	}

	return cnt;
}

static uint32_t _trec_pullFromBuf(void * dst, uint32_t len) {
	uint32_t cnt = 0;

	if (dst != NULL) {
		TATOMIC_STATE;
		TATOMIC_START();
		/* Pull as much data as requested or available */
		uint32_t target = __trec.cnt;							/* max possible target */
		if (len < target) {										/* reduced if less is requested */
			target = len;
		}
		for (; cnt < target; cnt++) {
			((uint8_t*)dst)[cnt] = __trec.buf[__trec.out];
			__trec.out++;
			/* Wrap around when at end */
			if (__trec.out >= sizeof(__trec.buf)) {
				__trec.out = 0;
			}
		}
		__trec.cnt -= cnt;
		TATOMIC_STOP();
	}

	return cnt;
}

static uint32_t _trec_availableBufSpace(void) {
	TATOMIC_STATE;

	/* Check actual buffer space */
	TATOMIC_START();
	uint32_t availByte = (sizeof(__trec.buf) - __trec.cnt);
	TATOMIC_STOP();

	return availByte ;
}

static uint32_t _trec_pendingInBuf(void) {
	TATOMIC_STATE;

	/* Check actual pending bytes */
	TATOMIC_START();
	uint32_t pendingByte = __trec.cnt;
	TATOMIC_STOP();

	return pendingByte ;
}

static void _trec_flushBuf(void) {
	TATOMIC_STATE;
	TATOMIC_START();
	__trec.in = 0;
	__trec.out = 0;
	__trec.cnt = 0;
	memset(&__trec.buf[0], 0, sizeof(__trec.buf));
	TATOMIC_STOP();
}

/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
void trec_init(void) {
	#if (TREC_TIMESRC == TREC_TIMESRC_RTCC)
	ttime_init(RTCC);
	#endif

	/* Flush memory */
	_trec_flushBuf();

	/* timestamp init */
	__trec.initTime =_trec_getTS();

	#ifdef TREC_AUTO_EN
	trec_auto_init();
	#endif
}

void trec_rec0(uint8_t user, uint8_t event) {
	/* Timestamping early */
	trec_rec_t newRec;
	newRec.time = _trec_getTS();

	/* record if we have the memory */
	if (__TREC_REC0_SIZE_BYTE <= _trec_availableBufSpace()) {
		TATOMIC_STATE;
		newRec.user = user;
		newRec.event = event;
		newRec.type = trec_recType_empty;
		newRec.len = 0;
		TATOMIC_START();
		_trec_cpy2buf(&newRec, sizeof(trec_rec_t));
		TATOMIC_STOP();
	} else {
		__trec.bufOVF = true;
	}
}

void trec_rec1(uint8_t user, uint8_t event, uint32_t val0) {
	/* Timestamping early */
	trec_rec_t newRec;
	newRec.time = _trec_getTS();

	/* record if we have the memory */
	if (__TREC_REC1_SIZE_BYTE <= _trec_availableBufSpace()) {
		TATOMIC_STATE;
		uint32_t param[1] = {val0};
		newRec.user = user;
		newRec.event = event;
		newRec.type = trec_recType_mem;
		newRec.len = sizeof(param);
		TATOMIC_START();
		_trec_cpy2buf(&newRec, sizeof(trec_rec_t));
		_trec_cpy2buf(param, newRec.len);
		TATOMIC_STOP();
	} else {
		__trec.bufOVF = true;
	}
}

void trec_rec2(uint8_t user, uint8_t event, uint32_t val0, uint32_t val1) {
	/* Timestamping early */
	trec_rec_t newRec;
	newRec.time = _trec_getTS();

	/* record if we have the memory */
	if (__TREC_REC2_SIZE_BYTE <= _trec_availableBufSpace()) {
		TATOMIC_STATE;
		uint32_t param[2] = {val0, val1};
		newRec.user = user;
		newRec.event = event;
		newRec.type = trec_recType_mem;
		newRec.len = sizeof(param);
		TATOMIC_START();
		_trec_cpy2buf(&newRec, sizeof(trec_rec_t));
		_trec_cpy2buf(param, newRec.len);
		TATOMIC_STOP();
	} else {
		__trec.bufOVF = true;
	}
}

void trec_rec3(uint8_t user, uint8_t event, uint32_t val0, uint32_t val1, uint32_t val2) {
	/* Timestamping early */
	trec_rec_t newRec;
	newRec.time = _trec_getTS();

	/* record if we have the memory */
	if (__TREC_REC3_SIZE_BYTE <= _trec_availableBufSpace()) {
		TATOMIC_STATE;
		uint32_t param[3] = {val0, val1, val2};
		newRec.user = user;
		newRec.event = event;
		newRec.type = trec_recType_mem;
		newRec.len = sizeof(param);
		TATOMIC_START();
		_trec_cpy2buf(&newRec, sizeof(trec_rec_t));
		_trec_cpy2buf(param, newRec.len);
		TATOMIC_STOP();
	} else {
		__trec.bufOVF = true;
	}
}

void trec_rec4(uint8_t user, uint8_t event, uint32_t val0, uint32_t val1, uint32_t val2, uint32_t val3) {
	/* Timestamping early */
	trec_rec_t newRec;
	newRec.time = _trec_getTS();

	/* record if we have the memory */
	if (__TREC_REC4_SIZE_BYTE <= _trec_availableBufSpace()) {
		TATOMIC_STATE;
		uint32_t param[4] = {val0, val1, val2, val3};
		newRec.user = user;
		newRec.event = event;
		newRec.type = trec_recType_mem;
		newRec.len = sizeof(param);
		TATOMIC_START();
		_trec_cpy2buf(&newRec, sizeof(trec_rec_t));
		_trec_cpy2buf(param, newRec.len);
		TATOMIC_STOP();
	} else {
		__trec.bufOVF = true;
	}
}

void trec_recMem(uint8_t user, uint8_t event, void * mem, uint16_t len) {
	/* Timestamping early */
	trec_rec_t newRec;
	newRec.time = _trec_getTS();
	
	/* record if we have the memory */
	if ((len + sizeof(trec_rec_t)) <= _trec_availableBufSpace()) {
		TATOMIC_STATE;
		newRec.user = user;
		newRec.event = event;
		newRec.type = trec_recType_mem;
		newRec.len = len;
		TATOMIC_START();
		_trec_cpy2buf(&newRec, sizeof(trec_rec_t));
		_trec_cpy2buf(mem, len);
		TATOMIC_STOP();	
	} else {
		__trec.bufOVF = true;
	}
}

void trec_recMsg(uint8_t user, uint8_t event, const char * msg) {
	/* Timestamping early */
	trec_rec_t newRec;
	newRec.time = _trec_getTS();

	/* Find the end of the string */
	uint16_t cnt = 0;
	for (; cnt<__TREC_MAX_DATASIZE; cnt++) {
		if (msg[cnt] == '\0') {
			cnt++;	/* null char need to stay with string */
			break;
		}
	}

	/* start the record only if we have the space */
	if ((sizeof(trec_rec_t)+cnt) <= _trec_availableBufSpace()) {
		TATOMIC_STATE;
		newRec.user = user;
		newRec.event = event;
		newRec.type = trec_recType_msg;
		newRec.len = cnt;
		TATOMIC_START();
		_trec_cpy2buf(&newRec, sizeof(trec_rec_t));
		_trec_cpy2buf((void*)msg, newRec.len);
		TATOMIC_STOP();	
	} else {
		__trec.bufOVF = true;
	}
}

/* -------------------------------- +
|									|
|	Autonomous output streaming		|
|									|
+ -------------------------------- */
#ifdef TREC_AUTO_EN
void trec_auto_init(void) {
#ifdef TREC_AUTO_USE_UART
	/* Init IOs and Clocks */
	CMU_ClockEnable(cmuClock_GPIO, true);
	CMU_ClockEnable(emutil_findCMUClock(TREC_AUTO_UART), true);

	/* Init UART */

	/* Init LDMA */

	/* Enable everything */
#endif
}

void trec_auto_update(void) {
	/* Check if pending */

	/* Compute longest dma transfer possible */

	/* start transfer */
}

void trec_auto_ldma_irq(void) {
#ifdef TREC_AUTO_USE_UART
	/* Handle if done or not */

	/* If so, stop txing and wait for next update call */

	/* If not, prepare the next longest dma transfer possible */
#endif
}
#endif

#pragma GCC diagnostic warning "-Wunused-function"		/* Restore the warnings from unused private function in your lib */
#pragma GCC diagnostic warning "-Wunused-parameter"

