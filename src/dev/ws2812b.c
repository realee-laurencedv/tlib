/*
	Check header for details
*/
#include <dev/ws2812b.h>

/* -------------------------------- +
|									|
|	Private Constant				|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Global Variable					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Private Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
tcode_t WS2812B_encode(void * dest, uint8_t * src, uint16_t len) {

	if ((dest == NULL) || (src == NULL)) {
		return tcode_null;
	}
	uint32_t actualLen = (uint32_t)len<<1;
	uint32_t cnt = 0;
	uint8_t testVal;
	uint16_t destVal;

	for (;cnt<actualLen; cnt++) {

		/* cnt/2 is index, shifting down only odd one */
		testVal = ( src[cnt>>1] >>(4*(cnt&0b1)) ) & 0xF;
		switch (testVal) {
			default:		destVal = WS2812B_NIB_0000;	break;
			case 0b0001:	destVal = WS2812B_NIB_0001;	break;
			case 0b0010:	destVal = WS2812B_NIB_0010;	break;
			case 0b0011:	destVal = WS2812B_NIB_0011;	break;
			case 0b0100:	destVal = WS2812B_NIB_0100;	break;
			case 0b0101:	destVal = WS2812B_NIB_0101;	break;
			case 0b0110:	destVal = WS2812B_NIB_0110;	break;
			case 0b0111:	destVal = WS2812B_NIB_0111;	break;
			case 0b1000:	destVal = WS2812B_NIB_1000;	break;
			case 0b1001:	destVal = WS2812B_NIB_1001;	break;
			case 0b1010:	destVal = WS2812B_NIB_1010;	break;
			case 0b1011:	destVal = WS2812B_NIB_1011;	break;
			case 0b1100:	destVal = WS2812B_NIB_1100;	break;
			case 0b1101:	destVal = WS2812B_NIB_1101;	break;
			case 0b1110:	destVal = WS2812B_NIB_1110;	break;
			case 0b1111:	destVal = WS2812B_NIB_1111;	break;
		}
		((uint16_t *)dest)[cnt] = destVal;
	}
	return tcode_success;
}

