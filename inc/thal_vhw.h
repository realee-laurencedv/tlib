/*
*	\name		thal_hw.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Thal Virtual HW definitions
*	\note		
*	\warning	None
*	\license	refer to LICENSE.MD included, if not, contact admin@realee.tech
*/

#ifndef __THAL_VHW_H__
#define	__THAL_VHW_H__	1

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
/* autoconf */
#ifndef TLIB_DISABLE_AUTOINC_CONF
#include <conf.h>
#endif

#include <thal.h>							/* Abstract interface */

/* Protection against unintended inclusion */
#ifdef THAL_USE_VHW

/* -------------------------------- +
|									|
|	Constant						|
|									|
+ -------------------------------- */
/* Default value */
#define THAL_VHW_INFO_VERSION_DEF			(0x00010000)
#define THAL_VHW_INFO_HWPLATFORM_DEF		(thal_hw_virtual)
#define	THAL_VHW_CPU_ATOMICFLAG_DEF			(false)
#define THAL_HW_STAT_DEF					(0x00000000)


/* -------------------------------- +
|	CPU								|
+ -------------------------------- */
#define THAL_HW_CPU_CNT			(1)
extern thal_cpu_t				__thal_vhw_cpu[THAL_HW_CPU_CNT];


/* -------------------------------- +
|	DMA								|
+ -------------------------------- */
#define THAL_HW_DMA_CNT			(2)
#define THAL_HW_DMACH_CNT		(16)
extern thal_dma_t				__thal_vhw_dma[THAL_HW_DMA_CNT];
extern thal_dmach_t				__thal_vhw_dmach[THAL_HW_DMA_CNT][THAL_HW_DMACH_CNT];

#define THAL_HW_DMA_CTL_DEF		(0x00000000)
#define THAL_HW_DMA_STAT_DEF	(0x00000000)

#define THAL_HW_DMACH_CTL_DEF	(0x00000000)
#define THAL_HW_DMACH_STAT_DEF	(0x00000000)


/* -------------------------------- +
|	CLK								|
+ -------------------------------- */
#define THAL_HW_CLK_CNT			(1)
extern thal_clk_t				__thal_vhw_clk[THAL_HW_CLK_CNT];


/* -------------------------------- +
|	GPIO							|
+ -------------------------------- */
#define THAL_HW_GPIO_CNT		(4)
#define THAL_HW_GPIOPIN_CNT		(32)
extern thal_gpio_t				__thal_vhw_gpio[THAL_HW_GPIO_CNT];
extern thal_gpiopin_t 			__thal_vhw_gpiopin[THAL_HW_GPIO_CNT][THAL_HW_GPIOPIN_CNT];


/* -------------------------------- +
|	TIMER							|
+ -------------------------------- */
#define THAL_HW_TIMER_CNT		(8)
#define THAL_HW_TIMERCC_CNT		(4)
extern thal_timer_t				__thal_vhw_timer[THAL_HW_TIMER_CNT];
extern thal_timercc_t			__thal_vhw_timercc[THAL_HW_TIMER_CNT][THAL_HW_TIMERCC_CNT];


/* -------------------------------- +
|	UART							|
+ -------------------------------- */
#define	THAL_HW_UART_CNT		(4)
extern thal_uart_t				__thal_vhw_uart[THAL_HW_UART_CNT];


/* -------------------------------- +
|	SPI								|
+ -------------------------------- */
#define	THAL_HW_SPI_CNT			(4)
extern thal_spi_t				__thal_vhw_spi[THAL_HW_SPI_CNT];


/* -------------------------------- +
|	I2C								|
+ -------------------------------- */
#define	THAL_HW_I2C_CNT			(4)
extern thal_i2c_t				__thal_vhw_i2c[THAL_HW_I2C_CNT];


/* -------------------------------- +
|	I2S								|
+ -------------------------------- */
#define	THAL_HW_I2S_CNT			(4)
extern thal_i2s_t				__thal_vhw_i2s[THAL_HW_I2S_CNT];


/* -------------------------------- +
|	ADC								|
+ -------------------------------- */
#define	THAL_HW_ADC_CNT			(2)
extern thal_adc_t				__thal_vhw_adc[THAL_HW_ADC_CNT];


/* -------------------------------- +
|	DAC								|
+ -------------------------------- */
#define	THAL_HW_DAC_CNT			(2)
extern thal_dac_t				__thal_vhw_dac[THAL_HW_DAC_CNT];


/* -------------------------------- +
|	Run-time						|
+ -------------------------------- */



#endif /* THAL_USE_VHW */

#ifdef __cplusplus
}
#endif
#endif	/* __THAL_VHW_H__ */
