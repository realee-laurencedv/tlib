/*
*	\name		trec_data.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		trec data value dictionnary
*	\note		
*	\warning	
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/

#ifndef __TREC_DATA_H__
#define __TREC_DATA_H__	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>


/* -------------------------------- +
|									|
|	User code						|
|									|
+ -------------------------------- */
#define	TREC_USER_NONE		(0x00)
#define	TREC_USER_HW		(0x01)
#define	TREC_USER_CPU		(0x02)
#define	TREC_USER_TREC		(0x03)
#define	TREC_USER_OS		(0x04)
#define	TREC_USER_ISR		(0x05)
#define	TREC_USER_BTL		(0x11)
#define	TREC_USER_APP		(0x12)
#define	TREC_USER_TEST		(0xF0)


/* -------------------------------- +
|									|
|	Event type code					|
|									|
+ -------------------------------- */
#define	TREC_EVT_NONE		(0x00)
#define	TREC_EVT_INIT		(0x01)
#define	TREC_EVT_MEM		(0x02)
#define	TREC_EVT_MSG		(0x03)
#define	TREC_EVT_ERR		(0x80)


#ifdef __cplusplus
}
#endif
#endif

