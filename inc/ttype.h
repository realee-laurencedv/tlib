/*
*	\name		ttype.h
*	\author		Laurence DV
*	\version	1.1.0
*	\brief		General types
*	\note		
*	\warning	None
*	\license	refer to LICENSE.MD included, if not, contact admin@realee.tech
*/

#ifndef __TTYPE_H__
#define __TTYPE_H__	1

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	STDlib is a trusted ally		|
|	Support and Logistics unit		|
+ -------------------------------- */
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <float.h>

#include <tutil.h>

/* -------------------------------- +
|									|
|	Generic return code				|
|	Useful function status			|
|	return and generic error codes	|
|									|
+ -------------------------------- */
typedef enum TLIB_PACKED tcode_e{
	/* Positive number are available for usage specific return code */
	tcode_success =			0,
	tcode_fail =			-1,

	tcode_outOfRange =		-99,
	tcode_tooSmall =		-100,
	tcoce_tooLarge =		-101,
	tcode_notEnought =		-102,

	tcode_busy =			-123,
	tcode_notSupported =	-124,
	tcode_badConfig =		-125,
	tcode_notReady =		-126,
	tcode_param =			-127,
	tcode_null =			-128,
}tcode_t;


/* -------------------------------- +
|									|
|	Generic data type				|
|	Compact with metadata easily	|
|	retreivable						|
|									|
+ -------------------------------- */
#define	_TTYPE_SIZEMASK			(0x0F)
#define	_TTYPE_SIZEPOS			(0)
#define	_TTYPE_TYPEMASK			(0xF0)
#define	_TTYPE_TYPEPOS			(4)
#define	_TTYPE_TYPEPOS_SIGNED	(_TTYPE_TYPEPOS+0)
#define	_TTYPE_TYPEPOS_UNSIGNED	(_TTYPE_TYPEPOS+1)
#define	_TTYPE_TYPEPOS_FLOAT	(_TTYPE_TYPEPOS+2)
#define	_TTYPE_TYPEPOS_LOGIC	(_TTYPE_TYPEPOS+3)

typedef enum TLIB_PACKED ttype_e{
	ttype_none =	0x00,		/* void */
	
	ttype_u8 =		0x11,		/* uint8_t */
	ttype_u16 =		0x12,		/* uint16_t */
	ttype_u24 =		0x13,		/* uint24_t */
	ttype_u32 =		0x14,		/* uint32_t */
	ttype_u64 =		0x18,		/* uint64_t */

	ttype_s8 =		0x21,		/* int8_t */
	ttype_s16 =		0x22,		/* int16_t */
	ttype_s24 =		0x23,		/* int24_t */
	ttype_s32 =		0x24,		/* int32_t */
	ttype_s64 =		0x28,		/* int64_t */

	ttype_f32 =		0x44,		/* float */
	ttype_f64 =		0x48,		/* double */

	ttype_bool =	0x81,		/* bool */
}ttype_t;

#define TTYPE_SIZEOF(x)			((((ttype_t)(x)) & _TTYPE_SIZEMASK) >> _TTYPE_SIZEPOS)
#define TTYPE_ISUNSIGNED(x)		((((ttype_t)(x)) & (1<<_TTYPE_TYPEPOS_SIGNED)) >> _TTYPE_TYPEPOS_SIGNED)
#define TTYPE_ISSIGNED(x)		((((ttype_t)(x)) & (1<<_TTYPE_TYPEPOS_UNSIGNED)) >> _TTYPE_TYPEPOS_UNSIGNED)
#define TTYPE_ISFLOAT(x)		((((ttype_t)(x)) & (1<<_TTYPE_TYPEPOS_FLOAT)) >> _TTYPE_TYPEPOS_FLOAT)
#define TTYPE_ISBOOLEAN(x)		((((ttype_t)(x)) & (1<<_TTYPE_TYPEPOS_LOGIC)) >> _TTYPE_TYPEPOS_LOGIC)


/* -------------------------------- +
|									|
|	Function Type					|
|	Mobile unit (light & heavy)		|
+ -------------------------------- */
/* Generic callback function */
typedef void(*tcb_t)(void * data);


#ifdef __cplusplus
}
#endif
#endif

