/*
*	\name		tpin.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Abstract object and function to access pins
*	\note
*	\warning	None
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/
#ifndef __TPIN_H__
#define __TPIN_H__	1


#ifdef __cplusplus
	extern "C" {
#endif

/*--------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>
#include <tutil.h>
#include <ttype.h>


/*--------------------------------- +
|									|
|	Public Type						|
|									|
+ -------------------------------- */
typedef struct _tpin_cfg_s {
	void *		hw;			/* hardware registers start address */
}tpin_cfg_t;

typedef enum _tpin_mode_e {
	tpin_mode_unknown,
	tpin_mode_unused,
	tpin_mode_gpio,
	tpin_mode_digital,
	tpin_mode_analog,
	tpin_mode_specific,
}tpin_mode_t;

typedef struct _tpin_s {
	/* current direction */
	void *		hw;			/* hardware register block */
	tpin_mode_t mode;		/* current mode */
	void *		mutex;		/* concurrent access control */
}tpin_t;


/*--------------------------------- +
|									|
|	Public Macro					|
|									|
+ -------------------------------- */
/* Return the encoded unique identifier from given discrete numbers */
#define	TPIN(port, pin)					((((tpin_t)(port))<<6) + ((pin)&0x3F))
/* Return only the port unique identifier */
#define	TPIN_PORT(gpio)					(((tpin_t)(gpio))>>6)
/* Return only the pin number */
#define	TPIN_PIN(gpio)					(((gpio)&0xFF))


/*--------------------------------- +
|									|
|	Public Constant					|
|									|
+ -------------------------------- */
#define TPIN_INVALID					((tpin_t)(0xFFFF))		/* Represent an invalid or unknown io */


/*--------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
/* initialize a tpin object in $tpin memory with $cfg content */
tcode_t tpin_init(tpin_t * tpin, uint8_t port, uint8_t pin, void * cfg);

/* multi user access */
tcode_t tpin_take(tpin_t * tpin);
tcode_t tpin_release(tpin_t * tpin);


#ifdef __cplusplus
}
#endif
#endif

