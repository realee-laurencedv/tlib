
/*
*	\name		dev_ws2812b.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Addressable LED RGB WS2812B specific definitions
*	\note		
*	\warning	
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/
#ifndef __DEV_WS2812B_H__
#define __DEV_WS2812B_H__	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <tutil.h>
#include <ttype.h>


/* -------------------------------- +
|									|
|	Device constants				|
|	(4:1 ratio)						|
+ -------------------------------- */
#define	WS2812B_COLOR_NB					(3)
#define	WS2812B_ENCODING_RATIO				(4)
#define	WS2812B_COLORSIZE_BIT				(8)
#define	WS2812B_PIXSIZE_BYTE				(WS2812B_COLOR_NB)
#define	WS2812B_ENCODEDSIZE_BYTE			(WS2812B_PIXSIZE_BYTE * WS2812B_ENCODING_RATIO)


/* -------------------------------- +
|									|
|	Protocol constants				|
|	(4:1 ratio)						|
+ -------------------------------- */
#define	WS2812B_BIT_0						(0b1000)
#define	WS2812B_BIT_1						(0b1110)
#define WS2812B_NIB_0000					((WS2812B_BIT_0<<12)|(WS2812B_BIT_0<<8)|(WS2812B_BIT_0<<4)|WS2812B_BIT_0)
#define WS2812B_NIB_0001					((WS2812B_BIT_0<<12)|(WS2812B_BIT_0<<8)|(WS2812B_BIT_0<<4)|WS2812B_BIT_1)
#define WS2812B_NIB_0010					((WS2812B_BIT_0<<12)|(WS2812B_BIT_0<<8)|(WS2812B_BIT_1<<4)|WS2812B_BIT_0)
#define WS2812B_NIB_0011					((WS2812B_BIT_0<<12)|(WS2812B_BIT_0<<8)|(WS2812B_BIT_1<<4)|WS2812B_BIT_1)
#define WS2812B_NIB_0100					((WS2812B_BIT_0<<12)|(WS2812B_BIT_1<<8)|(WS2812B_BIT_0<<4)|WS2812B_BIT_0)
#define WS2812B_NIB_0101					((WS2812B_BIT_0<<12)|(WS2812B_BIT_1<<8)|(WS2812B_BIT_0<<4)|WS2812B_BIT_1)
#define WS2812B_NIB_0110					((WS2812B_BIT_0<<12)|(WS2812B_BIT_1<<8)|(WS2812B_BIT_1<<4)|WS2812B_BIT_0)
#define WS2812B_NIB_0111					((WS2812B_BIT_0<<12)|(WS2812B_BIT_1<<8)|(WS2812B_BIT_1<<4)|WS2812B_BIT_1)
#define WS2812B_NIB_1000					((WS2812B_BIT_1<<12)|(WS2812B_BIT_0<<8)|(WS2812B_BIT_0<<4)|WS2812B_BIT_0)
#define WS2812B_NIB_1001					((WS2812B_BIT_1<<12)|(WS2812B_BIT_0<<8)|(WS2812B_BIT_0<<4)|WS2812B_BIT_1)
#define WS2812B_NIB_1010					((WS2812B_BIT_1<<12)|(WS2812B_BIT_0<<8)|(WS2812B_BIT_1<<4)|WS2812B_BIT_0)
#define WS2812B_NIB_1011					((WS2812B_BIT_1<<12)|(WS2812B_BIT_0<<8)|(WS2812B_BIT_1<<4)|WS2812B_BIT_1)
#define WS2812B_NIB_1100					((WS2812B_BIT_1<<12)|(WS2812B_BIT_1<<8)|(WS2812B_BIT_0<<4)|WS2812B_BIT_0)
#define WS2812B_NIB_1101					((WS2812B_BIT_1<<12)|(WS2812B_BIT_1<<8)|(WS2812B_BIT_0<<4)|WS2812B_BIT_1)
#define WS2812B_NIB_1110					((WS2812B_BIT_1<<12)|(WS2812B_BIT_1<<8)|(WS2812B_BIT_1<<4)|WS2812B_BIT_0)
#define WS2812B_NIB_1111					((WS2812B_BIT_1<<12)|(WS2812B_BIT_1<<8)|(WS2812B_BIT_1<<4)|WS2812B_BIT_1)

#define	WS2812B_DATARATE_BPS				(800000)
#define	WS2812B_SPI_BITRATE_BPS				(WS2812B_ENCODING_RATIO * WS2812B_DATARATE_BPS)


/* -------------------------------- +
|									|
|	Helper macros					|
|									|
+ -------------------------------- */
#define	WS2812B_PIXBUF_SIZE_BYTE(pixNb)		(WS2812B_PIXSIZE_BYTE * (uint32_t)(pixNb))
#define	WS2812B_PHYBUF_SIZE_BYTE(pixNb)		(WS2812B_PIXBUF_SIZE_BYTE(pixNb) * WS2812B_ENCODING_RATIO)
#define	WS2812B_MAX_DEV_IN_STRING_FPS(fps)	((WS2812B_DATARATE_BPS / (WS2812B_ENCODEDSIZE_BYTE*8)) / (uint32_t)(fps))


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
/* Encode a number of input byte into the specified format, ready to send on PHY */
tcode_t WS2812B_encode(void * dest, uint8_t * src, uint16_t len);


#ifdef __cplusplus
}
#endif
#endif
