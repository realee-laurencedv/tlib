/*
*	\name		tbug.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Debug tools and utilities
*	\note		
*	\warning	All formated string fct use internal buffering,
				Internal buffering is limited to __TBUG_MAXSTRINGCHAR in len
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/

#ifndef __TBUG_H__
#define __TBUG_H__	1

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <tutil.h>
#include <ttype.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#ifndef TLIB_DISABLE_AUTOINC_CONF
#include <conf.h>
#endif



/* -------------------------------- +
|									|
|	Public constant					|
|									|
+ -------------------------------- */
#define	TBUG_STREAM_ENV			(1)			/* Using environement's stdio printf (useful for testing) */
#define	TBUG_STREAM_SWO			(2)			/* Using SWD's dedicated output (NOT TESTED) */
#define TBUG_STREAM_GPIO		(3)			/* Using n continuous GPIOs as parallel port (NOT IMPLEMENTED) */
#define	TBUG_STREAM_UART		(4)			/* Using a USART or UART peripheral (NOT IMPLEMENTED) */
#define	TBUG_STREAM_USBCDC		(5)			/* Using a CDC IN Endpoint (NOT IMPLEMENTED) */
#define TBUG_STREAM_LOGMEM		(6)			/* Using a Log file in memory (NOT IMPLEMENTED) */

#define	TBUG_VERBOSITY_NONE		(0)
#define	TBUG_VERBOSITY_DEBUG	(10)
#define	TBUG_VERBOSITY_INFO		(20)
#define	TBUG_VERBOSITY_WARNING	(30)
#define	TBUG_VERBOSITY_ERROR	(40)
#define	TBUG_VERBOSITY_CRITICAL	(50)


/* -------------------------------- +
|									|
|	Public config					|
| (define those before to overload) |
+ -------------------------------- */
/* SWO specific settings and functions */
#ifdef TBUG_USE_SWO
	#ifndef TBUG_SWO_LOC
	#define	TBUG_SWO_LOC	(0)
	#endif
	
	 #define	TBUG_STREAM		(TBUG_STREAM_SWO)
#endif

/* UART specific settings and functions */
#ifdef TBUG_USE_UART
	#ifndef TBUG_UART_DEV
	#define	TBUG_UART_DEV	(USART0)
	#endif

	#ifndef TBUG_UART_TXLOC
	#define	TBUG_UART_TXLOC	(0)
	#endif

	#ifndef TBUG_UART_BAUD
	#define	TBUG_UART_BAUD	(1200000)
	#endif

	#define	TBUG_STREAM		(TBUG_STREAM_UART)
#endif

/* Retargettable stream controls */
/* But the valid values list is in this file too... so you cant define it before knowing the actual value... doh */
#ifndef TBUG_STREAM
#define	TBUG_STREAM		(TBUG_STREAM_ENV)
#endif

#ifndef TBUG_VERBOSITY
#define	TBUG_VERBOSITY	TBUG_VERBOSITY_ERROR
#endif

/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
/* Those are generic call, retargetable via TBUG_STREAM */
void tbug_init(void);
void tbug_u8(uint8_t data);
void tbug_u16(uint16_t data);
void tbug_u32(uint32_t data);
void tbug_string(const char * string);
void tbug_array(uint8_t * array, uint32_t len);
void tbug_format(const char * format, ...);
void tbug_log(uint8_t verbosity, const char * user, const char * msg, ...);

/* SWO specific API */
void tbug_swo_init(void);
void tbug_swo_u8(uint8_t data);
void tbug_swo_u16(uint16_t data);
void tbug_swo_u32(uint32_t data);
void tbug_swo_string(const char * string);
void tbug_swo_array(uint8_t * array, uint32_t len);
void tbug_swo_format(const char * format, ...);

/* GPIO specific API */
void tbug_gpio_init(void);
void tbug_gpio_u8(uint8_t data);
void tbug_gpio_u16(uint16_t data);
void tbug_gpio_u32(uint32_t data);
void tbug_gpio_string(const char * string);
void tbug_gpio_array(uint8_t * array, uint32_t len);
void tbug_gpio_format(const char * format, ...);

/* UART specific API */
void tbug_uart_init(void);
void tbug_uart_u8(uint8_t data);
void tbug_uart_u16(uint16_t data);
void tbug_uart_u32(uint32_t data);
void tbug_uart_string(const char * string);
void tbug_uart_array(uint8_t * array, uint32_t len);
void tbug_uart_format(const char * format, ...);

/* USB CDC specific API */
void tbug_usbcdc_init(void);
void tbug_usbcdc_u8(uint8_t data);
void tbug_usbcdc_u16(uint16_t data);
void tbug_usbcdc_u32(uint32_t data);
void tbug_usbcdc_string(const char * string);
void tbug_usbcdc_array(uint8_t * array, uint32_t len);
void tbug_usbcdc_format(const char * format, ...);

/* LOGMEM specific API */
void tbug_logmem_init(void);
void tbug_logmem_u8(uint8_t data);
void tbug_logmem_u16(uint16_t data);
void tbug_logmem_u32(uint32_t data);
void tbug_logmem_string(const char * string);
void tbug_logmem_array(uint8_t * array, uint32_t len);
void tbug_logmem_format(const char * format, ...);


#ifdef __cplusplus
}
#endif
#endif

