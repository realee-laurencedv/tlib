/*
*	\name		tusi.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Universal Streaming Interface common definition
*	\note		
*	\warning	None
*	\license	refer to LICENSE.MD included, if not, contact admin@realee.tech
*/

#ifndef __TUSI_H_
#define	__TUSI_H_	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <ttype.h>
#include <tutil.h>


/* -------------------------------- +
|									|
|	Type							|
|									|
+ -------------------------------- */
typedef struct tusi_s {
	tusi_s *			self;
	struct {
		ttype_t			wordType;
		void const *	hw;
		CMUclock_t		clk;
		IRQn_Type		irqRx;
		IRQn_Type		irqTx;
	}ctl;
	void const *		buf;
	const uint32_t		bufLen;

	// tcode_t (*init)(void * tusi);
	uint32_t (*tx)(void * src, uint32_t len);
	uint32_t (*rx)(void * dst, uint32_t len);
	uint32_t (*trx)(void * src, void * dst, uint32_t len);
	tcode_t (*ioctl)(void * args);
}tusi_t;

tcode_t tusi_init(tusi_t * tusi);

#ifdef __cplusplus
}
#endif
#endif	/* __TUSI_H_ */

