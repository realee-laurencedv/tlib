/*
*	\name		tcol.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Color definitions and std types
*	\note		
*	\warning	None
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/
#ifndef __TCOL_H__
#define __TCOL_H__	1

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
/* autoconf */
#ifndef TLIB_DISABLE_AUTOINC_CONF
#include <conf.h>
#endif

/* environement */
#include <tutil.h>
#include <ttype.h>


/* -------------------------------- +
|									|
|	Color types						|
|									|
+ -------------------------------- */
/* Red, Green, Blue */
typedef union tcol_rgb_u {
	uint8_t bytes[3];
	struct {
		uint8_t	r;
		uint8_t	g;
		uint8_t b;
	};
}tcol_rgb_t;

/* Red, Green, Blue, White */
typedef union tcol_rgbw_u {
	uint8_t bytes[4];
	uint32_t all;
	struct {
		uint8_t	r;
		uint8_t	g;
		uint8_t b;
		uint8_t w;
	};
}tcol_rgbw_t;

#ifdef __cplusplus
}
#endif
#endif
