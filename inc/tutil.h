/*
*	\name		tutil.h
*	\author		Laurence DV
*	\version	1.5.0
*	\brief		General utilities and macro tools
*	\note
*	\warning	None
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/

#ifndef __TUTIL_H__
#define __TUTIL_H__	1

#ifdef __cplusplus
	extern "C" {
#endif

#include <stdint.h>


/* -------------------------------- +
|									|
|	Define							|
|									|
+ -------------------------------- */
#ifndef BIT0		/* Very weak redefinition safety */
#define	BIT0		(0x0000000000000001)
#define	BIT1		(0x0000000000000002)
#define	BIT2		(0x0000000000000004)
#define	BIT3		(0x0000000000000008)
#define	BIT4		(0x0000000000000010)
#define	BIT5		(0x0000000000000020)
#define	BIT6		(0x0000000000000040)
#define	BIT7		(0x0000000000000080)
#define	BIT8		(0x0000000000000100)
#define	BIT9		(0x0000000000000200)
#define	BIT10		(0x0000000000000400)
#define	BIT11		(0x0000000000000800)
#define	BIT12		(0x0000000000001000)
#define	BIT13		(0x0000000000002000)
#define	BIT14		(0x0000000000004000)
#define	BIT15		(0x0000000000008000)
#define	BIT16		(0x0000000000010000)
#define	BIT17		(0x0000000000020000)
#define	BIT18		(0x0000000000040000)
#define	BIT19		(0x0000000000080000)
#define	BIT20		(0x0000000000100000)
#define	BIT21		(0x0000000000200000)
#define	BIT22		(0x0000000000400000)
#define	BIT23		(0x0000000000800000)
#define	BIT24		(0x0000000001000000)
#define	BIT25		(0x0000000002000000)
#define	BIT26		(0x0000000004000000)
#define	BIT27		(0x0000000008000000)
#define	BIT28		(0x0000000010000000)
#define	BIT29		(0x0000000020000000)
#define	BIT30		(0x0000000040000000)
#define	BIT31		(0x0000000080000000)
#define	BIT32		(0x0000000100000000)
#define	BIT33		(0x0000000200000000)
#define	BIT34		(0x0000000400000000)
#define	BIT35		(0x0000000800000000)
#define	BIT36		(0x0000001000000000)
#define	BIT37		(0x0000002000000000)
#define	BIT38		(0x0000004000000000)
#define	BIT39		(0x0000008000000000)
#define	BIT40		(0x0000010000000000)
#define	BIT41		(0x0000020000000000)
#define	BIT42		(0x0000040000000000)
#define	BIT43		(0x0000080000000000)
#define	BIT44		(0x0000100000000000)
#define	BIT45		(0x0000200000000000)
#define	BIT46		(0x0000400000000000)
#define	BIT47		(0x0000800000000000)
#define	BIT48		(0x0001000000000000)
#define	BIT49		(0x0002000000000000)
#define	BIT50		(0x0004000000000000)
#define	BIT51		(0x0008000000000000)
#define	BIT52		(0x0010000000000000)
#define	BIT53		(0x0020000000000000)
#define	BIT54		(0x0040000000000000)
#define	BIT55		(0x0080000000000000)
#define	BIT56		(0x0100000000000000)
#define	BIT57		(0x0200000000000000)
#define	BIT58		(0x0400000000000000)
#define	BIT59		(0x0800000000000000)
#define	BIT60		(0x1000000000000000)
#define	BIT61		(0x2000000000000000)
#define	BIT62		(0x4000000000000000)
#define	BIT63		(0x8000000000000000)
#endif


/*--------------------------------- +
|									|
|	Data manipulation macro			|
|									|
+ -------------------------------- */
#define __XCAT(a,b)						(a##b)
#define	CONCAT(a,b)						(__XCAT(a,b))								/* concatenate 2 preprocessor string */
#define CONCAT3(a,b,c)					(__XCAT(__XCAT(a,b),c))						/* concatenate 3 preprocessor string */
#define	CONCAT4(a,b,c,d)				(__XCAT(__XCAT(a,b),__XCAT(c,d)))			/* concatenate 4 preprocessor string */
#define SWAP(a, b)						(((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))	/* swap in memory value a with value b */
#define FORCE_BIT(target, mask, value)	((target) ^= ((value) ^ (target)) & (mask))	/* force the value on the target through the masked bits */
#define SET_BIT(target, mask)			((target) |= (mask))						/* force the masked bit to 1 */
#define CLR_BIT(target, mask)			((target) &= ~(mask))						/* force the masked bit to 0 */
#define TOG_BIT(target, mask)			((target) ^= (mask))						/* inverse the masked bit value */
#define GET_BIT(target, offset)			((target>>offset)&0x01)						/* return the bit designated by the offset*/


/*--------------------------------- +
|									|
|	Arithmetic macro				|
|									|
+ -------------------------------- */
#define	MAX(a,b)						__extension__({__typeof__(a) _a = (a); __typeof__(b) _b = (b); _a > _b ? _a : _b;})		/* taken from em_common.h */
#define	MIN(a,b)						__extension__({__typeof__(a) _a = (a); __typeof__(b) _b = (b); _a < _b ? _a : _b;})		/* taken from em_common.h */
#define	ABS(a)							((a) > 0 ? (a) : -(a))																	/* Absolute value of a number */
#define ABSDELTA(a,b)					((a) > (b) ? ((a)-(b)) : ((b)-(a)))														/* Absolute difference between two number */


/*--------------------------------- +
|									|
|	Execution macro					|
|									|
+ -------------------------------- */
#define NOP()							__ASM("nop")								/* NOP instruction call */
#define	EATJOULES(howMany)				(while((--((int32_t)howMany)) > 0))			/* Uses the CPU as an entropy creation device using energy to do absolutely nothing useful. (aka delay) */
																					/* WARNING: By using this function you are moraly obligated to write a comment using "nom nom"
																					 * 	or "rob" describing why you didn't use an rtos service for your particular usage, not a joke */
#define	TBREAK()						__BKPT(1)									/* Add a breakpoint */
#define	TBRAKE()						(TBREAK())


/*--------------------------------- +
|									|
|	Compilation macro				|
|									|
+ -------------------------------- */
#define TLIB_WEAK							__attribute__((weak))
#define TLIB_PRIVATE						static
#define TLIB_INLINE							inline
#define TLIB_STATIC_INLINE					static inline
#define TLIB_PACKED							__attribute__((packed))


/*--------------------------------- +
|									|
|	Warning sushing					|
|	Sadly those cant be macros...	|
+ -------------------------------- */
/* To stop warning for unused function and parameters */
/* For others see: https://gcc.gnu.org/onlinedocs/gcc-4.1.2/gcc/Warning-Options.html */

/* To ignore : */
/* #pragma GCC diagnostic ignored "-Wunused-function" */
/* #pragma GCC diagnostic ignored "-Wunused-parameter" */

/* To re-enable: */
/* #pragma GCC diagnostic warning "-Wunused-function" */
/* #pragma GCC diagnostic warning "-Wunused-parameter" */


#ifdef __cplusplus
}
#endif
#endif
