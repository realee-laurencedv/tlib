/*
*	\name		thal.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Hardware Abstraction layer for tlib
*	\note		
*	\warning	None
*	\license	refer to LICENSE.MD included, if not, contact admin@realee.tech
*/

#ifndef __THAL_H__
#define	__THAL_H__	1

#ifdef __cplusplus
	extern "C" {
#endif

/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

/* autoconf */
#ifndef TLIB_DISABLE_AUTOINC_CONF
#include <conf.h>
#endif

/* tools */
#include <tutil.h>
#include <ttype.h>


/* -------------------------------- +
|									|
|	General definitions				|
|									|
+ -------------------------------- */
/* Supported hw platform */
typedef enum thal_hwPlatform_e {
	thal_hw_virtual,		/* Weak defined minimal implementation for tests */
	thal_hw_efm32gg11,		/* NOT IMPLEMENTED */
	thal_hw_efm32gg12,		/* NOT IMPLEMENTED */
	thal_hw_efm32tg11,		/* NOT IMPLEMENTED */
	thal_hw_lpc55,			/* NOT IMPLEMENTED */
}thal_hwPlatform_t;

/* API information container definition */
typedef struct thal_info_s {
	const uint32_t				version;
	const thal_hwPlatform_t		hwPlatform;
}thal_info_t;

/* API status structure definition */
typedef union thal_status_u {
	uint32_t			all;
	struct {
		uint32_t		hwCoupled	:1;
		uint32_t		inited		:1;
		uint32_t					:30;
	};
}thal_status_t;

/* General power level definition (may or may not be supported by peripheral) */
typedef enum thal_powerLvl_e {
	thal_powerLvl_off =				0x0,
	thal_powerLvl_minimal =			0x4,
	thal_powerLvl_low =				0x8,
	thal_powerLvl_reduced =			0xC,
	thal_powerLvl_full =			0xF,
}thal_powerLvl_t;

/* Abtracted hw peripheral list */
typedef enum thal_type_e {
	thal_type_none =				0x00,
	thal_type_cpu =					0x01,
	thal_type_dma =					0x02,
	thal_type_clk =					0x03,
	thal_type_gpio =				0x04,
	thal_type_timer =				0x05,
	thal_type_uart =				0x06,
	thal_type_spi =					0x07,
	thal_type_i2c =					0x08,
	thal_type_i2s =					0x09,
	thal_type_adc =					0x0A,
	thal_type_dac =					0x0B,
}thal_type_t;

/* -------------------------------- +
|	Protoypes of all hw parts		|
+ -------------------------------- */
struct thal_s;
struct thal_cpu_s;
struct thal_dma_s;
struct thal_clk_s;
struct thal_timer_s;
struct thal_gpio_s;
struct thal_uart_s;
struct thal_spi_s;
struct thal_i2c_s;
struct thal_i2s_s;
struct thal_adc_s;
struct thal_dac_s;

typedef struct thal_hwMap_s {
	struct thal_cpu_s *			cpu;
	struct thal_dma_s *			dma;
	struct thal_clk_s *			clk;
	struct thal_timer_s *		timer;
	struct thal_gpio_s *		gpio;
	struct thal_uart_s *		uart;
	struct thal_spi_s *			spi;
	struct thal_i2c_s *			i2c;
	struct thal_i2s_s *			i2s;
	struct thal_adc_s *			adc;
	struct thal_dac_s *			dac;
}thal_hwMap_t;

/* -------------------------------- +
|									|
|	Public API definitions			|
|									|
+ -------------------------------- */

/* -------------------------------- +
|	Run-time						|
+ -------------------------------- */
/*	thal handle definition
	this is only object needed at run-time */
typedef struct thal_s {
	thal_info_t					info;			/* Identification information */
	thal_status_t				stat;			/* Current status of thal instance */
	thal_hwMap_t				hw;				/* HW mapping of all abstracted parts */
}thal_t;

extern thal_t					_thal;			/* thal global instance handle (must be declared in specific thal implementation) */

/* Initialize and reset a thal instance */
thal_t * thal_init(thal_t * thal);

/* Graceful termination a thal instance */
thal_t * thal_term(thal_t * thal);

/* -------------------------------- +
|	CPU								|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
typedef enum thal_cpu_type_e {
	thal_cpu_type_none =		0x00,
	thal_cpu_type_virt8 =		0x01,
	thal_cpu_type_virt16 =		0x02,
	thal_cpu_type_virt32 =		0x03,
	thal_cpu_type_virt64 =		0x04,

	thal_cpu_type_cm0p =		0x10,
	thal_cpu_type_cm3 =			0x11,
	thal_cpu_type_cm4 =			0x12,
	thal_cpu_type_cm4f =		0x13,
	thal_cpu_type_cm7 =			0x14,
	thal_cpu_type_cm23 =		0x15,
	thal_cpu_type_cm33 =		0x16,
	thal_cpu_type_cm33f =		0x17,

	thal_cpu_type_cm4r =		0x20,
	thal_cpu_type_cm5r =		0x21,

	thal_cpu_type_misp32 =		0x30,
}thal_cpu_type_t;

typedef union thal_cpu_ctl_u {
	uint32_t						all;
	struct {
		uint32_t					en			:1;			/* Cpu is running */
		uint32_t								:31;
	};
}thal_cpu_ctl_t;

typedef union thal_cpu_stat_u {
	uint32_t						all;
	struct {
		uint32_t					running		:1;			/* Cpu is running */
		uint32_t					atomic		:1;			/* Cpu is in atomic mode (uninterruptable) */
		uint32_t								:30;
	};
}thal_cpu_stat_t;

/* -- Abstracted atomic parts -- */
typedef struct thal_cpu_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	const char *					name;					/* Name of the abstracted hw (string) */
	const thal_cpu_type_t			type;					/* Type of the abstracted hw */
	thal_cpu_ctl_t					ctl;
	thal_cpu_stat_t					stat;
}thal_cpu_t;


/* -- API -- */

/* Return current atomic status, and enter atomic mode regardless */
bool thal_cpu_atomicEnter(thal_cpu_t * cpu);

/* Exit current atomic mode and restore atomic status given */
void thal_cpu_atomicExit(thal_cpu_t * cpu, bool state);


/* -------------------------------- +
|	DMA								|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
struct thal_dma_xfer_s;
struct thal_dmach_s;
struct thal_dma_s;

/* Supported type of DMA controller */
typedef enum thal_dma_hwType_e {
	thal_dma_type_none =		0x00,
	thal_dma_type_simple =		0x01,				/* single xfer/ch, single ch at a time, prioritized by channel */
	thal_dma_type_linked =		0x11,				
}thal_dma_type_t;

typedef enum thal_dma_cmd_e {
	thal_dma_cmd_enable =			1,
	thal_dma_cmd_disable =			2,
	thal_dma_cmd_none =				0xFFFFFFFF,
}thal_dma_cmd_t;

typedef union thal_dma_capabilities_u {
	uint32_t		all;
	struct TLIB_PACKED {
		uint32_t	callback		:1;				/* Callback executed after xfer is done */
		uint32_t	hwLink			:1;				/* HW auto start of next xfer after current is done */
		uint32_t	prioritised		:1;				/* HW priority capable */
		uint32_t					:29;
	};
}thal_dma_capabilities_t;

/* Abstracted type of trigger event */
typedef enum thal_dma_trig_e {
	thal_dma_trig_none =				0x00000000,
	thal_dma_trig_soft =				0x00000001,
}thal_dma_trig_t;

/* Actual value triggering */
typedef enum thal_dma_trigVal_s {
	thal_dma_trigVal_lowLevel =			0b00,
	thal_dma_trigVal_risingEdge =		0b01,
	thal_dma_trigVal_fallingEdge =		0b10,
	thal_dma_trigVal_highLevel =		0b11,
}thal_dma_trigVal_t;

/* Supported transfer sequencing options */
typedef enum thal_dma_sequencing_s {
	thal_dma_seq_single =				0b00,
	thal_dma_seq_reload =				0b01,
	thal_dma_seq_linked =				0b10,
}thal_dma_sequencing_t;

typedef union thal_dma_stat_u {
	uint32_t						all;
	struct TLIB_PACKED {
		uint32_t					busy			:1;		/* Currently doing something */
		uint32_t					curCh			:8;		/* Currently operating DMACH */
		uint32_t									:23;
	};
}thal_dma_stat_t;

typedef union thal_dmach_stat_u {
	uint32_t						all;
	struct TLIB_PACKED {
		uint32_t					trigged		:1;			/* This channel is triggered */
		uint32_t					irqFlag		:1;			/* This channel requested an interrupt */
		uint32_t								:30;
	};
}thal_dmach_stat_t;

/* Callback after xfer is done */
typedef void (*thal_dma_xferCb)(struct thal_dma_s * dma, struct thal_dma_xfer_s * xfer, void * userData);

/* -- Abstracted atomic parts -- */
/* DMA Transfer, contain all necessary information to execute a memory transfer, can be linked, reapeated, etc */
/* ----------------------------------------------------------------------------------------------------------- */
typedef struct thal_dma_xfer_s{
	void *							src;					/* Source pointer */
	void *							dst;					/* Destination pointer */
	uint32_t						cnt;					/* Total elements nb to transfer */
	union {
		uint32_t					ctl;
		struct TLIB_PACKED {
			uint32_t				irqEn		:1;			/* This transfer trigger an interrupt when done */
			uint32_t				eleSize		:4;			/* Element size (in byte) */
			uint32_t				srcIncEn	:1;			/* Src auto increment after each element */
			int32_t					srcInc		:3;			/* Src increment size (in byte), can be negative */
			uint32_t				dstIncEn	:1;			/* Dst auto increment after each element */
			int32_t					dstInc		:3;			/* Dst increment size (in byte), can be negative */
			uint32_t				priority	:8;			/* Priority of this xfer, 0 being most */
			uint32_t							:10;
		};
	};
	union {
		uint32_t					ctl;
		struct TLIB_PACKED {
			thal_dma_trig_t			src;					/* Source of trigger */
			thal_dma_trigVal_t		val;					/* Value of trigger*/
		};	
	}trigger;
	struct TLIB_PACKED {
		thal_dma_sequencing_t		seq;					/* What to do after the current xfer is done  */
		struct thal_dma_xfer_s *	next;					/* Next xfer ptr if linked, NULL for single xfer */
	}link;
	thal_dma_xferCb					cb;						/* Callback called after this transfer is done (single cb support for now) */
}thal_dma_xfer_t;

/* DMA Channel, multiple can be ready (waiting trigger) in a single dma, only one is executed at once,  */
/* ---------------------------------------------------------------------------------------------------- */
typedef struct thal_dmach_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	thal_dma_xfer_t *				xfer;					/* Currently operating transfer of this channel */
	tcode_t							(*attachXfer)		(struct thal_dmach_s * ch, thal_dma_xfer_t * xfer);		/* Attach a transfer to this channel (will start when triggered) */
	tcode_t							(*detachXfer)		(struct thal_dmach_s * ch, thal_dma_xfer_t * xfer);		/* Detach a transfer from this channel (also disable the channel) */
	thal_dmach_stat_t				(*stat)				(struct thal_dmach_s * ch);								/* Return current status from this channel */
}thal_dmach_t;

/* DMA, actual parts that do the memory transfer, one instance per hw dma master (usually) */
/* --------------------------------------------------------------------------------------- */
typedef struct thal_dma_s {
	const thal_type_t				type;			/* Type of the abstracted hw (should always be thal_type_dma) */
	const char *					name;			/* Name of the abstracted hw (string) */
	void * const					hw;				/* Pointer to start of hw register bank */
	thal_dmach_t * const			ch;				/* DMACH array */
	const uint8_t					chNb;			/* Total DMACH number */
	tcode_t							(*init)				(struct thal_dma_s * dma);									/* Initialize for minimal operation */
	tcode_t							(*term)				(struct thal_dma_s * dma);									/* Gracefully terminate */
	thal_dma_capabilities_t			(*getCapabilities)	(struct thal_dma_s * dma);									/* Return a description of abstracted hw */
	tcode_t 						(*pwr)				(struct thal_dma_s * dma, thal_powerLvl_t newPowerLvl);	/* Configure the abstracted hw into power mode */
	tcode_t 						(*cmd)				(struct thal_dma_s * dma, thal_dma_cmd_t command);			/* Command the abstracted hw to do something (ie start/stop) */
	thal_dma_stat_t					(*stat)				(struct thal_dma_s * dma);									/* Return current abstracted hw status in generic form */
}thal_dma_t;

/* -- API -- */
tcode_t thal_dma_init(thal_dma_t * dma);
tcode_t thal_dma_term(thal_dma_t * dma);
thal_dma_capabilities_t thal_dma_getCapabilities(thal_dma_t * dma);
tcode_t thal_dma_pwr(thal_dma_t * dma, thal_powerLvl_t newPowerLvl);
tcode_t thal_dma_cmd(thal_dma_t * dma, thal_dma_cmd_t command);
thal_dma_stat_t thal_dma_stat(thal_dma_t * dma);

tcode_t thal_dma_xferInit(thal_dma_xfer_t * xfer);
tcode_t thal_dma_xferTerm(thal_dma_xfer_t * xfer);
tcode_t thal_dma_xferCbAttach(thal_dma_xfer_t * xfer, thal_dma_xferCb cb);
tcode_t thal_dma_xferCbDetach(thal_dma_xfer_t * xfer, thal_dma_xferCb cb);

thal_dmach_stat_t thal_dmach_statGet(thal_dmach_t * ch);
tcode_t thal_dmach_xferAttach(thal_dmach_t * ch, thal_dma_xfer_t * xfer);
tcode_t thal_dmach_xferDetach(thal_dmach_t * ch, thal_dma_xfer_t * xfer);


/* -------------------------------- +
|	CLK								|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
typedef enum thal_clk_type_e {
	thal_clk_none =				0x0000,
	thal_clk_mainSrc =			0x0001,
	thal_clk_cpu =				0x0002,
}thal_clk_type_t;

/* -- Abstracted atomic parts -- */
typedef struct thal_clk_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	const char *					name;					/* Name of the abstracted hw (string) */
}thal_clk_t;

/* -- API -- */
void thal_clk_setFreq(void);
void thal_clk_getFreq(void);


/* -------------------------------- +
|	GPIO							|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
typedef enum thal_gpio_mode_e {
	thal_gpio_mode_none =		0x0000,
	thal_gpio_mode_disabled =	0x0200,
	thal_gpio_mode_input =		0x0001,
	thal_gpio_mode_output =		0x0100,
	thal_gpio_mode_openDrain =	0x0101,
}thal_gpio_mode_t;

typedef enum thal_gpio_edge_e {
	thal_gpio_edge_none =		0x00,
	thal_gpio_edge_rising =		0x01,
	thal_gpio_edge_falling =	0x02,
}thal_gpio_edge_t;

typedef enum thal_gpio_irq_e {
	thal_gpio_irq_none =		0x00,
	thal_gpio_irq_edge =		0x01,
	thal_gpio_irq_value =		0x02,
}thal_gpio_irq_t;

/* -- Abstracted atomic parts -- */
typedef struct thal_gpiopin_s {
	void * const					hw;						/* Pointer to start of hw register bank */
}thal_gpiopin_t;

typedef struct thal_gpio_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	const char *					name;					/* Name of the abstracted hw (string) */
}thal_gpio_t;

/* -- API -- */
void thal_gpio_reset(void);
void thal_gpio_setup(void);
void thal_gpio_control(void);
void thal_gpio_getStat(void);

void thal_gpio_setIrq(void);
void thal_gpio_getIrq(void);
void thal_gpio_setIrqFlag(void);
void thal_gpio_getIrqFlag(void);

void thal_gpio_setPin(void);
void thal_gpio_getPin(void);

void thal_gpio_setPort(void);
void thal_gpio_getPort(void);


/* -------------------------------- +
|	TIMER							|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
typedef enum thal_timer_mode_e {
	thal_timer_mode_none =			0x0000,
	thal_timer_mode_disabled =		0x0001,
	thal_timer_mode_countUp =		0x0101,
	thal_timer_mode_countDn =		0x0102,
	thal_timer_mode_countUpDn =		0x0103,
	thal_timer_mode_singleUp =		0x0201,
	thal_timer_mode_singleDn =		0x0202,
	thal_timer_mode_singleUpDn =	0x0203,
}thal_timer_mode_t;

typedef enum thal_timer_chMode_e {
	thal_timer_chMode_none =		0x0000,
	thal_timer_chMode_disabled =	0x0001,
	thal_timer_chMode_pwm =			0x0100,
	thal_timer_chMode_capture =		0x0200,
	thal_timer_chMode_compare =		0x0300,
}thal_timer_chMode_t;

typedef enum thal_timer_irq_e {
	thal_timer_irq_none =			0x00000000,
	thal_timer_irq_ovf =			0x00000001,

	thal_timer_irq_ch0 =			0x00010000,
	thal_timer_irq_ch1 =			0x00020000,
	thal_timer_irq_ch2 =			0x00040000,
	thal_timer_irq_ch3 =			0x00080000,
	thal_timer_irq_ch4 =			0x00100000,
	thal_timer_irq_ch5 =			0x00200000,
	thal_timer_irq_ch6 =			0x00400000,
	thal_timer_irq_ch7 =			0x00800000,
	thal_timer_irq_ch8 =			0x01000000,
	thal_timer_irq_ch9 =			0x02000000,
	thal_timer_irq_ch10 =			0x04000000,
	thal_timer_irq_ch11 =			0x08000000,
	thal_timer_irq_ch12 =			0x10000000,
	thal_timer_irq_ch13 =			0x20000000,
	thal_timer_irq_ch14 =			0x40000000,
	thal_timer_irq_ch15 =			0x80000000,
}thal_timer_irq_t;

/* -- Abstracted atomic parts -- */
typedef struct thal_timer_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	const char *					name;					/* Name of the abstracted hw (string) */
}thal_timer_t;

typedef struct thal_timercc_s {
	void * const					hw;						/* Pointer to start of hw register bank */
}thal_timercc_t;

/* -- API -- */
void thal_timer_reset(void);
void thal_timer_setup(void);
void thal_timer_control(void);
void thal_timer_getStat(void);

void thal_timer_setIrq(void);
void thal_timer_getIrq(void);
void thal_timer_setIrqFlag(void);
void thal_timer_getIrqFlag(void);

void thal_timer_setCnt(void);
void thal_timer_getCnt(void);

void thal_timer_chReset(void);
void thal_timer_chSetup(void);
void thal_timer_chControl(void);
void thal_timer_chGetStat(void);
void thal_timer_chSetCnt(void);
void thal_timer_chGetCnt(void);


/* -------------------------------- +
|	UART							|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
typedef enum thal_uart_mode_e {
	thal_uart_mode_none =			0x0000,
	thal_uart_mode_disabled =		0x0001,
	thal_uart_mode_rx =				0x0101,
	thal_uart_mode_tx =				0x0102,
	thal_uart_mode_rxtx =			0x0103,
	thal_uart_mode_irda =			0x0200,
}thal_uart_mode_t;

typedef enum thal_uart_irq_e {
	thal_uart_irq_none =			0x0000,
	thal_uart_irq_rx =				0x0001,
	thal_uart_irq_tx =				0x0002,
	thal_uart_irq_frameErr =		0x0004,
	thal_uart_irq_timeOut =			0x0008,
	thal_uart_irq_rxThrs =			0x0010,
	thal_uart_irq_txThrs =			0x0020,
	thal_uart_irq_rxFull =			0x0100,
	thal_uart_irq_txEmpty =			0x0200,
	thal_uart_irq_rxOvf =			0x1000,
	thal_uart_irq_txUdf =			0x2000,
}thal_uart_irq_t;

/* -- Abstracted atomic parts -- */
typedef struct thal_uart_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	const char *					name;					/* Name of the abstracted hw (string) */
}thal_uart_t;

/* -- API -- */
void thal_uart_reset(void);
void thal_uart_setup(void);
void thal_uart_control(void);
void thal_uart_getStat(void);

void thal_uart_setFreq(void);
void thal_uart_getFreq(void);

void thal_uart_setIrq(void);
void thal_uart_getIrq(void);
void thal_uart_setIrqFlag(void);
void thal_uart_getIrqFlag(void);

void thal_uart_tx(void);
void thal_uart_rx(void);
void thal_uart_trx(void);


/* -------------------------------- +
|	SPI								|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
typedef enum thal_spi_mode_e {
	thal_spi_mode_none =				0x0000,
	thal_spi_mode_disabled =			0x0001,
	thal_spi_mode_master_duplex =		0x0100,
	thal_spi_mode_master_halfduplex =	0x0101,
	thal_spi_mode_master_sequential =	0x0102,
	thal_spi_mode_slave_duplex =		0x0200,
	thal_spi_mode_slave_halfduplex =	0x0201,
	thal_spi_mode_slave_sequential =	0x0202,
}thal_spi_mode_t;

typedef enum thal_spi_irq_e {
	thal_spi_irq_none =					0x0000,
	thal_spi_irq_rx =					0x0001,
	thal_spi_irq_tx =					0x0002,
	thal_spi_irq_rxThrs =				0x0010,
	thal_spi_irq_txThrs =				0x0020,
	thal_spi_irq_rxFull =				0x0100,
	thal_spi_irq_txEmpty =				0x0200,
	thal_spi_irq_rxOvf =				0x1000,
	thal_spi_irq_txUdf =				0x2000,
}thal_spi_irq_t;

/* -- Abstracted atomic parts -- */
typedef struct thal_spi_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	const char *					name;					/* Name of the abstracted hw (string) */
}thal_spi_t;

/* -- API -- */
void thal_spi_reset(void);
void thal_spi_setup(void);
void thal_spi_control(void);
void thal_spi_getStat(void);

void thal_spi_setFreq(void);
void thal_spi_getFreq(void);

void thal_spi_setIrq(void);
void thal_spi_getIrq(void);
void thal_spi_setIrqFlag(void);
void thal_spi_getIrqFlag(void);

void thal_spi_tx(void);
void thal_spi_rx(void);
void thal_spi_trx(void);


/* -------------------------------- +
|	I2C								|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
typedef enum thal_i2c_mode_e {
	thal_i2c_mode_none =			0x0000,
	thal_i2c_mode_disabled =		0x0001,
	thal_i2c_mode_master =			0x0100,
	thal_i2c_mode_slave =			0x0200,
}thal_i2c_mode_t;

typedef enum thal_i2c_irq_e {
	thal_i2c_irq_none =				0x0000,
	thal_i2c_irq_rx =				0x0001,
	thal_i2c_irq_tx =				0x0002,
	thal_i2c_irq_slaveReq =			0x0100,
}thal_i2c_irq_t;

/* -- Abstracted atomic parts -- */
typedef struct thal_i2c_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	const char *					name;					/* Name of the abstracted hw (string) */
}thal_i2c_t;

/* -- API -- */
void thal_i2c_reset(void);
void thal_i2c_setup(void);
void thal_i2c_control(void);
void thal_i2c_getStat(void);

void thal_i2c_setFreq(void);
void thal_i2c_getFreq(void);

void thal_i2c_setIrq(void);
void thal_i2c_getIrq(void);
void thal_i2c_setIrqFlag(void);
void thal_i2c_getIrqFlag(void);

void thal_i2c_tx(void);
void thal_i2c_rx(void);


/* -------------------------------- +
|	I2S								|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
/* -- Abstracted atomic parts -- */
typedef struct thal_i2s_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	const char *					name;					/* Name of the abstracted hw (string) */
}thal_i2s_t;

/* -- API -- */
void thal_i2s_reset(void);
void thal_i2s_setup(void);
void thal_i2s_control(void);
void thal_i2s_getStat(void);

void thal_i2s_setFreq(void);
void thal_i2s_getFreq(void);

void thal_i2s_setIrq(void);
void thal_i2s_getIrq(void);
void thal_i2s_setIrqFlag(void);
void thal_i2s_getIrqFlag(void);

void thal_i2s_tx(void);
void thal_i2s_rx(void);


/* -------------------------------- +
|	ADC								|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
typedef enum thal_adc_mode_e {
	thal_adc_mode_none =			0x0000,
	thal_adc_mode_disabled =		0x0001,
	thal_adc_mode_single =			0x0100,
	thal_adc_mode_group =			0x0101,
	thal_adc_mode_continuous =		0x0102,
}thal_adc_mode_t;

typedef enum thal_adc_irq_e {
	thal_adc_irq_none =				0x0000,
	thal_adc_irq_dataReady =		0x0001,
	thal_adc_irq_singleDone =		0x0100,
	thal_adc_irq_groupDone =		0x0200,
}thal_adc_irq_t;

/* -- Abstracted atomic parts -- */
typedef struct thal_adc_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	const char *					name;					/* Name of the abstracted hw (string) */
}thal_adc_t;

/* -- API -- */
void thal_adc_reset(void);
void thal_adc_setup(void);
void thal_adc_control(void);
void thal_adc_getStat(void);

void thal_adc_setIrq(void);
void thal_adc_getIrq(void);
void thal_adc_setIrqFlag(void);
void thal_adc_getIrqFlag(void);

void thal_adc_convertSingle(void);
void thal_adc_convertGroup(void);
void thal_adc_convertContinuous(void);


/* -------------------------------- +
|	DAC								|
+ -------------------------------- */
/* -- Abstracted common data representation -- */
/* -- Abstracted atomic parts -- */
/* -- API -- */
typedef enum thal_dac_mode_e {
	thal_dac_mode_none =			0x0000,
	thal_dac_mode_disabled =		0x0001,
	thal_dac_mode_single =			0x0100,
	thal_dac_mode_group =			0x0101,
	thal_dac_mode_continuous =		0x0102,
}thal_dac_mode_t;

typedef enum thal_dac_irq_e {
	thal_dac_irq_none =				0x0000,
	thal_dac_irq_ready =			0x0001,
	thal_dac_irq_singleDone =		0x0100,
	thal_dac_irq_groupDone =		0x0200,
}thal_dac_irq_t;

typedef struct thal_dac_s {
	void * const					hw;						/* Pointer to start of hw register bank */
	const char *					name;					/* Name of the abstracted hw (string) */
}thal_dac_t;

void thal_dac_reset(void);
void thal_dac_setup(void);
void thal_dac_control(void);
void thal_dac_getStat(void);

void thal_dac_setIrq(void);
void thal_dac_getIrq(void);
void thal_dac_setIrqFlag(void);
void thal_dac_getIrqFlag(void);

void thal_dac_convertSingle(void);
void thal_dac_convertGroup(void);
void thal_dac_convertContinuous(void);




#ifdef __cplusplus
}
#endif
#endif	/* __THAL_H__ */

