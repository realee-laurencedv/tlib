/*
*	\name		tledstr.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		LED string utility
*	\note		Support 3 and 4 color LEDs
*	\warning	None
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/
#ifndef __TLEDSTR_H__
#define __TLEDSTR_H__	1

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
/* autoconf */
#ifndef TLIB_DISABLE_AUTOINC_CONF
#include <conf.h>
#endif

/* environement */
#include <tutil.h>
#include <ttype.h>
#include <string.h>
#include <tbug.h>
#include <tcol.h>

/* specific */
#include <dev/ws2812b.h>

/* hw */
#include <thal.h>
#include <tpin.h>


/* -------------------------------- +
|									|
|	Public Type						|
|									|
+ -------------------------------- */
typedef enum tledstr_type_e {
	tledstr_none =			0x00,
	tledstr_ws2812b =		0x01,
}tledstr_type_t;

typedef enum tledstr_state_e {
	tledstr_state_unknown =	0x00,
	tledstr_state_ready =	0x10,
	tledstr_state_txing =	0x11,
	tledstr_state_closing =	0x12,
	tledstr_state_error =	0xFF,
}tledstr_state_t;

typedef struct tledstr_s {
	const struct {
		// USART_TypeDef *		hw;					/* Associated usart peripheral */
		thal_spi_t *		hw;
		uint8_t				loc;				/* Location number of the usart (tx only, clk enabled in debug mode) */
		uint8_t				dmaCh;				/* Specific dma channel to use for this string */
		uint16_t			pixNb;				/* Total number of pixel of this led string */
		tledstr_type_t		type;				/* This led string's type of led */
		bool				activeVal;			/* Phy bit active value (ie; true for active-high led string) */
		void *				pixBuf;				/* Pointer to start of the memory array dedicated to the pixel buffer */
		void *				phyBuf;				/* Buffer for the physical interface to work in */
		uint32_t			phyBufLen;			/* Size (in byte) of the phy buffer */
	}config;
	struct {
		// LDMA_Descriptor_t	desc[3];			/* Internal use only */
		// LDMA_TransferCfg_t	conf;				/* Internal use only */
	}ldma;
	tledstr_state_t			state;				/* This ledstr current state */
	uint8_t					pixSize;			/* Internal use, size of each pixel (in byte) */
	uint8_t					xferSize;			/* Internal use, size of encoded pixel (in byte) */
	uint16_t				cnt;				/* Internal use only */
	tcode_t (*encode)(void * dest, uint8_t * src, uint16_t len);
}tledstr_t;


/* -------------------------------- +
|									|
|	Contant							|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Config							|
|									|
+ -------------------------------- */
/* Define this in your conf.h to enable the clk output (useful for logic analyser) */
//#define	TLEDSTR_CLKEN	1


/*--------------------------------- +
|									|
|	Public Macro					|
|									|
+ -------------------------------- */



/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
/*	\brief				Initialize a given LED string
 *	\note				the .config part of the struct should be correctly initialized before given to _init()
 *	\return	tcode		tcode_success when everything went well
 *						tcode_null when given a null pointer
 *						tcode_badConfig when something is wrong in config (can be many things, check code for details)
 *						tcode_tooSmall when phybuffer is too small for at least 2 pixel (or 1 if pixNb is 1)
 */
tcode_t tledstr_init(tledstr_t * tledstr);

tcode_t tledstr_pixelSet(tledstr_t * tledstr, void * src, uint16_t pixID, uint16_t pixNb);
tcode_t tledstr_pixelGet(tledstr_t * tledstr, void * src, uint16_t pixID, uint16_t pixNb);

tcode_t tledstr_frameSend(tledstr_t * tledstr);

void tledstr_dma_cb(tledstr_t * tledstr);


#ifdef __cplusplus
}
#endif
#endif
