/*
*	\name		trec.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Data recorder C-side API
*	\note		Autonomous mode assume you initialized the ldma peripheral in main before calling trec_auto_init()
*	\warning	
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/

#ifndef __TREC_H__
#define __TREC_H__	1

#ifdef __cplusplus
extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
/* autoconf */
#ifndef TLIB_DISABLE_AUTOINC_CONF
#include <conf.h>
#endif

/* environement */
#include <stdint.h>
#include <string.h>
#include <trec_data.h>
#include <tbug.h>
#include <ttime.h>
#include <tatomic.h>

/* hw */


/* -------------------------------- +
|									|
|	Public constant					|
|									|
+ -------------------------------- */
#define TREC_TIMESRC_NONE		0
#define	TREC_TIMESRC_RTCC		1
#define	TREC_TIMESRC_RTC		2
#define	TRED_TIMESRC_TIMER		3


/* -------------------------------- +
|									|
|	Public config					|
| (define those before to overload) |
+ -------------------------------- */
#ifndef TREC_TIMESRC
#define	TREC_TIMESRC	TREC_TIMESRC_NONE		/* Default time source is the internal RTCC */
#endif
#ifndef TREC_BUF_SIZE_BYTE
#define	TREC_BUF_SIZE_BYTE		(4096)			/* Default buffer size for recording is 4K (in ram) */
#endif

#ifdef TREC_AUTO_EN								/* Define this to enable to autonomous output streaming from the recorder */
	#ifdef TREC_AUTO_USE_UART					/* Define this to use a USART in UART mode as output */
		#ifndef	TREC_AUTO_UART
		#define	TREC_AUTO_UART		(USART2)
		#endif
		#ifndef	TREC_AUTO_TXLOC
		#define	TREC_AUTO_TXLOC		(0)
		#endif
		#ifndef	TREC_AUTO_BAUD
		#define	TREC_AUTO_BAUD		(1200000)
		#endif
	#endif

	#ifdef TREC_AUTO_TRIG_USE_TIMER				/* Define this to use a timer to check if something is pending */
		#ifndef	TREC_AUTO_TRIG_TIMER
		#define	TREC_AUTO_TRIG_TIMER		(LETIMER0)
		#endif
		#ifndef TREC_AUTO_TRIG_TIMER_FREQ
		#define	TREC_AUTO_TRIG_TIMER_FREQ	(1000)
		#endif
	#endif
#endif

/* -------------------------------- +
|									|
|	Public type						|
|									|
+ -------------------------------- */
/* Control structure of trec, public for testability */
typedef struct __attribute__((packed)){
	uint32_t	initTime;					/* time stamp of this seession init */
	uint32_t	in;							/* writing index to record in data */
	uint32_t	out;						/* reading index to stream out data */
	uint32_t	cnt;						/* Internal buffer byte counter */
	bool		bufOVF;						/* True only when a buffer overflow occured (auto-shutdown) */
	uint8_t		buf[TREC_BUF_SIZE_BYTE];
}trec_t;

typedef enum __attribute__((packed)){
	trec_recType_none =		0x0,
	trec_recType_empty =	0x1,
	trec_recType_mem =		0x2,
	trec_recType_msg =		0x3,
}trec_recType_t;

typedef struct __attribute__((packed)){
	uint32_t				time;
	uint8_t					user;
	uint8_t					event;
	union {
		uint16_t			ctl;
		struct {
			uint16_t		type:4;
			uint16_t		len:12;
		};
	};
}trec_rec_t;

/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
/* Initialize time keeping and output stream */
void trec_init(void);

/* Add an Event record with n parameters  */
void trec_rec0(uint8_t user, uint8_t event);
void trec_rec1(uint8_t user, uint8_t event, uint32_t val0);
void trec_rec2(uint8_t user, uint8_t event, uint32_t val0, uint32_t val1);
void trec_rec3(uint8_t user, uint8_t event, uint32_t val0, uint32_t val1, uint32_t val2);
void trec_rec4(uint8_t user, uint8_t event, uint32_t val0, uint32_t val1, uint32_t val2, uint32_t val3);

/* Add an Event record with a memory block */
void trec_recMem(uint8_t user, uint8_t event, void * mem, uint16_t len);

/* Add an Event record with a string message */
void trec_recMsg(uint8_t user, uint8_t event, const char * msg);

/* -------------------------------- +
|									|
|	Autonomous output streaming		|
|									|
+ -------------------------------- */
#ifdef TREC_AUTO_EN
/* Initialize the autonomous output streaming */
/* This is already called by trec_init when defined, but still accessible to reinit or other weird ass usage */
void trec_auto_init(void);

/* Triggers a check, if there is pending stuff in the buffer, everything will be emptied asap */
/* This will be automatically called by an internal timer, when implemented, but can always be called externally */
void trec_auto_update(void);

/* Handles the dma irq, used mostly to stop txing when nothing else to tx */
void trec_auto_ldma_irq(void);
#endif

#ifdef __cplusplus
}
#endif
#endif

