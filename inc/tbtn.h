/*
*	\name		tbtn.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Button utilities
*	\note
*	\warning	None
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/
#ifndef __TBTN_H__
#define __TBTN_H__	1

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <tutil.h>
#include <tpin.h>
#include <ttype.h>
#include <ttime.h>


/* -------------------------------- +
|									|
|	Public Type						|
|									|
+ -------------------------------- */
/* Event a button can receive */
typedef enum __attribute__((packed)) tbtn_event_e {
	tbtn_event_none =		0x00,
	tbtn_event_press =		0x10,
	tbtn_event_release =	0x20,
}tbtn_event_t;

/* Callback function format to react to button events */
typedef void(*tbtn_cb_t)(void);

typedef struct tbtn_s {
	tpin_t		pin;
	const bool 	activeVal;
	ttime_t		debounceTime;			/* Not implemented */
	tbtn_cb_t 	pressCb;
	tbtn_cb_t	releaseCb;
}tbtn_t;

/* -------------------------------- +
|									|
|	Contant							|
|									|
+ -------------------------------- */


/*--------------------------------- +
|									|
|	Public Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
tcode_t tbtn_init(tbtn_t * btn);

bool tbtn_getState(tbtn_t * btn);

tcode_t tbtn_attachCb(tbtn_t * btn, tbtn_cb_t cb);
tcode_t tbtn_detachCb(tbtn_t * btn, tbtn_cb_t cb);


#ifdef __cplusplus
}
#endif
#endif
