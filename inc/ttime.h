/*
*	\name		ttime.h
*	\author		Laurence DV
*	\version	1.0.0
*	\brief		Timing and RTCC utilities
*	\note		the storage format is in second-since-epoch in Q31.32, epoch 0 start at 1900-01-01-00:00:00.000
				This is following ntpv4 format to handle 32bit limitation and enable very precise timing
				lsb is 1/4294967296 of a second aka 230 picosec
*	\warning	None
*	\license	refer to LICENSE.md included, if not, contact admin@realee.tech
*/
#ifndef __TTIME_H__
#define __TTIME_H__	1

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <tutil.h>
#include <ttype.h>


/* -------------------------------- +
|									|
|	Public Type						|
|									|
+ -------------------------------- */

typedef union {
	uint64_t		raw;
	struct {
		int32_t		integer;
		uint32_t	frac;
	};
}ttime_t;


/* -------------------------------- +
|									|
|	Contant							|
|									|
+ -------------------------------- */


/*--------------------------------- +
|									|
|	Public Macro					|
|									|
+ -------------------------------- */


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
tcode_t ttime_init(void);

tcode_t ttime_setCurrentTime(ttime_t newTime);


uint32_t ttime_getNow_sec(void);
uint64_t ttime_getNow_secFrac(void);

#ifdef __cplusplus
}
#endif
#endif
