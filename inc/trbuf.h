/*
*	\name		trbuf.h
*	\author		Laurence DV
*	\version	2.0.0
*	\brief		Ring Buffer implementation for various uses
*	\note		Buffer memory must be provided, either with external malloc or any os specific similar tool
				Buffer size is not dynamic, user must create-new,copy,destroy-old if resizing is necessary
				Buffer elements are not dynamic, given by defined memory passed to init()
				MACRO are supplied to help compute the necessary memory, see "Public Macro" section
*	\warning	None
*	\license	refer to LICENSE.MD included, if not, contact admin@realee.tech
*/

#ifndef __TRBUF_H__
#define __TRBUF_H__	1

#ifdef __cplusplus
	extern "C" {
#endif


/* -------------------------------- +
|									|
|	Include							|
|									|
+ -------------------------------- */
#include <tutil.h>
#include <ttype.h>
#include <string.h>


/* -------------------------------- +
|									|
|	Public Type						|
|									|
+ -------------------------------- */
typedef struct __attribute__((packed)) trbuf_s{
	const union {
		uint8_t			ctl;
		struct {
			uint8_t			ovfSel:2;	/* Select the overflow mechanism (00: silent drop | 01: discard oldest) */
			uint8_t			udfSel:2;	/* Select the underflow mechanism (00: return empty | 01: repeat last) */
			uint8_t			flushSel:2;	/* Select the flushing type (00: rapid | 01: complete) */
			uint8_t			:2;
		};
	};
	const ttype_t		eleType;		/* Configured element's type of this ring-buffer, MUST be provided before init */
	volatile uint32_t	eleCnt;			/* Current element count in the buffer, managed and used by trbuf, please use api */
	const uint32_t		len;			/* Total lengh of buffer (in element nb), MUST be provided before init */
	void *				buf;			/* Start address of the actual memory array, MUST be provided before init */
	volatile void *		in;				/* Input/Push/Write pointer to buffer, will be managed by trbuf */
	volatile void *		out;			/* Output/Pull/Read pointer from buffer, will be managed by trbuf */
	void *				end;			/* End address of the actual memory array, managed and used by trbuf */
}trbuf_t;


/* -------------------------------- +
|									|
|	Contant							|
|									|
+ -------------------------------- */
#define	TRBUF_OVFSEL_MASK		(0b11000000)
#define	TRBUF_OVFSEL_POS		(6)
#define	TRBUF_UDFSEL_MASK		(0b00110000)
#define	TRBUF_UDFSEL_POS		(4)
#define	TRBUF_FLUSHSEL_MASK		(0b00001100)
#define	TRBUF_FLUSHSEL_POS		(2)

#define	TRBUF_OVFSEL_SILENT		(0b00)	/* Overflow mode: silently drop the new element (maintain buffer content) */
#define	TRBUF_OVFSEL_DISCARD	(0b01)	/* Overflow mode: discard the oldest element in buffer (keep last n element) */
#define	TRBUF_UDFSEL_EMPTY		(0b00)	/* Underflow mode: return to user with empty (typical non-stop polling usage) */
#define	TRBUF_UDFSEL_REPEAT		(0b01)	/* Underflow mode: repeat the lastest element (asynchronous register emulation) */
#define	TRBUF_FLUSHSEL_RAPID	(0b00)	/* Flush mode: rapid control-only flush (overwriting peripheral buffer) */
#define	TRBUF_FLUSHSEL_COMPLETE	(0b01)	/* Flush mode: complete memory flush (working cache space, shared memory) */


/*--------------------------------- +
|									|
|	Public Macro					|
|									|
+ -------------------------------- */
#define	TRBUF_ARRAYSIZE(type, cnt)		((cnt)*TTYPE_SIZEOF(type))		/* Gives the size (in bytes) of the memory array necessary */
#define	TRBUF_CTLSIZE()					(sizeof(trbuf_t))				/* Gives the size (in bytes) of the control structure */
#define TRBUF_TOTALSIZE(type, cnt)		(TRBUF_ARRAYSIZE(type,cnt) + TRBUF_CTLSIZE())	/* Gives the total size (in bytes) of memory needed */


/* -------------------------------- +
|									|
|	Public API						|
|									|
+ -------------------------------- */
tcode_t trbuf_init(trbuf_t * buf);

tcode_t trbuf_flush(trbuf_t * buf);

uint32_t trbuf_getFreeSpace(trbuf_t * buf);
uint32_t trbuf_getUsedSpace(trbuf_t * buf);
uint32_t trbuf_getTotalSpace(trbuf_t * buf);

bool trbuf_isEmpty(trbuf_t * buf);
bool trbuf_isFilled(trbuf_t * buf);
bool trbuf_isFull(trbuf_t * buf);

uint32_t trbuf_pushElements(trbuf_t * buf, void * src, uint32_t eleCnt);
uint32_t trbuf_pullElements(trbuf_t * buf, void * dst, uint32_t eleCnt);

#ifdef __cplusplus
}
#endif
#endif

